import { Box, Typography } from '@material-ui/core';
import React from 'react';
import construction from '../assets/images/building.png';

export default function UnderConstruction(props) {
    return <div>
        <Typography align="center" gutterBottom>
            <Box fontFamily="Fuggles" fontSize="h3.fontSize">
                {props.Text}
            </Box>
            <img src={construction} alt="Website is building" />
        </Typography>
    </div>;
}