import sectionRecordApi from "../../api/sectionRecord.api";
import { selectCurrentReadBookId } from "../title/selector";

const addSectionReducer = async (record, thunkApi) => {
  let rbid = selectCurrentReadBookId(thunkApi.getState());
  if(typeof rbid !== 'string' || rbid.length === 0) {
    return thunkApi.rejectWithValue('readbook is not available');
  }else{
    const response = await sectionRecordApi.add(rbid, record);
    return response.data;
  }
}

addSectionReducer.fulfilledCallback = (state, action) => {
  let section = {...action.meta.arg, ...action.payload};
  section.recs = [];
  state.data.push(section);
  state.status = 'succeeded';
}
addSectionReducer.rejectedCallback = (state, action) => {
  // state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addSectionReducer };