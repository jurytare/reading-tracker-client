import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getTitles, pickTitle, unPickTitle } from '../services/reducers/title';
import { selectTitles, selectCurrentTitleId } from '../services/reducers/title/selector';

import ListSelector from '../components/ListSelector';
import SearchBar from '../components/SearchBar';
import { createBook } from '../services/reducers/book';

function BookListContainer(props) {

  const dispatch = useDispatch();
  const bookList = useSelector(selectTitles);

  useEffect(() => {
    dispatch(getTitles(10));
  }, [dispatch]);
  
  const [searchContent, setSearchContent] = useState('');
  const currentTitleId = useSelector(selectCurrentTitleId);

  const handleSelectBook = (title, id) => {
    if(currentTitleId !== id) {
      dispatch(pickTitle({title, id}));
    }else{
      dispatch(unPickTitle());
    }
  }

  const handleAddBook = () => {
    dispatch(createBook());
  }

  const handleSearch = searchContent => {
    setSearchContent(searchContent);
  }

  return (
    <div>
      <SearchBar onAdding={handleAddBook} onSearching={null} onSearch={handleSearch}/>
      <ListSelector 
        List={bookList.map(title => ({
          ...title,
          elementName: title.name + " (" + title.usage + ")",
        }))} 
        onSelect={handleSelectBook} Filter={searchContent}
      />
    </div>
  )
}

export default BookListContainer;