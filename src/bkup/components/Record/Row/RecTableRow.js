import React, { useEffect, useState } from 'react';
import { ACT_EVENT, VIEW_MODE } from '../../../const/info.const';
import TimeInfo from '../../comon/TimeInfo';
import InfoController from '../../Info/InfoController';
import RecType from './RecType';

function RecTableRow(props) {

  const [Record, setRecord] = useState(props.Record)

  useEffect(() => {
    setRecord(props.Record)
  }, [props.Record])

  const handleEvent = (cmd, nextView) => {
    console.log("Got Event", cmd, "nextView", nextView, "record", Record)
    let newRecord = Record
    if(cmd === ACT_EVENT.CHANGE) {
      if (!(nextView & VIEW_MODE.isEditBit)
        && (Record.type & VIEW_MODE.isEditBit)){
        newRecord = props.Record
      }
    }else{
      if(props.onRecordChange) {
        if(props.onRecordChange(cmd, Record)) {
          console.log("Something wrong?")
        }
      }
    }
    setRecord({...newRecord, type:nextView})
  }

  const handleTypeChange = () => {
    let newType = Record.type ^ VIEW_MODE.isBeginBit
    setRecord({ ...Record, type: newType })
  }

  const handlePageChange = e => {
    setRecord({ ...Record, page: parseInt(e.target.value) })
  }

  const handleTimeChange = date => {
    setRecord({ ...Record, date: date })
  }
  return (
    <tr>
      <td key="rec-controller">
        <InfoController onEvent={handleEvent} ViewMode={Record.type} />
      </td>

      <td key="rec-info-type">
        <RecType Type={Record.type} onChange={handleTypeChange} />
      </td>
      <td>
        <TimeInfo 
          isEditing={Record.type & VIEW_MODE.isEditBit} 
          Time={Record.date} 
          onChange={handleTimeChange} />
      </td>
      <td>
        {
          Record.type & VIEW_MODE.isEditBit ?
            < input type={typeof Record.page} value={Record.page} onChange={handlePageChange} />
            :
            <label>{Record.page}</label>
        }
      </td>
    </tr>
  )
}

export default RecTableRow;