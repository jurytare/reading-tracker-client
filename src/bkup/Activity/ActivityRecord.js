import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faEdit, faBan, faTrash, faPause, faExclamationTriangle, faCheck } from '@fortawesome/free-solid-svg-icons';

import { convertInt2TimeStr } from '../Utils';
import { RecordCommand, RecordType } from './RecordRef'

function ActivityRecord(props) {

  const RecordElement = {
    day: ["Date", "date", "Remove"],
    time: ["Time", "time", "this"],
    page: ["Page", "number", "?"],
  }
  const RecordExtra = {
    process: ["Process", "number", ""],
    efc: ["Efficiency", "number", ""],
    eta: ["ETA", "text", ""],
  }

  let tempRecord = props.record
  if (tempRecord && tempRecord.hasOwnProperty('date')) {
    [tempRecord.day, tempRecord.time] = convertInt2TimeStr(tempRecord.date)
  }

  const [record, setRecord] = useState(tempRecord)

  useEffect(() => {
    setRecord(tempRecord)
  }, [tempRecord])

  if (!record) {
    return null
  }

  function handleEventCmd(cmd, val) {
    val.date = new Date(val.day + " " + val.time).getTime()
    // console.log(date)
    if (props.onRecordChange) {
      return props.onRecordChange(cmd, val.recId, val)
    }
    return 0
  }

  const handlePosEvent = e => {
    let oldType = record.type
    if (oldType & RecordType.isEditBit) {
      oldType &= ~(RecordType.isEditBit)
      if (oldType & RecordType.isRmBit) {
        if (handleEventCmd(RecordCommand.ADD, { ...record })) {
          return -1
        }
        oldType &= ~(RecordType.isRmBit)
      } else {
        if (handleEventCmd(RecordCommand.EDIT, { ...record })) {
          console.log("Got error. Exit!")
          return 0
        }
      }
    } else {
      if (oldType & RecordType.isRmBit) {
        if (handleEventCmd(RecordCommand.RM, { ...record })) {
          return -1
        }
        oldType &= ~(RecordType.isRmBit)
      } else {
        oldType |= (RecordType.isEditBit)
      }
    }
    setRecord({ ...record, type: oldType })
  }

  const handleNegEvent = e => {
    let oldType = record.type
    if (oldType & RecordType.isEditBit) {
      oldType &= ~(RecordType.isEditBit)
      if (oldType & RecordType.isRmBit) {
        handleEventCmd(RecordCommand.RM, record)
        return 0
        // oldType &= ~(RecordType.isRmBit)
      } else {
        setRecord(tempRecord)
        return 0
      }
    } else {
      if (oldType & RecordType.isRmBit) {
        console.log("Cancel remove an element")
        oldType &= ~(RecordType.isRmBit)
      } else {
        console.log("Trying to remove an element")
        oldType |= (RecordType.isRmBit)
      }
    }
    setRecord({ ...record, type: oldType })
  }

  const handleChange = e => {
    let elem = e.target.dataset.key
    let val = e.target.value
    if (elem === "page") {
      val = parseInt(val, 10)
    }
    setRecord({ ...record, [elem]: val })
  }

  const handleTypeChange = e => {
    setRecord({
      ...record,
      type: record.type ^ RecordType.isStartBit
    })
  }

  return (
    <tr>
      <td key={"rec-btn"}>
        <button variant="warning" size="sm"
          onClick={handlePosEvent}
        >
          <FontAwesomeIcon size="xs" icon={
            (record.type & RecordType.isEditBit) ||
              (record.type & RecordType.isRmBit) ?
              faCheck : faEdit
          } />
        </button>
        <button variant="danger" size="sm"
          onClick={handleNegEvent}
        >
          <FontAwesomeIcon size="xs" icon={
            (record.type & RecordType.isEditBit) ||
              (record.type & RecordType.isRmBit) ?
              faBan : faTrash
          } />
        </button>
      </td>

      <td key="rec-info-type">{
        record.type & RecordType.isEditBit ?
          <button variant="warning" size="sm"
            onClick={handleTypeChange}
          >
            <FontAwesomeIcon size="xs" icon={
              (record.type & RecordType.isStartBit) ?
                faPlay : faPause
            } />
          </button>
          :
          <FontAwesomeIcon size="xs" icon={
            record.type & RecordType.isRmBit ? faExclamationTriangle :
              record.type & RecordType.isStartBit ? faPlay : faPause} />
      }</td>

      {Object.keys(RecordElement).map(id => {
        return (<td key={"rec-info-" + id}>{
          record.type & RecordType.isEditBit ?
            <input
              type={RecordElement[id][1]}
              data-key={id}
              value={record[id] || ""}
              onChange={handleChange}
            />
            : record.type & RecordType.isRmBit ?
              <label>{RecordElement[id][2]}</label>
              : <label>{record[id]}</label>
        }</td>)
      })}

      {
        (record.type & (~RecordType.isStartBit) === 0) ?
          Object.keys(RecordExtra).map(id => {
            return (
              <td key={"rec-extra-" + id}>
                <label>{record[id]}</label>
              </td>
            )
          }) : null
      }

    </tr>
  )
}

export default ActivityRecord;