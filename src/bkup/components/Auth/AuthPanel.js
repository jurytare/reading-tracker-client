import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Dropdown from 'react-bootstrap/Dropdown';

import UserLogin from './UserLogin';
import UserRegister from './UserRegister';

import './AuthPanel.css'

function AuthPanel(props) {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      className="nav-link"
      href="#login"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));

  const CustomMenu = React.forwardRef(
    ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {

      // const [value, setValue] = useState('');
      return (
        <div
          ref={ref}
          style={style}
          className={className}
          aria-labelledby={labeledBy}
        >
          <Tabs defaultActiveKey="login">
            <Tab tabClassName="login-tab" eventKey="login" title="Sign in">
              <UserLogin onLogin={props.onLogin}/>
            </Tab>
            <Tab tabClassName="login-tab" eventKey="register" title="Sign up">
              <UserRegister />
            </Tab>
          </Tabs>
        </div>
      );
    },
  );

  return (
    <div>
      <Dropdown>
      <Dropdown.Toggle as={CustomToggle} >
        Login
      </Dropdown.Toggle>

        <Dropdown.Menu align="right" className="AuthPanel" as={CustomMenu} />
      </Dropdown>

    </div>
  );
}

export default AuthPanel;