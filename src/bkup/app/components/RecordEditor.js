import { faStop, faPause, faPlay, faStepForward, faExclamation, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { ReadingState } from '../containers/ReadPlayerContainer';
import { convertInt2TimeStr } from '../utils/Utils';

function getStateIcon(recState) {
  let icon;
  switch(recState) {
    case ReadingState.READING: icon = faPlay; break;
    case ReadingState.PAUSED: icon = faPause; break;
    case ReadingState.RESUME: icon = faStepForward; break;
    case ReadingState.IDLE: icon = faStop; break;
    default: icon = faExclamation; break;
  }
  return icon;
}

function RecordEditor(props) {

  let BaseTime = typeof props.BaseTime === 'number' ? props.BaseTime : 0;
  let records = Array.isArray(props.Records) ? props.Records : [];
  let timeStrs = records.map(rec => {
    let [dayStr, timeStr] = convertInt2TimeStr((rec.time + BaseTime));
    return dayStr+"T"+timeStr;
  })

  const handleDeleteRecord = (ev, recId) => {
    ev.preventDefault();
    if(typeof props.onDelete === 'function') {
      props.onDelete(recId);
    }
  }
  return (
    <table>
      <tbody>
        <tr>
          <td><b>Type</b></td>
          <td><b>Time (s)</b></td>
          <td><b>Page</b></td>
        </tr>
        {records.map((rec, id) => {
          return (
            <tr key={"record-editor-" + rec._id}>
              <td>
                <button variant="warning" size="sm"
                  onClick={event => props.onChange(event, 'state', rec._id)}
                >
                  <FontAwesomeIcon size="xs" icon={getStateIcon(rec.state)} />
                </button>
              </td>
              <td>
                < input
                  type="datetime-local"
                  step="1"
                  value={timeStrs[id]}
                  onChange={event => props.onChange(event, 'time', rec._id)}
                />
              </td>
              <td>
                < input
                  type="number"
                  value={rec.page}
                  onChange={event => props.onChange(event, 'page', rec._id)}
                />
              </td>
              <td>
                <button variant="danger" size="sm"
                  onClick={event => handleDeleteRecord(event, rec._id)}
                >
                  <FontAwesomeIcon size="xs" icon={faTrash} />
                </button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default RecordEditor;
