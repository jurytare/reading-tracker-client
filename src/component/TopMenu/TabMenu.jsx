import { Tab, Tabs } from '@material-ui/core';
import React from 'react';
import { A, usePath } from 'hookrouter';

function RouteLink() {
  return React.forwardRef((props, ref) => <A {...props}/>);
}

const tabName = [
  {link: '/community', label: "Community"} ,
  {link: '/library', label: "Library"} ,
  {link: '/bookshelf', label: "Bookshelf"} ,
  {link: '/about', label: "About"} ,
]

export default function TabMenu(props) {
  const path = usePath();
  const [val, setVal] = React.useState(false);

  React.useEffect(() => {
    let tmp = path.split('/', 2);
    if(tmp.length < 2) {
      setVal(false);
    }else{
      let foundId = tabName.findIndex(val => val.link === ('/' + tmp[1]));
      if(foundId >= 0) {
        setVal(tabName[foundId].link);
      }else{
        setVal(false);
      }
    }
  }, [path])
  
  return (
    <div>
      <Tabs value={val}>
        {tabName.map(tn => 
          <Tab key={tn.link} value={tn.link} href={tn.link} label={tn.label} component={RouteLink()}/>
        )}
      </Tabs>
    </div>
  )
}