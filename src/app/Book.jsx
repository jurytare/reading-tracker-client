import React, { useContext, useEffect, useRef, useState } from 'react';
import { useQueryParams } from 'hookrouter';

import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import LocalLibraryOutlinedIcon from '@material-ui/icons/LocalLibraryOutlined';
import EventNoteOutlinedIcon from '@material-ui/icons/EventNoteOutlined';

import { Container, Tab, Tabs } from '@material-ui/core';
import InfoContainer from '../component/Info/InfoContainer';
import ReadPlayer from '../component/Player/ReadPlayer';
import ReadEditor from '../component/Record/ReadEditor';
import { useDispatch, useSelector } from 'react-redux';
import { loadBook } from '../service/reducers/book';
import { loadSectionByBid } from '../service/reducers/sectionList';
import { selectReadBookId } from '../service/reducers/sectionList/selector';
import { NotificationContext } from '../service/common/NotificationService';

export default function Book(props) {
  const dispatch = useDispatch();
  const [selVal, setSelVal] = useState('info');

  const [queryUrl] = useQueryParams();
  const rbid = useSelector(selectReadBookId);

  const notification = useRef(useContext(NotificationContext));

  useEffect(() => {
    try {
      let user = JSON.parse(localStorage.getItem("user"));
      if(user && user.uid !== 'anonymous') {
        return;
      }
    } catch (error) {
      
    }
    notification.current.setNotification({
      open: true,
      msg: "As an anonymous user, you can access all features but your activities are shared. Let's create your own account to keep your privacy",
      severity: "warning",
    })
  }, [])

  useEffect(() => {
    if(typeof queryUrl.bid === 'string') {
        dispatch(loadBook(queryUrl.bid));
        dispatch(loadSectionByBid(queryUrl.bid));
    }
  }, [queryUrl, dispatch])

  const handleChange = (event, newValue) => {
    setSelVal(newValue);
  };

  return (
    <Container>
      <Tabs
        value={selVal}
        onChange={handleChange}
        indicatorColor="secondary"
        textColor="secondary"
        // aria-label="icon label tabs example"
      >
        <Tab value='info' icon={<InfoOutlinedIcon />} />
        {typeof rbid === 'string' && rbid.length ?
          [
            <Tab key='tab-read' value='read' icon={<LocalLibraryOutlinedIcon />} />,
            <Tab key='tab-rec' value='record' icon={<EventNoteOutlinedIcon />} />
          ]
          : null
        }

      </Tabs>

      <InfoContainer RBID={rbid} show={selVal==='info'}/>
      <ReadPlayer RBID={rbid} show={selVal==='read'}/>
      <ReadEditor RBID={rbid} show={selVal==='record'}/>
    </Container>
  )
}