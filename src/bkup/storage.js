export const getData = (key) => {
	return undefined
	if (!localStorage) return -1;

	try {
		return JSON.parse(localStorage.getItem(key));
	} catch (err) {
		console.error(`Error getting item ${key} from localStorage`, err);
		return undefined;
	}
};

export const storeData = (key, item) => {
	console.log("Store data " + key)
	return undefined
	if (!localStorage) return -1;

	try {
		localStorage.setItem(key, JSON.stringify(item));
	} catch (err) {
		console.error(`Error storing item ${key} to localStorage`, err);
		return undefined
	}
	return 0
};

export const removeData = (key) => {
	if (!localStorage) return -1;

	try {
		localStorage.removeItem(key);
	} catch (err) {
		console.error(`Error remove item ${key} to localStorage`, err);
		return -2
	}
	return 0
}

export const moveData = (orgKey, newKey) => {
	if (!localStorage) return -1;

	try {
		let data = localStorage.getItem(orgKey)
		if(typeof data === "string"){
			localStorage.setItem(newKey, data)
			localStorage.removeItem(orgKey);
		}
	} catch (err) {
		console.error(`Error move item from ${orgKey} to ${newKey}`, err);
		return -2;
	}
	return 0;
}