import BookApi from "../../api/book.api";

const createBookReducer = async (book) => {
  const response = await BookApi.create(book);
  return response.data;
}

createBookReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'succeeded';
}
createBookReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { createBookReducer };