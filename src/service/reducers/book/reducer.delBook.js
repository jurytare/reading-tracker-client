import bookApi from "../../api/book.api";

const delBookReducer = async (rbid) => {
  const response = await bookApi.del(rbid);
  return response.data;
}

delBookReducer.fulfilledCallback = (state, action) => {
  state.data = {};
  state.status = 'succeeded';
}
delBookReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { delBookReducer };