import React from 'react';
import Book from '../app/Book';
import BookShelf from '../app/BookShelf';
import LandingPage from '../app/LandingPage';
import Library from '../app/Library';
import UnderConstruction from '../utils/UnderConstruction';

const MainRouter = {
  '/'           : () => <LandingPage/>,
  '/community'  : () => <UnderConstruction Text="The community will be available soon"/>,
  '/library'    : () => <Library />,
  '/bookshelf'  : () => <BookShelf />,
  '/about'      : () => <UnderConstruction Text="About me will be available soon"/>,

  '/book'       : () => <Book />,
  '/me'         : () => <UnderConstruction Text="Yours will be available soon"/>,
  '/settings'   : () => <UnderConstruction Text="Settings will be available soon"/>,
};

export default MainRouter;