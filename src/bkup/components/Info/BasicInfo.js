import React, { createContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { VIEW_MODE } from '../../const/info.const';
import { loadBook, unloadBook } from '../../reducers/book';
import { selectBook } from '../../reducers/book/selector';
import { selectCurrentBookId } from '../../reducers/title/selector';
import InfoController from './InfoController';
import InfoEditor from './InfoEditor';
import InfoShow from './InfoShow';

export const InfoContext = createContext()

function BasicInfo(props) {

  const dispatch = useDispatch();
  const currentBookId = useSelector(selectCurrentBookId);
  const bookInfo = useSelector(selectBook);
  const [viewState, setViewState] = useState(0);

  useEffect(() => {
    if(currentBookId) {
      dispatch(loadBook(currentBookId));
    }else {
      dispatch(unloadBook());
    }
  }, [currentBookId, dispatch])
 
  const handleChangeView = (cmd, nextView) => {
    setViewState(nextView);
  }

  return (viewState & VIEW_MODE.isEditBit) ?
        <InfoEditor Book={bookInfo} onEvent={handleChangeView} ViewMode={viewState}/>
        : 
        <div>
          <InfoController onEvent={handleChangeView} ViewMode={viewState} />
          <InfoShow Book={bookInfo} />
        </div>

}

export default BasicInfo;