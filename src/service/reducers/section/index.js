import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { editSectionReducer } from '../section/reducer.editSection';
import { loadSectionReducer } from './reducer.loadSection';

const sliceName = 'section';

const initialState = {
  data:{
    page: 0,
    date: 0,
    recs: [],
  },
  status: 'empty',
  error: null,
}

const loadSection = createAsyncThunk(sliceName + '/loadSection', loadSectionReducer);
const editSection = createAsyncThunk(sliceName + '/editSection', editSectionReducer);

const sectionSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    unloadSection: (state) => {
      state.data = initialState.data;
      state.status = initialState.status;
      state.error = null;
    }
  },
  extraReducers: {
    ...addTrackingFetch(loadSection, loadSectionReducer),
    ...addTrackingFetch(editSection, editSectionReducer),
  },
})

export default sectionSlice.reducer;
export { loadSection, editSection };
export const { unloadSection } = sectionSlice.actions;
