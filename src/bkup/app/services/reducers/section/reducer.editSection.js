import SectionApi from "../../api/sectionRecord.api";
import { normalizeSection } from "../reducer.utils";
import { selectCurrentReadBookId } from "../title/selector";

const editSectionReducer = async (section, thunkApi) => {
  // const response = await SectionApi.delete(bookId, secId);
  const rbid = selectCurrentReadBookId(thunkApi.getState());
  section = normalizeSection(section);
  const response = await SectionApi.edit(rbid, section._id, section);
  let secEditted = normalizeSection(response.data, 1000);
  if(typeof secEditted.page === 'number' || typeof secEditted.date === 'number') {
    
  }
  return secEditted;
}

editSectionReducer.fulfilledCallback = (state, action) => {
  let secEditted = action.payload;
  let secOrg = state.data;
  typeof secEditted.page === 'number' && (secOrg.page = secEditted.page);
  typeof secEditted.date === 'number' && (secOrg.date = secEditted.date);
  secEditted.recs.map(result => {
    let tmpId = secOrg.recs.findIndex(rec => rec._id === result._id);
    let recResult = {
      _id : result.resId || result._id,
      state : result.state,
      page : result.page,
      time : result.time,
    }
    if(result.hasOwnProperty('resId')){
      // Adding result to records
      if(tmpId >= 0) {
        secOrg.recs[tmpId] = recResult;
      }else{
        secOrg.recs.push(recResult);
      }
    }else if(result.hasOwnProperty('state')) {
      // Editing result to records
      tmpId >= 0 && (secOrg.recs[tmpId] = recResult);
    }else{
      tmpId >= 0 && (secOrg.recs.splice(tmpId, 1));
    }
  })
  state.status = 'succeeded';
}
editSectionReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = {}
  state.error.message = action.error.message;
  state.error.payload = action.payload;
}

export { editSectionReducer };