import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import authService from '../../services/auth.service';

// const required = (value) => {
//   if (!value) {
//     return (
//       <div className="alert alert-danger" role="alert">
//         This field is required!
//       </div>
//     );
//   }
// };

function UserLogin(props) {

  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();

    // const form = event.currentTarget;
    // if (form.checkValidity() === false) {
    //   console.log("Validation OK")
    // }else{
    //   console.log("Validation failed")
    //   return
    // }
    // setValidated(!validated);
    let username = event.target[0].value
    let password = event.target[1].value

    authService.login(username, password)
      .then(response => {
        if(props.onLogin) {
          props.onLogin(response)
        }
      })
      .catch (err => {
      console.log(err.response)
    })
    event.stopPropagation();
  }

  return (
    <div className="col-md-12">
      <div className="card card-container border-0">
        <Form noValidate 
          // validated={validated} 
          onSubmit={handleSubmit}>
          <Form.Group controlId="formLoginUserName">
            <Form.Control required 
              // isInvalid={!username.length} 
              type="text" placeholder="User Name" 
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>

          <Form.Group controlId="formLoginPassword">
            <Form.Control required 
              // isInvalid={!password.length} 
              type="password" 
              placeholder="Password" 
            />
          </Form.Group>
          <Form.Group controlId="formLoginCheckbox">
            <Form.Check type="checkbox" label="Remember me" />
          </Form.Group>
          <Button variant="primary" type="submit">Log in</Button>
        </Form>
      </div>
    </div>
  );
}

export default UserLogin;