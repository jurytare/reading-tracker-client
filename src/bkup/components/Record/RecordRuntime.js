import React, { useContext, useEffect, useRef, useState } from 'react';
import { VIEW_MODE } from '../../const/info.const';
import { REC_STATE } from '../../const/rec.const';
import { BookContext } from '../../context/BookContextProvider';
import { milis2DayHourMinSecStr } from '../../utils/Utils';

function getSpeed(data, from, to) {
  let pageDiff, timeDiff, speed, offsetTime;

  if(!data || data.length <= to) {
    return undefined
  }else{
    if(!(data[to].type & VIEW_MODE.isBeginBit)){
      offsetTime = (data[from].time - data[to].time);
      to++;
    }else{
      offsetTime = 0;
    }
    pageDiff = data[from].page - data[to].page;
    timeDiff = (data[from].time - data[to].time) - offsetTime;
    speed = timeDiff/pageDiff;
    return speed
  }
}

function RecordRuntime(props) {

  // console.log(props.RecData);
  const RecData = props.RecData || [];
  const {bookInfo} = useContext(BookContext);
  const [today, setDate] = useState(new Date());

  const progress = useRef("");
  const lastSpeed = useRef(0);
  const etaReading = useRef(0);

  const readTime = useRef(0);
  const averageReadSpeed = useRef(0);
  const etaRead = useRef(0);

  // console.log(RecData)
  useEffect(() => {
    const timer = setInterval(() => {    
      setDate(new Date());
    }, 1000);
    return () => {
      clearInterval(timer);
    }
  }, []);

  useEffect(() => {
    if(RecData.length < 1) return;
    const currentRec = RecData[0];
    progress.current = currentRec.rec[0].page + "/" + bookInfo.pages + " pages (" +
      (currentRec.rec[0].page * 100 / bookInfo.pages).toFixed(2) + "%)";

    lastSpeed.current = getSpeed(currentRec.rec, 0, 1);
    etaReading.current = Math.floor(lastSpeed.current * (bookInfo.pages - currentRec.rec[0].page));

    let previousTs;
    readTime.current = 0;
    RecData.forEach((recSection) => {
      previousTs = 0;
      readTime.current += recSection.rec[0].time;
      recSection.rec.forEach((rec, id, arr) => {
        let index = arr.length - id - 1;
        if(previousTs > 0) {
          readTime.current -= (arr[index].date - previousTs);
        }
        if(arr[index].type & VIEW_MODE.isBeginBit){
          previousTs = arr[index].date;
        }else{
          previousTs = 0;
        }
      });
    });

    let firstSectionId = RecData.length - 1;
    let firstRecId = RecData[firstSectionId].rec.length - 1;
    averageReadSpeed.current = 
      readTime.current / (currentRec.rec[0].page - RecData[firstSectionId].rec[firstRecId].page);

    etaRead.current = Math.floor(averageReadSpeed.current * (bookInfo.pages - currentRec.rec[0].page));
  }, [RecData, bookInfo]);
  
  var components;
  if(RecData.length){
  const currentRec = RecData[0];
  let firstSectionId, firstRecId, timeDriff, averageProgressSpeed;
  firstSectionId = RecData.length - 1;
  firstRecId = RecData[firstSectionId].rec.length - 1;
  timeDriff = today.getTime() - (currentRec.start_date + currentRec.rec[0].time);

  switch(props.RecState){
    case REC_STATE.Reading:
      let averageSpeed, etaReadingStr;
      if(currentRec.rec.length > 1){
        averageSpeed = 
          (today.getTime() - currentRec.start_date)/
          (currentRec.rec[0].page - currentRec.rec[currentRec.rec.length -1].page);
      }else{
        averageSpeed = 0;
      }
      etaReadingStr = new Date(etaReading.current+today.getTime());
    
      components = [
        { key: "et", tit: "Elapsed time", val: milis2DayHourMinSecStr(currentRec.start_date, today.getTime()) },
        { key: "pg", tit: "Progress", val: progress.current },
        { key: "cs", tit: "Last speed", val: milis2DayHourMinSecStr(0, lastSpeed.current||0) + "/ page" },
        { key: "as", tit: "Average speed", val: milis2DayHourMinSecStr(0, averageSpeed) + "/ page" },
        { key: "ea", tit: "ETA", val: etaReadingStr.toLocaleTimeString() + " - " + etaReadingStr.toLocaleDateString() },
      ]
      break;
    case REC_STATE.Idle:
      let etaProgress, etaReadStr;
      averageProgressSpeed = 
        (today.getTime() - RecData[firstSectionId].start_date)/
        (currentRec.rec[0].page - RecData[firstSectionId].rec[firstRecId].page);
      etaProgress = new Date(Math.floor(
        averageProgressSpeed * (bookInfo.pages - currentRec.rec[0].page)+today.getTime()
      ));
      
      etaReadStr = new Date(etaRead.current+today.getTime());
      components = [
        { key: "pg", tit: "Progress", val: progress.current },
        
        { key: "br1", tit: "-------------", val: "-------------" },
        
        { key: "pt", tit: "Total progress time", val: milis2DayHourMinSecStr(RecData[firstSectionId].start_date, today.getTime()) },
        { key: "ps", tit: "Average progress speed", val: milis2DayHourMinSecStr(0, averageProgressSpeed) + "/ page" },
        { key: "ep", tit: "ETA of progress", val: etaProgress.toLocaleTimeString() + " - " + etaProgress.toLocaleDateString() },
        
        { key: "br2", tit: "-------------", val: "-------------" },

        { key: "rt", tit: "Total reading time", val: milis2DayHourMinSecStr(0, readTime.current) },
        { key: "rs", tit: "Average reading speed", val: milis2DayHourMinSecStr(0, averageReadSpeed.current) + "/ page" },
        { key: "er", tit: "ETA if read now", val: etaReadStr.toLocaleTimeString() + " - " + etaReadStr.toLocaleDateString() },
      ]
      break;
    case REC_STATE.Finish:
      let totalProgressTime, finishTime;
      totalProgressTime = currentRec.start_date + currentRec.rec[0].time - RecData[firstSectionId].start_date;
      averageProgressSpeed = totalProgressTime/
        (currentRec.rec[0].page - RecData[firstSectionId].rec[firstRecId].page);
      finishTime = new Date(currentRec.rec[0].time + currentRec.start_date);
      components = [
        { key: "pg", tit: "Progress", val: progress.current },
        { key: "pt", tit: "Total progress time", val: milis2DayHourMinSecStr(0, totalProgressTime) },
        { key: "ps", tit: "Average progress speed", val: milis2DayHourMinSecStr(0, averageProgressSpeed) + "/ page" },
        { key: "rt", tit: "Total reading time", val: milis2DayHourMinSecStr(0, readTime.current) + " (" + (readTime.current/totalProgressTime*100).toFixed(2) + "%)" },
        { key: "rs", tit: "Average reading speed", val: milis2DayHourMinSecStr(0, averageReadSpeed.current) + "/ page" },
        { key: "fa", tit: "Finish at", val: finishTime.toLocaleTimeString() + " - " + finishTime.toLocaleDateString() },
      ];
      break;
    default:
      components = []
      break;
  }
  }else{
    components = [
      {key: "none", tit: "Your have not read this book. Start read it now!", val: ""}
    ]
  }

  return (
    <div>
      <label id="timenow">
        <b>Now</b>
        <p>{
          ": " + today.toLocaleTimeString() + 
          " - " + today.toLocaleDateString()
        }</p></label>
      <table>
        <tbody>
          {components.map(row => {
            return (
              <tr key={row.key}>
                <td>{row.tit}</td>
                <td>{row.val}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default RecordRuntime;
