import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Info from './common/Info';

function BookInfo(props) {

  let name, author, pages, usage, user, timeStr;

  if(props.Book) {
    name = props.Book.name
    author = props.Book.author
    pages = props.Book.pages
    usage = props.Book.usage
    user = props.Book.user

    const tmp = new Date(props.Book.dateCreated);
    timeStr = tmp.toDateString() + " " + tmp.toLocaleTimeString();
  }

  const handleEditEvent = event => {
    event.preventDefault();
    if(typeof props.onChangeView === 'function') props.onChangeView();
  }
  return (
    <div>
      <button
        variant="success" size="sm"
        onClick={handleEditEvent}
      >
        <FontAwesomeIcon icon={faEdit} size="sm" />
      </button>
      <table>
        <tbody>
          <Info label="Name" value={name} />
          <Info label="Author" value={author} />
          <Info label="Pages" value={pages} />
          <Info label="Usage reference" value={usage} />
          <Info label="Shared by" value={user} />
          <Info label="Date shared" value={timeStr} />
        </tbody>
      </table>
    </div>

  )
}

export default BookInfo;