import React, { createContext, useEffect, useState } from "react"
import ReadingAnalysis from "../components/Analysis/ReadingAnalysis"
import { ACT_EVENT, VIEW_MODE } from "../const/info.const"
import BookApi from "../services/BookApi"
import { binarySearch, decodeNum, encodeNum } from "../utils/Utils"

export const BookContext = createContext()

export default function BookContextProvider(props) {

  const [bookInfo, setBookInfo] = useState({})
  const [records, setRecords] = useState([])

  useEffect(() => {
    console.log("Render Context", props.BookId)
    if(props.BookId === "0"){
      setBookInfo({
        _id: props.BookId,
        name: "no-name",
        pages: 0,
        start_page: 0,
        start_date: Math.floor(new Date().getTime()),
        type: (VIEW_MODE.isEditBit | VIEW_MODE.isRmBit)
      })
    }else if(props.BookId !== ""){
      BookApi.readBook(props.BookId)
        .then(response => {
          let temp = response.data
          temp.start_date = decodeNum(temp.start_date)*1000
          setBookInfo(temp)
        })
        .catch(err => {
          console.log("Failed to fetch bookInfo", err)
        })

      BookApi.getAllRecord(props.BookId)
      .then(response => {
        let tmpData = []
        let insertID = 0
        response.data.forEach(rec => {
          rec.date = decodeNum(rec.date)*1000
          insertID = binarySearch(tmpData, rec, (val1, val2) => {
            return val2.date - val1.date
          })
          if(insertID < tmpData.length){
            insertID++
          }
          tmpData.splice(insertID, 0, rec)
        })
        setRecords(tmpData)
      })
      .catch(err => {
        console.log("Failed to fetch records", err)
      })
    }else{
      setBookInfo({})
      setRecords([])
    }
  }, [props.BookId])

  const crudBookInfo = (cmd, book) => {
    let tmpBook = {
      _id: book._id,
      name: book.name,
      pages: book.pages,
      start_page: book.start_page,
      start_date: book.start_date,
    }
    tmpBook.start_date = encodeNum(Math.floor(book.start_date/1000))
    switch (cmd) {
      case ACT_EVENT.CREAT:
        BookApi.createBook(tmpBook)
          .then(response => {
            setBookInfo({...book, _id: response.data._id, type:0 })
            if(props.onListChange) props.onListChange()
          })
          .catch(err => {
            console.log("Failed to save book", err)
          })
        break;
      case ACT_EVENT.DELETE:
        BookApi.deleteBook(tmpBook._id)
          .then(response => {
            // setBookInfo({})
            if(props.onListChange) props.onListChange()
            if(props.onSelectBook) props.onSelectBook("")
          })
          .catch(err => {
            console.log("Failed to remove book", err)
          })
        break;
      case ACT_EVENT.UPDATE:
        BookApi.updateBook(tmpBook._id, tmpBook)
          .then(response => {
            if(tmpBook.name !== bookInfo.name){
              if(props.onListChange) props.onListChange()
            }
            setBookInfo({...book, type:0})
          })
          .catch(err => {
            console.log("Faile to save editted book", err)
          })
        break;
      case ACT_EVENT.RMV:
        if(props.onSelectBook) props.onSelectBook("")
        break;
      default:
        console.log("Command", cmd, "does not support", book)
        break;
    }
  }

  const crudRecord = (cmd, record) => {
    console.log("Got command: " + cmd + " for ", record)
    let rec2Server = {
      _id: record._id,
      page: record.page,
      type: record.type & VIEW_MODE.isBeginBit,
      date:encodeNum(Math.floor(record.date/1000))
    }
    switch (cmd) {
      case ACT_EVENT.ADD:
        setRecords([record, ...records])
        break;
      case ACT_EVENT.RMV:
        setRecords(records.filter(rec => rec._id !== record._id))
        break;

      case ACT_EVENT.CREAT:
        BookApi.createRecord(props.BookId, rec2Server)
        .then(response => {
          // Update all records?
          let upRec = {
            _id: response.data.recId,
            type: rec2Server.type,
            page: record.page,
            date: record.date,
          };
          let upRecs = records.map(rec => {
            if (rec._id === record._id) {
              let tmp = upRec;
              upRec = null
              return tmp
            }
            return rec;
          })
          if(!upRec) {
            setRecords(upRecs);
          }else{
            setRecords([upRec, ...records]);
          }
        })
        .catch(err => {
          console.log("Failed to create a record", err)
        })
        break;
      case ACT_EVENT.DELETE:
        BookApi.deleteRecord(props.BookId, record._id)
        .then(response => {
          setRecords(records.filter(rec => {
            return rec._id !== record._id
          }))
        })
        .catch(err => {
          console.log("Failed to delete a record", err)
        })
        break;
      case ACT_EVENT.UPDATE:
        BookApi.updateRecord(props.BookId, rec2Server._id, rec2Server)
        .then(response => {
          record.type = rec2Server.type
          setRecords(records.map(rec => {
            return (rec._id === record._id) ?
            record : rec
          }))
        })
        .catch(err => {
          console.log("Failed to update a record", err)
        })
        break;
      default:
        break;
    }
  }

  return (
    <BookContext.Provider value={{bookInfo, crudBookInfo, records, crudRecord}}>
      {bookInfo._id? props.children:null}
      <ReadingAnalysis BookInfo={bookInfo} Records={records}/>
    </BookContext.Provider>
  )
}