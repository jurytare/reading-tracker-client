import { reducersTable } from "../reducer.utils";

export const selectSectionList = state => {
  const localState = state[reducersTable.SECTIONLIST];
  return localState.data.sections;
}

export const selectReadInfo = state => {
  return state[reducersTable.SECTIONLIST].data;
  
}

export const selectCurrentSectionId = state => {
  const localState = state[reducersTable.SECTIONLIST];
  return localState.currentId;
}

export const selectReadingSection = state => {
  const localData = state[reducersTable.SECTIONLIST].data.sections;
  if(localData && localData.length){
    return localData[localData.length - 1];
  }else{
    return undefined
  }
}

export const selectReadBookId = state => {
  return state[reducersTable.SECTIONLIST].data.rbid;
}