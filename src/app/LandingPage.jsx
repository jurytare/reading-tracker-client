import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Carousel from 'react-material-ui-carousel'
import { Box } from '@material-ui/core';
import Chart from 'react-google-charts';

const useStyles = makeStyles((theme) => ({
  box: {
    width:"100%" ,
    height:"100%"  ,
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
  },
  paper: {
    color: 'green',
  },
}));

export default function LandingPage(props) {

  const classes = useStyles();

  return (
    <Box width="100%" height="100%"  >
      <Typography align="center" gutterBottom> 
        {/* <Box fontFamily="Fuggles" fontSize="h3.fontSize"> */}
        <Box fontFamily="Satisfy" fontSize="h4.fontSize">
          People in the world are reading books. Would you like to see how your reading process being?
        </Box>
      </Typography>

      <Carousel
        autoPlay={true}
        animation="slide"
        // interval={5000}
        // stopAutoPlayOnHover={false}
      >
        <Box className={classes.box}>
          <Chart
            width={1200}
            height={600}
            chartType="ColumnChart"
            loader={<div>Loading Chart</div>}
            data={dataChart_1}
            options={{
              title: 'Time spending for reading in a week',
              chartArea: { width: '70%' },
              hAxis: {
                title: 'Days of a week',
                minValue: 1,
                maxValue: 7,
              },
              vAxis: {
                title: 'Time reading (seconds)',
              },
            }}
            legendToggle
          />
        </Box>
        <Box className={classes.box}>
          <Chart
            width={1200}
            height={600}
            chartType="BarChart"
            loader={<div>Loading Chart</div>}
            data={dataChart_2}
            options={{
              title: 'Total time reading of the areas in the world',
              chartArea: { width: '70%' },
              isStacked: true,
              hAxis: {
                title: 'Total seconds for reading',
                minValue: 0,
                maxValue: 59,
              },
              vAxis: {
                title: 'Places',
              },
            }}
            legendToggle
          />
        </Box>

        <Box className={classes.box} >
          <Chart
            width={1200}
            height={600}
            chartType="ColumnChart"
            loader={<div>Loading Chart</div>}
            data={dataChart_3}
            options={{
              title: 'Time spending for reading in a month',
              chartArea: { width: '70%' },
              hAxis: {
                title: 'Day in month',
                minValue: 0,
                maxValue: 31,
              },
              vAxis: {
                title: 'Time reading (seconds)',
              },
            }}
            legendToggle
          />
        </Box>
        {/* <Box className={classes.box} >
          <Typography> Item 4</Typography>
        </Box> */}
      </Carousel>
    </Box>
  )
}

const dataChart_1 = [
  ["Time reading", "Time (s)"],
  ["Sun", 2851],
  ["Mon", 4280],
  ["Tue", 6484],
  ["Wed", 5461],
  ["Thu", 5636],
  ["Fri", 5585],
  ["Sat", 1449],
];

const dataChart_2 = [
  ['Place', 'Reading time', 'Relax time'],
  ['Da Nang, VN', 8175, 4234],
  ['Ha Noi, VN', 3792, 1564],
  ['Ho Chi Minh, VN', 2695, 1242],
  ['Houston, TX', 2099, 424],
  ['Philadelphia, PA', 1526, 632],
]

const dataChart_3 = [
  ["Time reading", "Time (s)"],
  [1, 0],
  [2, 1139],
  [3, 1449],
  [4, 1276],
  [5, 1023],
  [6, 1510],
  [7, 1209],
  [8, 2735],
  [9, 1120],
  [10, 0],
  [11, 0],
  [12, 1623],
  [13, 1550],
  [14, 0],
  [15, 1137],
  [16, 994],
  [17, 0],
  [18, 2851],
  [19, 176],
  [20, 263],
  [21, 0],
  [22, 0],
  [23, 970],
  [24, 1551],
  [25, 3359],
  [26, 644],
  [27, 1097],
  [28, 4070],
  [29, 0],
  [30, 0],
  [31, 0],
]