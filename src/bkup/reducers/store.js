import { configureStore } from "@reduxjs/toolkit";
import { reducersTable } from "./reducer.utils";
import titleReducer from './title';
import bookReducer from './book';
import sectionReducer from './section';

export default configureStore({
    reducer: {
        [reducersTable.TITLE]: titleReducer,
        [reducersTable.BOOK]: bookReducer,
        [reducersTable.SECTION]: sectionReducer,
    }
})