import { reducersTable } from "../reducer.utils";

const loadSectionReducer = async (arg, thunkAPI) => {
  let sectionList = thunkAPI.getState()[reducersTable.SECTIONLIST];
  const {id, sid} = arg;
  if(sectionList.status === 'succeeded') {
    if(typeof id === 'number') {
      if(sectionList.data.length > id) {
        return sectionList.data[id];
      }else{
        return thunkAPI.rejectWithValue(
          `id (${id}) request out of data range (${sectionList.data.length})`
        )
      }
    } else if(typeof sid === 'string') {
      let section = sectionList.data.find(sec => sec._id === sid);
      if(section) {
        return section;
      }else{
        return thunkAPI.rejectWithValue(
          `${sid} is not avaiable`
        )
      }
    } else {
      let idx = sectionList.currentId;
      if(idx >= 0 && idx < sectionList.data.length) {
        return sectionList.data[idx];
      }else{
        return thunkAPI.rejectWithValue(
          `current ID (${idx}) is not valid`
        )
      }
    }
  }
  return thunkAPI.rejectWithValue('Data is not valid');
}

loadSectionReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'succeeded';
}
loadSectionReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadSectionReducer };