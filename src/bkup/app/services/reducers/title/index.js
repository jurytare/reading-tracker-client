import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { getTitlesReducer } from './reducer.getTitles';

const sliceName = 'bookTitles';

const initialState = {
  data:[],
  currentId: -1,
  status: 'empty',
  error: null,
}

const getTitles = createAsyncThunk(sliceName + '/getTitles', getTitlesReducer);

const titleSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    pickTitle: (state, action) => {
      const { id } = action.payload;
      state.currentId = id;
    },
    unPickTitle: (state, action) => {
      state.currentId = -1;
    }
  },
  extraReducers: {
    ...addTrackingFetch(getTitles, getTitlesReducer),
  },
})

export default titleSlice.reducer;
export const { pickTitle, unPickTitle } = titleSlice.actions;
export { getTitles };
