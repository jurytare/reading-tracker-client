import React from 'react';
// import Button from 'react-bootstrap/Button';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash, faBan, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ACT_EVENT, VIEW_MODE } from '../../const/info.const';

const InfoController = props => {

  function handleEventCmd(cmd, nextView) {
    // console.log("Got command " + cmd + " with nextView "+nextView)
    if(props.onEvent) props.onEvent(cmd, nextView)
  }

  const handlePosEvent = e => {
    let viewMode = props.ViewMode
    if (viewMode & VIEW_MODE.isEditBit) {
      viewMode &= ~(VIEW_MODE.isEditBit)
      if (viewMode & VIEW_MODE.isRmBit) {
        viewMode &= ~(VIEW_MODE.isRmBit)
        handleEventCmd(ACT_EVENT.CREAT, viewMode)
      } else {
        handleEventCmd(ACT_EVENT.UPDATE, viewMode)
      }
    } else {
      if (viewMode & VIEW_MODE.isRmBit) {
        viewMode &= ~(VIEW_MODE.isRmBit)
        handleEventCmd(ACT_EVENT.DELETE, viewMode)
      } else {
        viewMode |= VIEW_MODE.isEditBit
        handleEventCmd(ACT_EVENT.CHANGE, viewMode)
      }
    }
  }

  const handleNegEvent = e => {
    let viewMode = props.ViewMode
    if (viewMode & VIEW_MODE.isEditBit) {
      viewMode &= ~(VIEW_MODE.isEditBit)
      if (viewMode & VIEW_MODE.isRmBit) {
        viewMode &= ~(VIEW_MODE.isRmBit)
        handleEventCmd(ACT_EVENT.RMV, viewMode)
      } else {
        handleEventCmd(ACT_EVENT.CHANGE, viewMode)
      }
    } else {
      if (viewMode & VIEW_MODE.isRmBit) {
        viewMode &= ~(VIEW_MODE.isRmBit)
        handleEventCmd(ACT_EVENT.CHANGE, viewMode)
      } else {
        viewMode |= VIEW_MODE.isRmBit
        handleEventCmd(ACT_EVENT.CHANGE, viewMode)
      }
    }
  }

  return (
    <div>
        <div>
          {/* EDIT/SAVE BUTTON */}
          <button
            variant="warning" size="sm"
            onClick={handlePosEvent}
          >
            <FontAwesomeIcon icon={
            (props.ViewMode & VIEW_MODE.isEditBit) ||
              (props.ViewMode & VIEW_MODE.isRmBit) ?
              faCheck : faEdit
          } size="sm" />
          </button>

          {/* REMOVE/CANCEL BUTTON */}
          <button
            variant="danger" size="sm"
            onClick={handleNegEvent}
          >
            <FontAwesomeIcon icon={
            (props.ViewMode & VIEW_MODE.isEditBit) ||
              (props.ViewMode & VIEW_MODE.isRmBit) ?
              faBan : faTrash
          } size="sm" />
          </button>
        </div>
    </div>
  )
}

export default InfoController;