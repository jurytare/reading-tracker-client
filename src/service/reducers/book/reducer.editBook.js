import BookApi from "../../api/book.api";

const editBookReducer = async ({rbid, bInfo}) => {
  // return "";
  const response = await BookApi.edit(rbid, bInfo);
  return response.data;
}

editBookReducer.pendingCallback = (state, action) => {
  state.status = 'editting';
}

editBookReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}
editBookReducer.rejectedCallback = (state, action) => {
  state.status = 'ready';
  state.error = action.error.message;
}

export { editBookReducer };