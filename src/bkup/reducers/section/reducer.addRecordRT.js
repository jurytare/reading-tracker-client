import sectionRecordApi from "../../services/sectionRecord.api";

const addRecordRTReducer = async (bookId, secId, record) => {
  const response = await sectionRecordApi.addRec(bookId, secId, record);
  return response.data;
}

addRecordRTReducer.fulfilledCallback = (state, action) => {
//   state.data._id = action.payload;
  state.status = 'succeeded';
}
addRecordRTReducer.rejectedCallback = (state, action) => {
  // state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addRecordRTReducer };