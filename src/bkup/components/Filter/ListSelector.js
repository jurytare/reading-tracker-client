import React from 'react';

function ListSelector(props) {
  
  const handleSelect = e => {
    e.preventDefault();
    let listId = e.target.dataset.key;
    if (props.onSelect) {
      props.onSelect(props.List[listId], listId)
    }
  }

  return (
    <ol>
      {props.List.map((element, id) => {
        return (
          <li key={element._id}>
            <a href={"#" + element._id}
              onClick={handleSelect}
              data-key={id}
            >
              {element.name}
            </a>

          </li>
        )
      })}
    </ol>
  );
}

export default ListSelector;