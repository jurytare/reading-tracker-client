import bookReadApi from "../../api/bookRead.api";

const editBookReducer = async ({bookId, bookInfo}) => {
  // return "";
  const response = await bookReadApi.updateBook(bookId, bookInfo);
  return response.data;
}

editBookReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}
editBookReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { editBookReducer };