export default class SectionAlys {
    constructor(a,b) {
      if(typeof a === 'object') {
        this.date = a.date || 1;
        this.page = a.page || 1;
        this.lastTime = a.date;
        this.lastPage = a.page;
        const records = a.recs || [];
        this.progressTime = 0;
        this.readTime = 0;
  
        if (records.length) {
          const latestRec = records[records.length - 1];
          this.lastTime = latestRec.time;
          // this.lastPage = latestRec.page;
          
          this.progressTime = this.readTime = latestRec.time;
  
          this.records = records.map((rec, id, arr) => {
            if(!rec.state && id < arr.length - 1) {
              this.readTime -= (arr[id+1].time - rec.time);
            }
            if (rec.page) {
              this.lastPage = rec.page;
            }
            return {
              _id: rec._id,
              time: rec.time || 1,
              page: rec.page || 1,
              state: rec.state || 0,
            }
          });
        }else{
          this.records = [];
        }
      }else{
        this.date = a;
        this.page = b;
        this.records = [];
      }
    }
  
    getStatus() {
      let ret = 'Reading';
      if (this.records.length) {
        let lastRec = this.records[this.records.length - 1];
        if (!lastRec.state) {
          if (lastRec.page) {
            ret = 'Stopped';
          } else {
            ret = 'Paused';
          }
        }
      }
      return ret;
    }
  
    getStartDay() {
      return this.date;
    }
    getStartPage() {
      return this.page;
    }
    getCurrentPage() {
      let currentPage = this.getStartPage();
      if(this.records.length) {
        for (let i = this.records.length-1; i >= 0; i--) {
          let rec = this.records[i];
          if(rec.state === 0 && rec.page === 0) {
            continue;
          }
          currentPage = rec.page;
          break;
        }
      }
      return currentPage;
    }
  
    getProgressTime() {
      if(this.records.length) {
        return this.records[this.records.length - 1].time;
      }else{
        return 0;
      }
    }
  
    getReadTime() {
      let readTime = this.getProgressTime();
      this.records.forEach((rec, id, arr) => {
        if(rec.state === 0) {
          if(id < arr.length - 1) {
            readTime -= (arr[id+1].time - rec.time);
          }
        }
      });
      return readTime;
    }
  
    getSpeed(speedType) {
      let timeDiff, pageDiff;
      pageDiff = this.getCurrentPage() - this.getStartPage();
      if(pageDiff) {
        timeDiff = speedType === 'read'? this.getReadTime():this.getProgressTime();
        return (timeDiff/pageDiff);
      }else{
        return 0;
      }
    }
  
    getETA(etaType, totalPages) {
      let currentPage = this.getCurrentPage();
      let pageRemain = totalPages - currentPage;
      if(this.getStatus() === 'Stopped'){
        let lastTime = this.date;
        if(this.records.length) {
          lastTime += this.records[this.records.length - 1].time;
        }
        return pageRemain *  this.getSpeed(etaType) + lastTime;
      }else{
        return pageRemain * this.getSpeed(etaType);
      }
    }
  }