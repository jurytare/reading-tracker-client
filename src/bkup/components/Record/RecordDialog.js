import React, { useEffect, useRef, useState } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { REC_STATE, REC_EVENT } from '../../const/rec.const';

function RecordDialog(props) {

  // var btnController;
  const [isTimeHidden, setTimeHidden] = useState(true);
  const [recNotif, setRecNotif] = useState("");
  const pageRef = useRef();
  const timeRef = useRef();
  const RecState = props.RecState;

  useEffect(() => {
    if(RecState === REC_STATE.Reading){
      setRecNotif("");
    }else if(RecState === REC_STATE.Idle){
      setRecNotif("");
    }else if(RecState === REC_STATE.Stopped){
      setRecNotif("Do you want to save records?")
    }
  }, [RecState])

  const handleInit = () => {
    setTimeHidden(true);
    setRecNotif("")
  }

  function getPageAtTime() {
    let pageNum = parseInt(pageRef.current.value)
    let timeNum = new Date().getTime()
    if(timeRef.current.value !== "") {
      let tmp = new Date(timeRef.current.value.replace('T', ' ')).getTime()
      if(tmp === Number){
        timeNum = tmp;
      }
    }
    return {page: pageNum, time: timeNum}
  }

  const handleShowTime = event => {
    event.preventDefault();
    setTimeHidden(!isTimeHidden);
    console.log(timeRef)
    timeRef.current.value = ""
  }

  const handleUpdatePage = event => {
    if(RecState === REC_STATE.Reading){
      if(event.key === 'Enter') {
        if(pageRef.current.value !== ""){
          let data = getPageAtTime()
          console.log("Update page", data.page, "at", data.time)
          handleEvent(REC_EVENT.UPDATE, data)
        }else{
          setRecNotif("Missing page value");
        }
        pageRef.current.value = ""
        timeRef.current.value = ""
      }
    }
  }

  const handleEvent = (event, data) => {
    if(props.onEvent) {
      props.onEvent(event, data)
    }
  }

  const handlePosEvent = event => {
    let data = getPageAtTime();
    switch(RecState){
      case REC_STATE.isPaused:
        handleEvent(REC_EVENT.READ)
        break;
      case REC_STATE.Reading:
        handleEvent(REC_EVENT.PAUSE, data);
        break;
      case REC_STATE.Paused:
        if(pageRef.current.value !== ""){
          handleEvent(REC_EVENT.RESUME, data);
        }else{
          setRecNotif("Which page are you going to read?");
        }
        break;
      case REC_STATE.Stopped:
        handleEvent(REC_EVENT.END, data)
        break;
      default:
        if(pageRef.current.value !== ""){
          handleEvent(REC_EVENT.READ, data);
        }else{
          setRecNotif("Which page are you going to read?");
        }
        break;
    }
    pageRef.current.value = ""
    timeRef.current.value = ""
    pageRef.current.focus();
  }

  const handleNegEvent = event => {
    let data = getPageAtTime()
    switch (RecState) {
      case REC_STATE.isPaused:
        if (timeRef.current.value === "") {
          setRecNotif("When did you pause reading?")
        } else {
          setRecNotif("")
          handleEvent(REC_EVENT.PAUSE, data)
        }
        break;
      case REC_STATE.Reading:
      case REC_STATE.Paused:
        if (pageRef.current.value === "") {
          setRecNotif("Which page do you stop at?")
        } else {
          handleEvent(REC_EVENT.STOP, data);
          // setRecNotif("");
        }
        break;
      case REC_STATE.Stopped:
        handleEvent(REC_EVENT.END)
        break;
      default:
        if (props.onClose) {
          props.onClose();
        }
        break;
    }
    pageRef.current.value = ""
    timeRef.current.value = ""
    pageRef.current.focus();
  }

  let btnController, btnList = [];
  switch (RecState) {
    case REC_STATE.isPaused:
      btnController = {
        pos: { text: "Continue", color: "primary" },
        neg: { text: "Add break", color: "danger" },
      }
      break;
    case REC_STATE.Reading:
      btnController = {
        pos: { text: "Pause", color: "secondary" },
        neg: { text: "Stop", color: "danger" },
      }
      break;
    case REC_STATE.Paused:
      btnController = {
        pos: { text: "Resume", color: "primary" },
        neg: { text: "Stop", color: "danger" },
      }
      break;
    case REC_STATE.Stopped:
      btnController = {
        pos: { text: "Save", color: "primary" },
        neg: { text: "Discard", color: "danger" },
      }
      break;
    case REC_STATE.Finish:
      btnController = {
        neg: { text: "Close", color: "danger" },
      }
      break;
    case REC_STATE.Idle:
      btnController = {
        pos: { text: "Read", color: "primary" },
        neg: { text: "Close", color: "danger" },
      }
      break;
    default:
      btnController = {};
      break;
  }
  if (btnController.pos) {
    btnList.push(
      <Button key={"btnPos-" + RecState} variant={btnController.pos.color} onClick={handlePosEvent}>
        {btnController.pos.text}
      </Button>
    )
  }
  if (btnController.neg) {
    btnList.push(
      <Button key={"btnNeg-" + RecState} variant={btnController.neg.color} onClick={handleNegEvent}>
        {btnController.neg.text}
      </Button>
    )
  }

  return (
    <Modal
      show={RecState !== null}
      // onHide={handleClose}
      backdrop="static"
      keyboard={false}
      size="lg"
      onEnter={handleInit}
      // aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header>
        <Modal.Title>Reading...</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.children}
      </Modal.Body>
      <Modal.Footer>
        <div className="mr-auto">
          <div>
            <p hidden={recNotif === ""}>{recNotif}</p>
          </div>
          <div>
            <input type="number" placeholder='page' 
              ref={pageRef} onKeyDown={handleUpdatePage} autoFocus
            />
            <a href="#?" onClick={handleShowTime}> + Add time </a>
            <input hidden={isTimeHidden} type="datetime-local" step="1"
              ref={timeRef}
            />
          </div>
        </div>
        {btnList}
      </Modal.Footer>
    </Modal>
  );
}

export default RecordDialog;
