import React from 'react';

import { Box, Button, Paper } from '@material-ui/core';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import NoteAddOutlinedIcon from '@material-ui/icons/NoteAddOutlined';
import EditIcon from '@material-ui/icons/Edit';

import Info from '../common/Info';

import testImg from './no-image.png';

const bInfo = {
  name: "",
  author: "",
  pages: 0,
  usage: 0,
  user: "",
  dateCreated: 1620051835000,
}

export default function BookInfo(props) {

  const Book = props.Book || bInfo;
  const tmp = new Date(Book.dateCreated);
  const dayStr = tmp.toDateString();
  const timeStr = tmp.toLocaleTimeString();

  const handleEdit = ev => {
    ev.preventDefault();
    typeof props.onEditReq === 'function' && props.onEditReq(1);
  }

  const handleAdd = ev => {
    ev.preventDefault();
    typeof props.onAddReq === 'function' && props.onAddReq(Book._id);
  }

  const handleDelete = ev => {
    ev.preventDefault();
    typeof props.onRmvReq === 'function' && props.onRmvReq(props.RBID);
  }

  return (
    <Box>
      <img alt='test-review-img' src={testImg} />
      <Box display="flex" flexDirection="row-reverse">
        {props.RBID ?
          <React.Fragment>
            <Button color="secondary" onClick={handleDelete}>
              <DeleteOutlinedIcon />
            </Button>
            <Button color="primary" onClick={handleEdit}>
              <EditIcon />
            </Button>
          </React.Fragment>
          :
          <Button color="primary" onClick={handleAdd}>
            <NoteAddOutlinedIcon/>
          </Button>
        }
      </Box>
      <Paper elevation={3}>
        <table>
          <tbody>
            <Info label="Name" value={Book.name} />
            <Info label="Author" value={Book.author} />
            <Info label="Pages" value={Book.pages} />
            <Info label="Usage reference" value={Book.usage} />
            <Info label="Shared by" value={Book.user} />
            <Info label="Date shared" value={dayStr + " " + timeStr} />
          </tbody>
        </table>
      </Paper>
    </Box>
  )
}