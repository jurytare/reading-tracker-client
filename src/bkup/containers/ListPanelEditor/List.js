import React from 'react';

export default function ListOfLPE(props) {
    console.log('Found', props.List.length, 'elements')
    const Cont = props.Cont ? props.Container: 'ul';
    const Prefix = props.Prefix ? props.Prefix : 'li';
    return (
        <Cont>
            {!props.List ? null:
                props.List.map(element => (<props.Element value={element}/>))
            }
        </Cont>
    )
}