import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import { ClickAwayListener, Grow, MenuList, Paper, Popper } from '@material-ui/core';
import TabMenu from './TabMenu';
import { useDispatch, useSelector } from 'react-redux';
import { selectUserName } from '../../service/reducers/user/selector';
import { login, logout } from '../../service/reducers/user';
import Sign from './Sign';
import authService from '../../service/api/auth.service';
import { A } from 'hookrouter';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function MenuAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const uid = useSelector(selectUserName);
  const dispatch = useDispatch();

  React.useEffect(() => {
    let user;
    try {
      user = JSON.parse(localStorage.getItem("user"))
      if(user) {
        if(user.hasOwnProperty('expired')) {
          if(user.expired > new Date().getTime()){
            dispatch(login(user))
            return;
          }
        }
        localStorage.removeItem("user");
      }
    } catch (error) {
      console.log(error);
    }
    dispatch(logout());
  }, [dispatch])

  useEffect(() => {
    setAnchorEl(null);
  }, [uid])

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    authService.logout();
    dispatch(logout());
    setAnchorEl(null);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <A href="/" style={{ 
              textDecoration: 'none',
              color: 'white',
              }}>
              Mr. Tea
            </A>
          </Typography>
          <TabMenu />
          <Popper open={open} anchorEl={anchorEl} role={undefined} transition>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
              >

                <Paper z-index={1}>
                  <ClickAwayListener onClickAway={handleClose}>
                  {
                    uid ? 
                    <MenuList autoFocusItem={open} id="menu-list-grow" >
                      <MenuItem onClick={handleClose}>Profile</MenuItem>
                      <MenuItem onClick={handleClose}>My account</MenuItem>
                      <MenuItem onClick={handleLogout}>Logout</MenuItem>
                    </MenuList>
                    :
                    <div hidden={!open}>
                      <Sign/>
                    </div>
                  }
                  </ClickAwayListener>
                </Paper>

              </Grow>
            )}
          </Popper>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>

        </Toolbar>
      </AppBar>
    </div>
  );
}
