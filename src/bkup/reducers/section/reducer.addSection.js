import sectionRecordApi from "../../services/sectionRecord.api";

const addSectionReducer = async (bookId, record) => {
  const response = await sectionRecordApi.add(bookId, record);
  return response.data;
}

addSectionReducer.fulfilledCallback = (state, action) => {
  state.data._id = action.payload;
  state.status = 'succeeded';
}
addSectionReducer.rejectedCallback = (state, action) => {
  // state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addSectionReducer };