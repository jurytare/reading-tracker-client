import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadRecords, unloadRecords } from '../../reducers/section';
import { selectCurrentSection, selectSectionList } from '../../reducers/section/selector';
import ListSelector from '../Filter/ListSelector';

function SectionList(props) {

  const sections = useSelector(selectSectionList);
  const currentSection = useSelector(selectCurrentSection);
  const dispatch = useDispatch();

  useEffect(()=> {
    return () => {
      dispatch(unloadRecords());
    }
  }, [dispatch])

  const handleSelectSection = (section, id) => {
    if(currentSection && currentSection._id === section._id) {
      dispatch(unloadRecords());
    }else{
      dispatch(loadRecords({section, id}));
    }
  }

  return (
    <ListSelector
    onSelect={handleSelectSection}
    List={sections.map(section => ({
      _id: section._id,
      name: new Date(section.date * 1000).toLocaleString() + " (" + section.page + ")",
    }))}
  />
  );
}

export default SectionList;