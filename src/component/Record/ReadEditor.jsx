import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Box from '@material-ui/core/Box';
import SectionEditor from './SectionEditor';
import { useDispatch, useSelector } from 'react-redux';
import { selectSectionList } from '../../service/reducers/sectionList/selector';
import { editSection } from '../../service/reducers/sectionList';
import sectionRecordApi from '../../service/api/sectionRecord.api';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    height: 300,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
}));

export default function ReadEditor(props) {
  
  const rbid = props.RBID;
  const [selSecId, setSelSecId] = React.useState(-1);
  const classes = useStyles();
  const dispatch = useDispatch();
  const SectionList = useSelector(selectSectionList);

  if(!props.show) {
    return null;
  }
  
  const handleListSelect = (event, secId, id) => {
    if(selSecId !== id)
      setSelSecId(id);
    else
      setSelSecId(-1);
  }

  const handleCreateSection = section => {
    
  }

  const handleSaveSection = section => {
    sectionRecordApi.edit(rbid, section._id, section)
      .then(resp => {
        dispatch(editSection({
          sid: section._id,
          section: resp,
        }));
      }).catch(err => {
        console.log('Unable to save section');
      })
  }

  const handleDeleteSection = section => {
    sectionRecordApi.delete(rbid, section._id)
      .then(resp => {
        dispatch(editSection({sid:section._id}));
      }).catch(err => {
        console.log('Unable to remove section');
      })
  }

  return (
    <Box>
      <List className={classes.root}>
        {SectionList.map((sec, id) => (
          <ListItem
            button
            key={"recEdit-" + sec._id}
            selected={id === selSecId}
            onClick={event => handleListSelect(event, sec._id, id)}
          >
            <ListItemText primary={sec.page} secondary={sec.date} />
          </ListItem>
        ))}
      </List>
      {selSecId >= 0 && selSecId < SectionList.length ?
      <SectionEditor Section={SectionList[selSecId]} 
        onSave={handleSaveSection}
        onDelete={handleDeleteSection}
      />
      : null}
    </Box>
  )
}