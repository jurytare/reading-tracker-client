import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ListSelector from '../components/ListSelector';
import SearchBar from '../components/SearchBar';
import { createSection } from '../services/reducers/section';
import { pickSection, unPickSection } from '../services/reducers/sectionList';
import {  selectSectionList, selectCurrentSectionId } from '../services/reducers/sectionList/selector';

function SectionListContainer(props) {

  const dispatch = useDispatch();
  const sectionList = useSelector(selectSectionList);
  
  const [searchContent, setSearchContent] = useState('');
  const SectionStatus = useSelector(selectCurrentSectionId);

  const handleSelectSection = (section, id) => {
    if(SectionStatus !== id) {
      dispatch(pickSection({id:id}));
    }else{
      dispatch(unPickSection());
    }
  }

  const handleAddSection = () => {
    dispatch(createSection());
  }

  const handleSearch = searchContent => {
    setSearchContent(searchContent);
  }

  let ListSection = []

  sectionList.forEach(section => {
    const tmp = new Date(section.date);
    ListSection.push({
      ...section,
      elementName: tmp.toLocaleDateString() + " " + tmp.toLocaleTimeString() + " - " + section.page,
    })
  });

  return (
    <div>
      <SearchBar onAdding={handleAddSection} onSearching={null} onSearch={handleSearch} />
      { ListSection.length ?
        <ListSelector
          List={ListSection}
          onSelect={handleSelectSection} Filter={searchContent}
        /> : <div>No section available yet</div>
      }
    </div>
  )
}

export default SectionListContainer;