import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { Avatar, Container, ListItemAvatar, Typography } from '@material-ui/core';

import noImage from './no-image.png';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    // maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function BookCard(props) {
  const classes = useStyles();

  const Book = props.Book || {
    _id:0,
    name:"Null",
    author: "Null",
  }
  if(!props.Book) {return null};

  const handleSelect = (event) => {
    event.preventDefault();
    typeof props.onSelect === 'function' && props.onSelect(Book._id);
  }

  return (
    <Container className={classes.root} onClick={handleSelect}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem button>
          <ListItemAvatar>
            <Avatar alt="ava" src={noImage} />
          </ListItemAvatar>
          <ListItemText
            primary={Book.name}
            secondary={
              <React.Fragment>
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.inline}
                  color="textPrimary"
                >
                  {Book.author}
                </Typography>
                {/* {" — I'll be in your neighborhood doing errands this…"} */}
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />

      </List>

    </Container>
  );
}
