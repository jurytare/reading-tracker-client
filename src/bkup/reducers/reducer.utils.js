function defaultPendingCallback (state, action) {state.status = 'loading';}

function defaultFulfilledCallback (state, action) {
    state.data = action.payload
    state.status = 'succeeded';
}

function defaultRejectCallback (state, action) {
    state.status = 'failed';
    state.error = action.error.message;
}

export function addTrackingFetch (thunkAction, reducer) {
  return {
    [thunkAction.pending]: reducer.pendingCallback || defaultPendingCallback,
    [thunkAction.fulfilled]: reducer.fulfilledCallback || defaultFulfilledCallback,
    [thunkAction.rejected]: reducer.rejectedCallback || defaultRejectCallback,
  }
}

export const reducersTable = {
  BOOK: 'book',
  SECTION: 'sections',
  TITLE: 'titles',
}
