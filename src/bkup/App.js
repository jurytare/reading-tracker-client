import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import React from 'react';
import TopMenu from './router/TopMenu';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Help from './components/Help/Help';
import ReadingTracker from './app/ReadingTracker';

function App() {

  return (
    <BrowserRouter>
      <TopMenu />
      <Switch>
        <Route exact path={["/", "/readingTracker"]} component={ReadingTracker} />
        <Route exact path="/help" component={Help} />
        {/*<Route exact path="/register" component={Register} />
        <Route exact path="/profile" component={Profile} />
        <Route path="/user" component={BoardUser} />
        <Route path="/mod" component={BoardModerator} />
        <Route path="/admin" component={BoardAdmin} /> */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
