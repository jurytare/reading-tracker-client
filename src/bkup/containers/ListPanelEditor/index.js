import React from 'react';
import EditorOfLPE from './Editor';
import ListOfLPE from './List';
import PanelOfLPE from './Panel';

export default function ListPanelEditor(props) {
    return (
        <div>
            {props.children}
        </div>
    )
}

ListPanelEditor.List = ListOfLPE;
ListPanelEditor.Panel = PanelOfLPE;
ListPanelEditor.Editor = EditorOfLPE;