import React from 'react';
import Info from './common/Info';

// Status: idle/reading/paused
// Progress: 1/10 (10%)
// Start date: yyyy-mm-dd hh:mm:ss (y m d h m s)
// Progress speed: yy mm did hh mm ss / page
// Expect ETA

export default function ReadingAnalysis(props) {
  return (
    <table>
      <tbody>
        <Info label="Status" value='' />
        <Info label="Page start" value='' />
        <Info label="Page read" value='' />
        <Info label="Start date" value='' />
        <Info label="Total time" value='' />
        <Info label="Reading time" value='' />
        <Info label="Reading speed" value='' />
        <Info label="Expect ETA" value='' />
      </tbody>
    </table>
  )
}