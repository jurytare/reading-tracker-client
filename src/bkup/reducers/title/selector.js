import { reducersTable } from "../reducer.utils";

export const selectTitles = state => {
  let bookList = state[reducersTable.TITLE]
  return bookList.data;
}

export const selectCurrentBookId =  state => {
  let bookList = state[reducersTable.TITLE];
  return bookList.currentId < 0 ? undefined : bookList.data[bookList.currentId]._id;
}

export const selectCurrentReadBookId =  state => {
  let bookList = state[reducersTable.TITLE];
  return bookList.currentId < 0 ? undefined : bookList.data[bookList.currentId].rbid;
}