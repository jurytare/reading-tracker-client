/* eslint-disable no-use-before-define */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export default function TextWithClear(props) {

  const handleChange = (event, value) => {
    event.preventDefault();
    if(props.onChange) props.onChange();
  }

  return (
    <div style={{ width: 200 }}>
      <Autocomplete
        onInputChange={handleChange}
        freeSolo
        disableClearable
        options={[]}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search input"
            margin="normal"
            variant="outlined"
            InputProps={{ ...params.InputProps, type: 'search' }}
          />
        )}
      />
    </div>
  )
};