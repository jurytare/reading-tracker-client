import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { addRecordRTReducer } from './reducer.addRecordRT';
import { addSectionReducer } from './reducer.addSection';
import { ChangeLocalSection } from './reducer.changeSectionLocal';
import { delSectionReducer } from './reducer.delSection';
import { loadSectionsReducer } from './reducer.loadSections';

const sliceName = 'sections';

const initialState = {
  data:[],
  sid: -1,
  status: 'empty',
  error: null,
}

const loadSections = createAsyncThunk(sliceName + '/loadSections', loadSectionsReducer);
const addSection = createAsyncThunk(sliceName + '/addSection', addSectionReducer);
const delSection = createAsyncThunk(sliceName + '/delSection', delSectionReducer);
const addRecordRT = createAsyncThunk(sliceName + '/addRecordRT', addRecordRTReducer);

const sectionsSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    loadRecords: ChangeLocalSection.loadRecords,
    unloadRecords: ChangeLocalSection.unloadRecords,
    addRecord: ChangeLocalSection.addRecord,
    editRecord: ChangeLocalSection.editRecord,
    delRecord: ChangeLocalSection.delRecord,
  },
  extraReducers: {
    ...addTrackingFetch(loadSections, loadSectionsReducer),
    ...addTrackingFetch(addSection, addSectionReducer),
    ...addTrackingFetch(delSection, delSectionReducer),
    ...addTrackingFetch(addRecordRT, addRecordRTReducer),
  },
})

export default sectionsSlice.reducer;
export const { loadRecords, unloadRecords, addRecord, editRecord, delRecord } = sectionsSlice.actions;
export { loadSections, addSection, delSection, addRecordRT };
export const selectBook = state => state[sectionsSlice.name];