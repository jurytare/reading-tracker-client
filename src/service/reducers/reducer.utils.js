function defaultPendingCallback (state, action) {state.status = 'loading';}

function defaultFulfilledCallback (state, action) {
    state.data = action.payload
    state.status = 'succeeded';
}

function defaultRejectCallback (state, action) {
    state.status = 'failed';
    state.error = action.error.message;
}

export function addTrackingFetch (thunkAction, reducer) {
  return {
    [thunkAction.pending]: reducer.pendingCallback || defaultPendingCallback,
    [thunkAction.fulfilled]: reducer.fulfilledCallback || defaultFulfilledCallback,
    [thunkAction.rejected]: reducer.rejectedCallback || defaultRejectCallback,
  }
}

export const reducersTable = {
  USER: 'user',
  BOOK: 'book',
  BOOKTITLE: 'bookTitles',
  READTITLE: 'readTitles',
  SECTION: 'section',
  SECTIONLIST: 'sectionList'
}

export function normalizeSection(section, ratio) {
  if(typeof ratio !== 'number') {
    ratio = 1/1000;
  } 
  let tmp = {...section}
  if (tmp.date > 0) {
    tmp.date = Math.floor(tmp.date * ratio);
  }
  tmp.recs = section.recs.map((rec, id) => { 
    if (rec.time > 0) {
      return {...rec, time: Math.floor(rec.time * ratio)}; 
    }else{
      return rec
    }
  });
  return tmp;
}
