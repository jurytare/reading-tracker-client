import { faPlay, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

function RecTableHead(props) {

  const handleNewRecord = e => {
    if(props.onAddRec) props.onAddRec();
  }

  const handleStartRec = e => {
    if(props.onStartRec) props.onStartRec();
  }

  const headerComponents = [
    (<div>
      <button variant="success" size="sm"
        onClick={handleNewRecord}
      >
        <FontAwesomeIcon icon={faPlus} size="xs" />
      </button>
      <button variant="success" size="sm"
          onClick={handleStartRec}>
        <FontAwesomeIcon icon={faPlay} size="xs" />
      </button>
    </div>),
    (<label>Type</label>),
    (<label>Time</label>),
    (<label>Page</label>),
  ]
  return (
    <thead>
      <tr>{
        headerComponents.map((elm, id) => {
          return (<td key={"head-rec-" + id}>{elm}</td>)
        })
      }</tr>
    </thead>
  );
}

export default RecTableHead;