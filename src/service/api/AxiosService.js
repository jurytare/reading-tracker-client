import axios from "axios";
import { baseUrl } from "./service.const";

const user = JSON.parse(localStorage.getItem('user'))
let heading = {
  'Content-type': 'application/json',
}
if (user) {
  heading['x-access-token'] = user.accessToken
}
var link = axios.create({
  baseURL: baseUrl,
  headers: heading,
})
console.log(link.defaults.headers);

export default link;

export function setAuthToken(token) {
  if(typeof token === 'string' && token.length) {
    link.defaults.headers['x-access-token'] = token;
  }else{
    delete link.defaults.headers['x-access-token'];
  }
}