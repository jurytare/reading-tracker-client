import React from 'react';
import { Chart } from "react-google-charts";
import { ReadAnalysis } from './ReadAnalysis';

export default function MonthAnalysis(props) {

  const sections = props.Sections || [];
  let SecAlys = new ReadAnalysis(sections);
  let timeData = SecAlys.getMonthAnalysis();
  let dataChart = [['Time reading', 'Time (s)']];
  for (let i = 1; i < 32; i++) {
    let tmp = timeData.find(dat => dat.date === i);
    if (tmp) {
      dataChart.push([i, tmp.time]);
    }else{
      dataChart.push([i, 0])
    }
  }

  return (
  <Chart
    width={600}
    height={300}
    chartType="ColumnChart"
    loader={<div>Loading Chart</div>}
    data={dataChart}
    options={{
      title: 'The most day in a month for reading',
      chartArea: { width: '70%' },
      hAxis: {
        title: 'Day in month',
        minValue: 0,
        maxValue: 31,
      },
      vAxis: {
        title: 'Time reading (seconds)',
      },
    }}
    legendToggle
  />)
}