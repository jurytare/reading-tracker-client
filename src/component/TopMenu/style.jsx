import { makeStyles } from "@material-ui/core";


export const useStyles = makeStyles({
    signBox: {
      margin: '16px 16px 10px 10px',
    },
    tabLable: {
      minWidth: '96px',
    },
    inputText: {
      width: '100%',
    },
    signFooter: {
      margin: '16px 0px 16px 0px',
    }
  });