export const REQ = {
  BOOK: {
    ADD: 'AB',
    RM: 'RB',
    EDIT: 'EB',
  }
}

export const ACT_MODE = {
  UNSELECT: "UNSELECT",
  VIEW: "SELECTED",
  EDITING: "EDITING",
  ADDING: "ADDING",
}

export const ACT_EVENT = {
  ADD: "ADD",
  RMV: "RMV",
  SAVE: "SAVE",
  CHANGE: "CHANGE",

  CREAT: "CREAT",
  READ: "READ",
  UPDATE: "UPDATE",
  DELETE: "DELETE",
}

export const VIEW_MODE = {
  isBeginBit: 1<<0,
  isEditBit: 1<<1,
  isRmBit: 1<<2,
}

export const PROPS_NAME = {
  name: "Name",
  pages: "NO. pages",
  start_page: "Starting page",
  start_day: "Starting day",
  start_time: "Starting time",
}