import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
// import './TopMenu.css'
import UserLogin from '../components/Auth/UserLogin';
import AuthPanel from '../components/Auth/AuthPanel';
import { FormControl, InputGroup, Nav, Navbar } from 'react-bootstrap';
import authService from '../services/auth.service';

// {
//   id: "5ffaa7f25c759c09203a459c",
//   username: "xuantra",
//   email: "jurygone@yahoo.com.vn",
//   roles: [
//     "ROLE_ADMIN"
//   ],
//   accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmFhN2YyNWM3NTljMDkyMDNhNDU5YyIsImlhdCI6MTYxMDI5NTk0OCwiZXhwIjoxNjEwMzgyMzQ4fQ.PvcWoLknKbRR10YBpuYyjs2gVjORRXExBAadJ0qxwfo"
// }


function TopMenu(props) {
  const [currentUser, setCurrentUser] = useState(undefined);

  useState(() => {
    const user = authService.getCurrentUser()
    setCurrentUser(user || undefined)
  }, [])

  const logOut = e => {
    e.preventDefault()
    authService.logout()
    setCurrentUser(undefined)
  }

  const handleLogin = (user) => {
    setCurrentUser(user)
  }

  return (
    <div>
      <Navbar bg="dark" variant="dark" expand="sm">
        <Navbar.Brand href="/">Mr. Tea</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar">
          <Nav className="mr-auto">
            <NavLink className="nav-link" activeClassName="active" to="/readingTracker">Reading Tracker</NavLink>
            <NavLink className="nav-link" activeClassName="active" to="/help">Help</NavLink>
          </Nav>
          <Nav className="ml-auto">
            {currentUser ? ([
              <NavLink key="menu-profile" className="nav-link" activeClassName="nav-link active" to="/profile">{currentUser.username}</NavLink>,
              <NavLink key="menu-logout" className="nav-link" activeClassName="nav-link active" to="#" onClick={logOut}>Log out</NavLink>
            ]):([
                <InputGroup key="menu-quick-access" size="sm" className="ql-container ">
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Quick access code"/>
                </InputGroup>
              ,
                <AuthPanel onLogin={handleLogin} key="menu-login"/>
            ])}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default TopMenu;