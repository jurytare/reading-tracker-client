import React, { useState } from 'react';
import { navigate } from 'hookrouter';

import { useDispatch, useSelector } from 'react-redux';

import { Button, CircularProgress, Container, List, ListItem } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import BookCard from '../component/Info/BookCard';
import BookEditor from '../component/Info/BookEditor';
import { selectBookTitles, selectBookTitlesStatus } from '../service/reducers/bookTitle/selector';
import { getBookTitles } from '../service/reducers/bookTitle';

export default function Library(props) {

  const [CreationMode, setCreationMode] = useState(0);

  const dispatch = useDispatch();
  const bookList = useSelector(selectBookTitles);
  const blStatus = useSelector(selectBookTitlesStatus);

  React.useEffect(() => {
    dispatch(getBookTitles(10));
  }, [dispatch])

  const handleChangeMode = ev => {
    ev.preventDefault();
    setCreationMode(1);
  }

  const handleSelect = bid => {
    navigate('/book', false, {bid: bid});
  }

  const handleCreateBook = (book) => {
    // Update list needed
    navigate('/book', false, {bid: book._id})
  }

  if(CreationMode) {
    return (
      <Container>
        <BookEditor 
          onCancel={() => setCreationMode(0)}
          onSave={handleCreateBook}
        />
      </Container>
    )
  }

  return (
    <Container>
      <Button color="secondary" onClick={handleChangeMode}>
        <AddIcon /> 
      </Button>
      
    { blStatus === 'ready' ?
      <List>{
        bookList.map((book) => (
          <ListItem key={"title"+book._id}>
            <BookCard Book={book} onSelect={handleSelect}/>
          </ListItem>
        ))
      }</List>
      : <CircularProgress />
    }
    </Container>
  )
}