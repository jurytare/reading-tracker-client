import React from 'react';
import { convertInt2TimeStr } from '../../utils/Utils';

function TimeInfo(props) {

  const [day, time] = convertInt2TimeStr(props.Time)
  var TimeStr = day+"T"+time

  const handleTimeChange = e => {
    let date = new Date(e.target.value.replace("T", " "))
    if(props.onChange) props.onChange(date.getTime())
  }

  return (
    <div>
      {props.isEditing ?
      < input type="datetime-local" step="1" value={TimeStr} onChange={handleTimeChange}/>
      :
      < label > {": " + TimeStr.replace("T", " ")}</label >
      }
    </div>
  )
}

export default TimeInfo;
