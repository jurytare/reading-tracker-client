import { reducersTable } from "../reducer.utils";

export const selectReadTitles = state => {
  let bookList = state[reducersTable.READTITLE]
  return bookList.data || [];
}

export const selectReadTitlesStatus = state => {
  return state[reducersTable.READTITLE].status;
}

// export const selectCurrentTitle =  state => {
//   let bookList = state[reducersTable.TITLE];
//   if( bookList.currentId < 0 || bookList.currentId >= bookList.data.length) {
//     return undefined;
//    } else {
//     return bookList.data[bookList.currentId];
//    };
// }

// export const selectCurrentTitleId =  state => {
//   return state[reducersTable.TITLE].currentId;
// }

export const selectCurrentBookId = state => {
  let readList = state[reducersTable.READTITLE];
  if (readList.currentId < 0 || readList.currentId >= readList.data.length) {
    return undefined;
  } else {
    return readList.data[readList.currentId]._id;
  };
}

export const selectCurrentReadBookId =  state => {
  let bookList = state[reducersTable.READTITLE];
  return bookList.currentId < 0 ? undefined : bookList.data[bookList.currentId].rbid;
}