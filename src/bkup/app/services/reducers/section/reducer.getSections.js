import sectionRecordApi from "../../api/sectionRecord.api";

const getSectionsReducer = async (rbid) => {
  const response = await sectionRecordApi.getList(rbid);
  return response.data;
}

getSectionsReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.sid = -1;
  state.status = 'succeeded';
}
getSectionsReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { getSectionsReducer };