import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { loadSectionListReducer } from './reducer.loadSectionList';
import { loadSectionByBidReducer } from './reducer.loadSectionsByBid';

const sliceName = 'sectionList';

const initialState = {
  data: {},
  currentId: -1,
  status: 'empty',
  error: null,
}

const loadSectionList = createAsyncThunk(sliceName + '/loadSectionList', loadSectionListReducer);
const loadSectionByBid = createAsyncThunk(sliceName + '/loadSectionByBid', loadSectionByBidReducer);

const sectionListSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    pickSection: (state, action) => {
      const { id } = action.payload;
      state.currentId = id;
    },
    unPickSection: (state) => {
      state.currentId = -1;
    },
    unloadSectionList: (state) => {
      state.data = initialState.data;
      state.status = initialState.status;
      state.error = initialState.error;
    },
    addSection: (state, action) => {
      if(state.status === 'ready') {
        state.data.sections.push(action.payload);
      }
    },
    editSection: (state, action) => {
      if(state.status === 'ready') {
        const {sid, section} = action.payload;
        let sections = state.data.sections || [];
        let index;
        if(typeof sid !== 'undefined') {
          index = sections.findIndex(sec => sec._id === sid);
        }else{
          index = sections.findIndex(sec => sec._id === section._id);
        }
        if(index >= 0 && index < sections.length) {
          if(section) {
            state.data.sections[index] = section;
          }else{
            state.data.sections.splice(index, 1);
          }
        }
      }
    }
  },
  extraReducers: {
    ...addTrackingFetch(loadSectionList, loadSectionListReducer),
    ...addTrackingFetch(loadSectionByBid, loadSectionByBidReducer),
  },
})

export default sectionListSlice.reducer;
export const { pickSection, unPickSection, unloadSectionList, addSection, editSection } = sectionListSlice.actions;
export { loadSectionList, loadSectionByBid };
