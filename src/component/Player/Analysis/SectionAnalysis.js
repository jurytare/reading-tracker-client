import { ReadingState } from "../../common/utils";

function readingTime(recList) {
  let offTime, totalTime;
  offTime = 0;
  totalTime = recList[recList.length - 1].time;
  recList.forEach(rec => {
    if(offTime) {
      totalTime -= (rec.time - offTime);
      offTime = 0;
    }
    if(rec.state === ReadingState.PAUSED) {
      offTime = rec.time;
    }
  })
  return totalTime
}

export class SectionAnalysis {
  constructor(section) {
    this.rawSec = section;
    this.baseTime = section.date;
    this.eventList = [
      {
        state: ReadingState.READING,
        time: 0,
        page: section.page,
      },
      ...section.recs,
    ]
    this.readingTime = readingTime(this.eventList);
  }

  getLatestTime() {
    return this.baseTime + this.eventList[this.eventList.length - 1].time;
  }

  getLatestState() {
    return this.eventList[this.eventList.length - 1].state;
  }

  getReadingRealTime() {
    let readTime = this.readingTime;
    let lastRec = this.eventList[this.eventList.length - 1];
    if(lastRec.state === ReadingState.READING || lastRec.state === ReadingState.RESUME) {
      readTime += (new Date().getTime() - this.baseTime - lastRec.time);
    }
    return readTime;
  }

  getLatestReadPage() {
    let page = this.eventList[0].page;
    for (let i = this.eventList.length - 1; i > 0; i--) {
      const rec = this.eventList[i];
      if(rec.state === ReadingState.READING || rec.state === ReadingState.IDLE) {
        page = rec.page;
        break;
      }
    }
    return page;
  }

  getReadPage() {
    return this.getLatestReadPage() - this.eventList[0].page;
  }

  getHourAnalysis(result) {
    let data = Array.isArray(result) ? result : [];
    makeStatistics(this.eventList, this.baseTime, (date, time) => {
      let minId = date.getMinutes();
      let tmp = data.find(element => element.min === minId);
      if(tmp) {
        tmp.time += Math.floor(time/1000);
      }else{
        data.push({
          min: minId,
          time: Math.floor(time/1000),
        })
      }
    });
  }

  getDayAnalysis(result) {
    let data = Array.isArray(result) ? result : [];
    makeStatistics(this.eventList, this.baseTime, (date, time) => {
      let hourId = date.getHours();
      let tmp = data.find(element => element.hour === hourId);
      if(tmp) {
        tmp.time += Math.floor(time/1000);
      }else{
        data.push({
          hour: hourId,
          time: Math.floor(time/1000),
        })
      }
    });
  }

  getWeekAnalysis(result) {
    let data = Array.isArray(result) ? result : [];
    makeStatistics(this.eventList, this.baseTime, (date, time) => {
      let dayId = date.getDay();
      let tmp = data.find(element => element.day === dayId);
      if(tmp) {
        tmp.time += Math.floor(time/1000);
      }else{
        data.push({
          day: dayId,
          time: Math.floor(time/1000),
        })
      }
    });
  }
  
  getMonthAnalysis(result) {
    let data = Array.isArray(result) ? result : [];
    makeStatistics(this.eventList, this.baseTime, (date, time) => {
      let dateId = date.getDate();
      let tmp = data.find(element => element.date === dateId);
      if(tmp) {
        tmp.time += Math.floor(time/1000);
      }else{
        data.push({
          date: dateId,
          time: Math.floor(time/1000),
        })
      }
    });
  }
}

const mSecPerMin = 60000

function makeStatistics(recList, baseTime, callback) {
  let offTime, preTs, toMin;
  let date;

  preTs = offTime = 0;
  // baseTime -= (new Date().getTimezoneOffset() * mSecPerMin);

  toMin = (Math.floor(baseTime / mSecPerMin) + 1) * mSecPerMin - baseTime;

  date = new Date(baseTime);
  // data = Array.isArray(result) ? result : [];

  for (let i = 0; i < recList.length; i++) {
    const rec = recList[i];
    if(offTime) {
      offTime = 0;
      preTs = rec.time;
      while(toMin <= preTs) toMin += mSecPerMin;
    }else{
      while(preTs < rec.time) {
        let time;
        if(rec.time < toMin) {
          time = rec.time - preTs;         
          preTs = rec.time;
        }else{
          time = toMin - preTs;
          date = new Date(toMin + baseTime);
          preTs = toMin;
          toMin += mSecPerMin;
        }

        callback(date, time);

        // let minId = time.getMinutes();
        // let tmp = data.find(element => element.min === minId);
        // if(tmp) {
        //   tmp.time += Math.floor(amount/1000);
        // }else{
        //   data.push({
        //     min: minId,
        //     time: Math.floor(amount/1000),
        //   })
        // }
      }
    }
    if(rec.state === ReadingState.PAUSED) {
      offTime = rec.time;
    }
  }
}
