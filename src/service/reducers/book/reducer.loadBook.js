import BookApi from "../../api/book.api";

const loadBookReducer = async (bid) => {
  const response = await BookApi.get(bid);
  return response.data;
}

loadBookReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.data.dateCreated = action.payload.dateCreated * 1000;
  state.status = 'ready';
  state.error = null;
}

loadBookReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadBookReducer };