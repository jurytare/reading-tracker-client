import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadRecords, loadSections, unloadRecords } from '../../reducers/section';
import { selectCurrentSID } from '../../reducers/section/selector';
import { selectCurrentReadBookId } from '../../reducers/title/selector';
import AnalysisOverall from './AnalysisOverall';
import RecordReview from './RecordReview';
import SectionAnalysis from './SectionAnalysis';
import SectionList from './SectionList';

function ReadingRecord(props) {

  const rbid = useSelector(selectCurrentReadBookId);
  const dispatch = useDispatch();

  useEffect(() => {
    if(rbid){
      dispatch(loadSections(rbid));
    }
  }, [rbid, dispatch])

  const AddAndRead = event => {
    console.log('Trying to add & read')
    event.preventDefault();
  }

  return rbid ?
    <div>
      <AnalysisOverall />
      <SectionList />
      <SectionAnalysis/>
      <RecordReview/>
    </div>
    :
    <div>
      <p>Your bookshelf does not have this book. Click
          <a href='#read' onClick={AddAndRead}> reading </a>
          to acquire and read it</p>
    </div>
}

export default ReadingRecord;
