export const REC_STATE = {
    Idle: 0,
    Reading: 1,
    Paused: 2, 
    Stopped: 3,
    isPaused: 4,
    Finish: 5,
  }

  export const REC_EVENT = {
    READ: 'READ',
    PAUSE: 'PAUSE',
    STOP: 'STOP',
    RESUME: 'RESUME',
    UPDATE: 'UPDATE',
    END: 'END',
  }
  