import React from 'react';
import { Container } from 'react-bootstrap';
import picLogin from './help-pics/login.PNG';
import mainPage from './help-pics/main-page.PNG';
import createBook from './help-pics/create-book.PNG';
import readyBook from './help-pics/book-ready.PNG';
import prepareRead from './help-pics/prepare-reading.PNG';
import readingFirst from './help-pics/reading-first.PNG';
import readingSecond from './help-pics/reading-second.PNG';
import pauseRead from './help-pics/pause-reading.PNG';
import stopRead from './help-pics/stop-reading.PNG';
import processRead from './help-pics/reading-process.PNG';

function Help() {

  return (
    <Container>
      <h4><b>Phần mềm hỗ trợ đánh giá việc đọc sách</b></h4>
      <br/>
      
      <h5><b>1. Giới thiệu</b></h5>
      <p>Phần mềm được xây dựng nhằm hỗ trợ người đọc sách dễ dàng giám sát, đánh giá hiệu quả đọc sách của họ. Từ những kết quả này, người yêu sách sẽ có động lực để nâng cao kĩ năng đọc sách của mình.</p>
      <p>Đây là phiên bản sơ khai (0.1.0) để diễn tả các tính năng chính của phần mềm. Do vậy chương trình chưa được hoàn chỉnh về trải nghiệm người dùng. Trong quá trình sử dụng, người yêu sách có góp ý gì thì hãy gửi về địa chỉ email: jurytare@gmail.com</p>
      
      <h5><b>2. Hướng dẫn sử dụng</b></h5>
      <h6><b>2.1 Đăng nhập</b></h6>
      <p>Đọc sách là công việc cá nhân. Do vậy mọi hoạt động của bạn trên phần mềm sẽ được lưu giữ và bảo mật theo từng tài khoản riêng biệt.</p>
      <p>Để đăng nhập thì bạn có thể truy cập vào menu góc phải</p>
      <img src={picLogin} alt="login"/>
      <p>Sau khi đăng nhập thành công, trang web cần tải lại (bấm vào Mr. Tea) để lấy thông tin của riêng mình</p>
      <Container><img src={mainPage} alt="main-page"/></Container>
      
      <h6><b>2.2 Tạo sách</b></h6>
      <ul>
        <li>Click vào + bên cạnh ô tìm kiếm</li>
        <li>Điền thông tin sách</li>
        <li>Bấm nút tick (v)</li>
      </ul>
      <img src={createBook} alt="create-book"/>
      <br/><br/>
      <p>Sau khi tạo sách xong, sách của bạn sẽ xuất hiện trong danh sách truy cập</p>
      <img src={readyBook} alt="ready-book" />
      <p><i>Bạn có thể bấm vào nút Play để bắt đầu theo dõi việc đọc sách</i></p>

      <h6><b>2.3 Theo dõi đọc sách</b></h6>
      <p>{'Quá trình đọc sách được chia làm các giai đoạn: Bắt đầu đọc --> Tạm dừng --> Tiếp tục đọc --> Dừng --> Lưu dữ liệu'}</p>
      <p><u>2.3.1 Bắt đầu đọc:</u> Nhập số trang để bắt đầu và bấm nút Read</p>
      <img src={prepareRead} alt="prepare-first"/>
      <p>Nếu đọc sách lần đầu, các thông số trong trang web sẽ chưa hiển thị rõ. Trong quá trình đọc sách, thỉnh thoảng (không cần liên tục) vào trình duyệt để cập nhật số trang bạn đang đọc nhé. Cập nhật số trang đang đọc bằng cách nhập số trang và nhấn Enter.</p>
      <img src={readingFirst} alt="read-first"/>
      <p>Từ lần cập nhật thứ 2 trở đi, các thông số sẽ trở nên rõ ràng hơn với các thông số hữu ích</p>
      <img src={readingSecond} alt="read-second"/>
      <ul>
        <li>Elapsed time: Tổng thời gian từ lúc bắt đầu bấm nút Read bắt đầu đọc sách</li>
        <li>Progress: Tiến độ hoàn thành việc đọc sách</li>
        <li>Last speed: Tốc độ đọc (thời gian / 1 trang) của lần cập nhật trang đang đọc mới nhất</li>
        <li>Average speed: Tốc độ đọc (thời gian / 1 trang) của toàn bộ quá trình đọc (bao gồm thời gian tạm dừng)</li>
        <li>ETA: Dự đoán thời gian hoàn thành (100%) quyển sách</li>
      </ul>

      <p><u>2.3.2 Tạm dừng:</u> Bấm nút Read</p>
      <p>Khi có sự kiện bên ngoài khiến bạn tạm dừng quá trình đọc sách, nút Pause sẽ trở nên hữu ích. Khi ở trạng thái tạm dừng, bạn có thể quay lại trạng thái đọc bằng cách nhập số trang quay lại và bấm nút Continue</p>
      <img src={pauseRead} alt="pause-read"/>

      <p><u>2.3.3 Dừng đọc sách:</u> Nhập số trang và bấm nút Stop</p>
      <img src={stopRead} alt="stop-read"/>
      <p>Sau khi bấm vào Stop, cửa sổ tiếp theo xuất hiện để hỏi về việc lưu hay không lưu thông tin của toàn bộ quá trình đọc sách. Sau khi lưu xong, trang web sẽ tự động trả về trạng thái sẵn sàng cho lần đọc sách tiếp theo</p>
      <img src={processRead} alt="reading-process"/>
      <p>Ý nghĩa các thông số:</p>
      <ul>
        <li>Total progress time: Tổng thời gian tính từ khi bạn bắt đầu đọc sách (tính từ lần đọc sách đầu tiên)</li>
        <li>Average progress speed: Tốc độ đọc sách (thời gian / 1 trang) tính theo Total progress time / Process</li>
        <li>ETA of Progress: Dự đoán thời gian sẽ hoàn thành quyển sách</li>
        <li>Total reading time: Tổng thời gian bạn tập trung đọc sách (không tính thời gian nghỉ & tạm dừng)</li>
        <li>Average reading speed: Tốc độ đọc sách khi bạn tập trung</li>
        <li>ETA if read now: Dự đoán thời gian hoàn thành quyển sách khi bạn tập trung đọc sách liên tục ngay từ bây giờ</li>
      </ul>
    </Container>
  );
}

export default Help;
