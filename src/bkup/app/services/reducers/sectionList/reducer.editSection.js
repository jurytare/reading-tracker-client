import sectionRecordApi from "../../api/sectionRecord.api";

const editSectionReducer = async (bookId, secId) => {
  const response = await sectionRecordApi.delete(bookId, secId);
  return response.data;
}

editSectionReducer.fulfilledCallback = (state, action) => {
  state.data = {};
  state.status = 'succeeded';
}
editSectionReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { editSectionReducer };