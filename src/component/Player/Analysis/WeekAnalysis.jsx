import React from 'react';
import { Chart } from "react-google-charts";
import { ReadAnalysis } from './ReadAnalysis';

export default function WeekAnalysis(props) {

  const sections = props.Sections || [];
  let SecAlys = new ReadAnalysis(sections);
  let timeData = SecAlys.getWeekAnalysis();
  const dayStr = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

  let dataChart = [['Time reading', 'Time (s)']];
  for (let i = 0; i < dayStr.length; i++) {
    let tmp = timeData.find(dat => dat.day === i);
    if (tmp) {
      dataChart.push([dayStr[i], tmp.time]);
    }else{
      dataChart.push([dayStr[i], 0])
    }
  }

  return (
  <Chart
    width={600}
    height={300}
    chartType="ColumnChart"
    loader={<div>Loading Chart</div>}
    data={dataChart}
    options={{
      title: 'The most day in a week for reading',
      chartArea: { width: '50%' },
      hAxis: {
        title: 'Day in week',
        minValue: 0,
        maxValue: 7,
        viewWindow: {
          max: 7,
        }
      },
      vAxis: {
        title: 'Time reading (seconds)',
      },
    }}
    legendToggle
  />)
}

// const timeData = [
//    {day: 3, time: 200, },
//    {day: 12, time: 4000, },
//    {day: 17, time: 9432, },
//    {day: 24, time: 4, },
//    {day: 29, time: 100, },
//    {day: 34, time: 3493, },
//    {day: 48, time: 123, },
// ]