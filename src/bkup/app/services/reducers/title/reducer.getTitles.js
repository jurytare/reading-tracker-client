import BookApi from "../../api/BookApi";
import bookReadApi from "../../api/bookRead.api";

const getTitlesReducer = async (numberOfTitles) => {
  // If data is available, return here
  let resp, bookList = [];
  resp = await bookReadApi.getList();
  bookList = resp.data;
  resp = await BookApi.getTitles(numberOfTitles);
  resp.data.forEach(bookTitle => {
    if(!bookList.find(tit => tit._id === bookTitle._id)) {
      bookList.push(bookTitle);
    }
  });
  return bookList;
}

getTitlesReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}

getTitlesReducer.rejectedCallback = (state, action) => {
  state.data = [];
  state.status = 'failed';
  state.error = action.error.message;
}

export { getTitlesReducer };