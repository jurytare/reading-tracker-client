import sectionRecordApi from "../../api/sectionRecord.api";
import { normalizeSection } from "../reducer.utils";

const loadSectionListReducer = async (rbid) => {
  const response = await sectionRecordApi.getList(rbid);
  return response;
}

loadSectionListReducer.fulfilledCallback = (state, action) => {
  state.data.rbid = action.payload.rbid;
  state.data.pages = action.payload.pages;
  if(Array.isArray(action.payload.sections)){
    state.data.sections = action.payload.sections
  }else{
    state.data.sections = [];
  }

  state.currentId = state.data.length - 1;
  state.status = 'succeeded';
}
loadSectionListReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadSectionListReducer };