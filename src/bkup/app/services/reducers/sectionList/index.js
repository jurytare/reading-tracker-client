import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { delSectionReducer } from './reducer.delSection';
import { addSectionReducer } from './reducer.addSection';
import { loadSectionListReducer } from './reducer.loadSectionList';

const sliceName = 'sectionList';

const initialState = {
  data:[],
  currentId: -1,
  status: 'empty',
  error: null,
}

const loadSectionList = createAsyncThunk(sliceName + '/loadSectionList', loadSectionListReducer);
const addSection = createAsyncThunk(sliceName + '/addSection', addSectionReducer);
const delSection = createAsyncThunk(sliceName + '/delSection', delSectionReducer);

const sectionListSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    pickSection: (state, action) => {
      const { id } = action.payload;
      state.currentId = id;
    },
    unPickSection: (state) => {
      state.currentId = -1;
    },
    unloadSectionList: (state) => {
      state.data = initialState.data;
      state.status = initialState.status;
      state.error = initialState.error;
    },
    changeSectionList: (state, action) => {
      const { id, sid, section } = action.payload;

    }
  },
  extraReducers: {
    ...addTrackingFetch(addSection, addSectionReducer),
    ...addTrackingFetch(delSection, delSectionReducer),
    ...addTrackingFetch(loadSectionList, loadSectionListReducer),
  },
})

export default sectionListSlice.reducer;
export const { pickSection, unPickSection, unloadSectionList } = sectionListSlice.actions;
export { loadSectionList, addSection, delSection };