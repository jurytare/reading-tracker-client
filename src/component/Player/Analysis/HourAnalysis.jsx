import React from 'react';
import { Chart } from "react-google-charts";
import { ReadAnalysis } from './ReadAnalysis';

export default function HourAnalysis(props) {

  const sections = props.Sections || [];
  let SecAlys = new ReadAnalysis(sections);
  let timeData = SecAlys.getHourAnalysis();

  let dataChart = [['Time reading', 'Time (s)']];
  for (let i = 0; i < 60; i++) {
    let tmp = timeData.find(dat => dat.min === i);
    if (tmp) {
      dataChart.push([i, tmp.time]);
    }else{
      dataChart.push([i, 0])
    }
  }

  return (
  <Chart
    width={600}
    height={300}
    chartType="ColumnChart"
    loader={<div>Loading Chart</div>}
    data={dataChart}
    options={{
      title: 'The most minute in a hour for reading',
      chartArea: { width: '70%' },
      hAxis: {
        title: 'Minute in hour',
        minValue: 0,
        maxValue: 59,
      },
      vAxis: {
        title: 'Time reading (seconds)',
      },
    }}
    legendToggle
  />)
}