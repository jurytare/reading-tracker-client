import sectionRecordApi from "../../api/sectionRecord.api";
import { normalizeSection } from "../reducer.utils";

const loadSectionListReducer = async (rbid) => {
  const response = await sectionRecordApi.getList(rbid);
  return response.data;
}

loadSectionListReducer.fulfilledCallback = (state, action) => {
  if(Array.isArray(action.payload)){
    state.data = action.payload.map(section => {
      section = normalizeSection(section, 1000);
      return section;
    })
  }else{
    state.data = [];
  }
  state.currentId = state.data.length - 1;
  state.status = 'succeeded';
}
loadSectionListReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadSectionListReducer };