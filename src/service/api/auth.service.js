import axios from "axios";
import { setAuthToken } from "./AxiosService";
import { baseUrl } from "./service.const";

const register = (uid, email, password) => {
  return axios.post(baseUrl + "/auth/signup", {
    uid,
    email,
    password,
  });
};

const login = (uid, password) => {
  return axios.post(baseUrl + "/auth/signin", {
      uid,
      password,
    }).then(resp => {
      setAuthToken(resp.data.accessToken);
      return resp;
    })
};

const logout = () => {
  setAuthToken(null);
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};
