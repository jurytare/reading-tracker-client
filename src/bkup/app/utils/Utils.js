export function convertInt2TimeStr(timeNum) {
  let dateTime = new Date(timeNum)
  let dayStr = dateTime.getFullYear()
    + "-" + ("0" + (dateTime.getMonth() + 1)).slice(-2)
    + "-" + ("0" + dateTime.getDate()).slice(-2)
  let timeStr = ("0" + dateTime.getHours()).slice(-2)
    + ":" + ("0" + dateTime.getMinutes()).slice(-2)
    + ":" + ("0" + dateTime.getSeconds()).slice(-2)

  return [dayStr, timeStr]
}

export function recordGenerator (state, page, time) {
  let now = new Date();
  let rec = {};
  rec._id = -now.getTime();
  state !== null && (rec.state = state);
  page !== null && (rec.page = page);
  time !== null && (rec.time = time);
  
  return rec;
}

export function milis2DayHourMinSecStr(from, to) {
  let sec, min, hour, days;

  if(arguments.length < 2) {
    to = from;
    from = 0;
  }

  sec = Math.floor((to - from)/1000)
  days = Math.floor(sec / 86400);
  sec = sec % 86400;
  hour = Math.floor(sec / 3600);
  sec = sec % 3600;
  min = Math.floor(sec / 60);
  sec = sec % 60;

  return (days ? days + "d ":"") + 
    (hour? hour + "h ":"") + 
    (min? min + "m ":"") + sec + "s "
}

export function encodeNum(number, base) {
  const code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  if (!base) {
    base = code.length - 1
  } else if (base >= code.length || base === 1) {
    return -1
  }
  let res = ""
  while (number) {
    res = code[number % base] + res
    number = Math.floor(number / base)
  }
  return res
}

export function decodeNum(data, base) {
  const code = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  if (!base) {
    base = code.length - 1
  } else if (base >= code.length || base === 1) {
    return -1
  }
  let result = 0
  let res = 0
  let ch, i = 0;
  while (i < data.length) {
    ch = data.charAt(i)
    res = code.indexOf(ch)
    if (res < 0) {
      console.log("Unable to decode")
      return -1
    } else {
      result = result * base + res
    }
    i++
  }
  return result
}

export function binarySearch(arr, val, compFunc) {
  if (!arr) return -2
  if (arr.length === 0) return 0
  let nextID, lowRange, highRange, testVal
  nextID = lowRange = 0
  highRange = arr.length - 1
  if (compFunc(val, arr[highRange]) > 0) {
    return arr.length
  } else if (compFunc(val, arr[lowRange]) < 0) {
    return -1
  }

  while (highRange > lowRange) {
    nextID = ((highRange + lowRange) >> 1)
    // console.log("Check "+nextID+" in "+lowRange+" to "+highRange)
    testVal = compFunc(val, arr[nextID])
    if (testVal > 0) {
      if (nextID === lowRange) {
        return nextID
      }
      lowRange = nextID
    } else if (testVal < 0) {
      highRange = nextID
    } else {
      break
    }
  }
  return nextID
}