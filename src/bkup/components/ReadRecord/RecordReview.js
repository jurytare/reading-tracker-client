import React from 'react';
import { useSelector } from 'react-redux';
import { selectCurrentSection } from '../../reducers/section/selector';

function RecordReview(props) {

  const section = useSelector(selectCurrentSection);

  return (
    <div>
      <table>
        <tbody>
          {
            section ? 
            section.recs.map(rec => (
              <tr key={rec._id}>
                <td><b>Time</b>:</td>
                <td>{new Date((rec.time + section.date) * 1000).toLocaleString()}</td>
                <td> --- </td>
                <td><b>Page</b>:</td>
                <td>{rec.page}</td>
              </tr>
            )) : null
          }
        </tbody>
      </table>
    </div>
  );
}

export default RecordReview;