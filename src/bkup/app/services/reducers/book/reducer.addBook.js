import bookReadApi from "../../api/bookRead.api";

const addBookReducer = async (bookId) => {
  const response = await bookReadApi.add(bookId);
  return response.data;
}

addBookReducer.fulfilledCallback = (state, action) => {
  state.data._id = action.payload;
  state.status = 'succeeded';
}
addBookReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addBookReducer };