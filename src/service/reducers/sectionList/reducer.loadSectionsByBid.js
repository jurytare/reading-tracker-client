import sectionRecordApi from "../../api/sectionRecord.api";
import { normalizeSection } from "../reducer.utils";

const loadSectionByBidReducer = async (bid) => {
  const response = await sectionRecordApi.getListFromBid(bid);
  return response;
}

loadSectionByBidReducer.fulfilledCallback = (state, action) => {
  state.data.rbid = action.payload.rbid;
  state.data.pages = action.payload.pages;
  if(Array.isArray(action.payload.sections)){
    state.data.sections = action.payload.sections
  }else{
    state.data.sections = [];
  }

  state.currentId = state.data.length - 1;
  state.status = 'ready';
  state.error = null;
}
loadSectionByBidReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadSectionByBidReducer };