import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { addBookReducer } from './reducer.addBook';
import { delBookReducer } from './reducer.delBook';
import { editBookReducer } from './reducer.editBook';
import { loadBookReducer } from './reducer.loadBook';

const sliceName = 'bookInfo';

const initialState = {
  data:{
    _id: '',
    name: '',
    pages: 0,
  },
  status: 'empty',
  error: null,
}

const loadBook = createAsyncThunk(sliceName + '/loadBook', loadBookReducer);
const editBook = createAsyncThunk(sliceName + '/editBook', editBookReducer);
const addBook = createAsyncThunk(sliceName + '/addBook', addBookReducer);
const delBook = createAsyncThunk(sliceName + '/delBook', delBookReducer);

const bookSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    unloadBook: (state, action) => {
      state.data = initialState;
    }
  },
  extraReducers: {
    ...addTrackingFetch(loadBook, loadBookReducer),
    ...addTrackingFetch(addBook, addBookReducer),
    ...addTrackingFetch(delBook, delBookReducer),
    ...addTrackingFetch(editBook, editBookReducer),
  },
})

export default bookSlice.reducer;
export const { unloadBook } = bookSlice.actions;
export { loadBook, addBook, delBook, editBook };
