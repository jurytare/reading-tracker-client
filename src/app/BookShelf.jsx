import React, { useState } from 'react';
import { navigate } from 'hookrouter';

import { useDispatch, useSelector } from 'react-redux';

import { Button, CircularProgress, Container, List, ListItem } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import BookCard from '../component/Info/BookCard';
import { getReadTitles } from '../service/reducers/readTitle';
import { selectReadTitles, selectReadTitlesStatus } from '../service/reducers/readTitle/selector';
import BookEditor from '../component/Info/BookEditor';

export default function BookShelf(props) {

  const [CreationMode, setCreationMode] = useState(0);

  const dispatch = useDispatch();
  const readList = useSelector(selectReadTitles);
  const rlStatus = useSelector(selectReadTitlesStatus);

  React.useEffect(() => {
    dispatch(getReadTitles());
  }, [dispatch])

  console.log(rlStatus, readList)
  const handleChangeMode = ev => {
    ev.preventDefault();
    setCreationMode(1);
  }

  const handleSelect = bid => {
    navigate('/book', false, {bid: bid});
  }

  const handleCreateBook = (book) => {
    // Update list needed
    navigate('/book', false, {bid: book._id})
  }

  if(CreationMode) {
    return (
      <Container>
        <BookEditor 
          onCancel={() => setCreationMode(0)}
          onSave={handleCreateBook}
        />
      </Container>
    )
  }

  return (
    <Container>
      <Button color="secondary" onClick={handleChangeMode}>
        <AddIcon /> 
      </Button>
      
    { rlStatus === 'ready' ?
      <List>{
        readList.map((book) => (
          <ListItem key={"title"+book._id}>
            <BookCard Book={book} onSelect={handleSelect}/>
          </ListItem>
        ))
      }</List>
      : <CircularProgress />
    }
    </Container>
  )
}