import bookApi from "../../api/book.api";

const addBookReducer = async (bid) => {
  const response = await bookApi.add(bid);
  return response.data;
}

addBookReducer.fulfilledCallback = (state, action) => {
  state.data._id = action.payload;
  state.status = 'succeeded';
}
addBookReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addBookReducer };