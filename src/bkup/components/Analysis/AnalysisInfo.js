import React from 'react';
import { VIEW_MODE } from '../../const/info.const';
import { convertInt2TimeStr } from '../../utils/Utils';

function second2Time(second) {
  let hour = Math.floor(second / 3600)
  second = second % 3600
  let minute = Math.floor(second / 60)
  second = second % 60
  return [hour, minute, second]
}

function AnalysisInfo(props) {
  // const { bookInfo, records } = useContext(BookContext)

  var bookInfo = props.BookInfo || {}
  var records = props.Records

  if(records.length) {
    records = records.slice(0).reverse()
  }

  var startRec = undefined

  var analysisData = records.map(rec => {
    let processStr, durationStr

    let [day, time] = convertInt2TimeStr(rec.date)
    let timeStr = day + " " + time

    if (startRec) {
      let duration_sec = Math.floor((rec.date - startRec.date) / 1000)
      let [hour, minute, second] = second2Time(duration_sec)
      let process = (rec.page / bookInfo.pages) * 100

      durationStr =
        (hour ? hour.toString() + "h " : "") +
        (minute ? minute.toString() + "m " : "") +
        (second ? second.toString() + "s " : "")

      processStr = (process.toFixed(2)).toString() + "%"
      if ((rec.type & VIEW_MODE.isBeginBit) === 0) {
        startRec = undefined
      }
    }else{
      if(rec.type & VIEW_MODE.isBeginBit){
        startRec = rec
      }
      durationStr = ">>"
      processStr = ">>"
    }
    return {
      time: timeStr,
      page: rec.page,
      duration: durationStr,
      process: processStr,
    }

  })

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th><strong>Time</strong></th>
            <th><strong>Page</strong></th>
            <th><strong>Duration</strong></th>
            <th><strong>Process</strong></th>
          </tr>
        </thead>
        <tbody>
          {
            analysisData.map((dat, id) => {
              return (
                <tr key={"aly-" + id}>
                  <td>{dat.time}</td>
                  <td>{dat.page}</td>
                  <td>{dat.duration}</td>
                  <td>{dat.process}</td>
                </tr>
              )
            })
          }
        </tbody>

      </table>
    </div>
  );
}

export default AnalysisInfo;