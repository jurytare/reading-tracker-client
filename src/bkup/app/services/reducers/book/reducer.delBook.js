import bookReadApi from "../../api/bookRead.api";

const delBookReducer = async (bookId) => {
  const response = await bookReadApi.delete(bookId);
  return response.data;
}

delBookReducer.fulfilledCallback = (state, action) => {
  state.data = {};
  state.status = 'succeeded';
}
delBookReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { delBookReducer };