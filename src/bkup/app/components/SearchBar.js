import React, { useRef } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';


function SearchBar(props) {

  const barInput = useRef(undefined);

  const handleChangeFilter = event => {
    event.preventDefault();
    if(typeof props.onSearching === 'function') {
      props.onSearching(event.target.value);
    }
  }

  const handleSearch = event =>  {
    event.preventDefault();
    let searchContent = barInput.current.value;

    if(typeof props.onSearch === 'function') {
      props.onSearch(searchContent);
    }
  }

  const handleClearFilter = event => {
    barInput.current.value = '';
    handleSearch(event)
  }

  return (
    <div>
      <table>
        <thead>
          <tr>
            <td>
              <button variant="warning" size="sm"
              onClick={props.onAdding || null}
              >
                <FontAwesomeIcon size="xs" icon={faPlus} />
              </button>
            </td>
            <td>
              <input ref={barInput} type="text" onChange={handleChangeFilter} />
            </td>
            <td>
              <button variant="warning" size="sm"
              onClick={handleClearFilter}
              >
                <FontAwesomeIcon size="xs" icon={faTimes} />
              </button>
            </td>
            <td>
              <button variant="warning" size="sm"
                onClick={handleSearch}
              >
                <FontAwesomeIcon size="xs" icon={faSearch} />
              </button>
            </td>
          </tr>
        </thead>
      </table>
    </div>
  );
}

export default SearchBar;