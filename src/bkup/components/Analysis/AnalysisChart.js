import React from 'react';
import {
  ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip,
  Legend,
  ResponsiveContainer, 
} from 'recharts';
import { convertInt2TimeStr } from '../../utils/Utils';

function chartNomalize(chartData) {
  let max_eff = 0
  let max_dur = 0
  let datID = 0
  let time_offset = [0]
  let offValue = []
  chartData.forEach((dat, id) => {
    if(dat.eff > max_eff) {
      max_eff = dat.eff
    }
    if(max_dur < dat.duration) {
      max_dur = dat.duration
    }
    if(datID < dat.id) {
      datID = dat.id
      time_offset.push(max_dur * 1000 - (dat.time - chartData[id -1].time))
      offValue.push(id)
    }
  });
  let ratio_eff = 40 / max_eff

  let normData = chartData.map(dat => {
    // let tmp = "eff"+dat.id
    let newDat = {...dat}
    newDat.eff = newDat.eff * ratio_eff
    newDat.time += time_offset[newDat.id]

    newDat.eff_ratio = ratio_eff
    newDat.time_offset = time_offset[newDat.id]
    newDat.remain = 100 - newDat.process
    return newDat
  })
  offValue.forEach(val => {
    let modVal = {...normData[val - 1]}
    delete modVal.eff
    modVal.time++
    normData.splice(val, 0, modVal)
  })
  return normData
}

function formatTooltip(value, name, props) {
  // console.log("Value:",value)
  // console.log("Name:",name)
  // console.log("object:",props)
  if (typeof (name) === "string") {
    if (name === "process") {
      return [value.toFixed(2) + "%", "Process"]
    } else if (name === "eff") {
      return [(value / props.payload.eff_ratio).toFixed(2) + " pages/min", "Effective"]
    } else {
      return [null, null]
    }
  } else if (typeof (name) === "object") {
    if(name.length){
      value -= name[0].payload.time_offset
      let [day, time] = convertInt2TimeStr(value)
      let timeStr = day + " " + time
      return timeStr
    }else{
      return ""
    }
  }
}

function AnalysisChart(props) {

  var chartData = []

  const formatXAxis = (tickItem, id) => {
    let timeStr = ""
    if(chartData.length){
      const date = new Date(tickItem)
      let hourStr = date.getHours().toString()
      let minuteStr = date.getMinutes().toString()
      timeStr = hourStr + ":" + minuteStr
    }
    return timeStr;
  }

  if (props.Data && props.Data.length) {
    chartData = chartNomalize(props.Data)
    return (
      <ResponsiveContainer         
      height={400}>
      <ComposedChart
        width={800}
        height={400}
        data={chartData}
        margin={{
          top: 20, right: 0, bottom: 20, left: 0,
        }}
      >
        <CartesianGrid stroke="#f5f5f5" />
        <XAxis dataKey="time" scale="time" tickFormatter={formatXAxis} />
        <YAxis />
        <Tooltip formatter={formatTooltip} labelFormatter={formatTooltip}/>
        <Legend />
        <Area key="area-proc" type="monotone" dataKey="process" stackId="1" fill="#8884d8" stroke="#8884d8" />
        {/* <Area key="area-remain" type="monotone" dataKey="remain" stackId="1" fill="#ffff80" stroke="#ffff80" /> */}
        <Area key="area-eff" type="monotone" dataKey="eff" fill="#ffb380" stroke="#ff6600" strokeWidth={2}/>
      </ComposedChart>
      </ResponsiveContainer>
    );
  } else {
    return (<div></div>)
  }
}
export default AnalysisChart;