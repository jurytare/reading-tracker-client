import sectionRecordApi from "../../api/sectionRecord.api";

const delSectionReducer = async (bookId, secId) => {
  const response = await sectionRecordApi.delete(bookId, secId);
  return response.data;
}

delSectionReducer.fulfilledCallback = (state, action) => {
  state.data = {};
  state.status = 'succeeded';
}
delSectionReducer.rejectedCallback = (state, action) => {
  state.status = 'failed';
  state.error = action.error.message;
}

export { delSectionReducer };