import React from 'react';
import Info from './common/Info';

// Status: idle/reading/paused
// Progress: 1/10 (10%)
// Start date: yyyy-mm-dd hh:mm:ss (y m d h m s)
// Progress speed: yy mm did hh mm ss / page
// Expect ETA

export default function ProgressAnalysis(props) {
  return (
    <table>
      <tbody>
        <Info label="Progress" value='' />
        <Info label="Start date" value='' />
        <Info label="Progress speed" value='' />
        <Info label="Expect ETA" value='' />
      </tbody>
    </table>
  )
}