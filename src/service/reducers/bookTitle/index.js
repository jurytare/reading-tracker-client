import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { getBookTitlesReducer } from './reducer.getBookTitles';

const sliceName = 'bookTitles';

const initialState = {
  data:[],
  currentId: -1,
  status: 'empty',
  error: null,
}

const getBookTitles = createAsyncThunk(sliceName + '/getBookTitles', getBookTitlesReducer);

const titleSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    pickBookTitle: (state, action) => {
      const { id } = action.payload;
      state.currentId = id;
    },
    unPickBookTitle: (state, action) => {
      state.currentId = -1;
    }
  },
  extraReducers: {
    ...addTrackingFetch(getBookTitles, getBookTitlesReducer),
  },
})

export default titleSlice.reducer;
export const { pickBookTitle, unPickBookTitle } = titleSlice.actions;
export { getBookTitles };
