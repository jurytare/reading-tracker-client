import React, { createContext, useEffect, useState } from "react"
import api from "./services/api"
import { getData, storeData } from './storage'
import { binarySearch, convertInt2TimeStr, decodeNum } from "./Utils"

export const BookContext = createContext()

export default function BookContextProvider(props) {

  const [bookInfo, setBookInfo] = useState({})
  const [records, setRecords] = useState([])
  
  useEffect(() => {

    if(props.BookId !== ""){
      api.readBook(props.BookId)
        .then(response => {
          console.log("Got", response.data)
          let temp = response.data
          let tsTemp = convertInt2TimeStr(temp.start_date)
          temp.start_date = decodeNum(temp.start_date)*1000
          temp.start_day = tsTemp[0]
          temp.start_time = tsTemp[1]
          setBookInfo(temp)
        })
        .catch(err => {
          console.log("Failed to fetch bookInfo", err)
        })

      api.getAllRecord(props.BookId)
      .then(response => {
        let tmpData = []
        let insertID = 0
        response.data.forEach(rec => {
          rec.date = decodeNum(rec.date)*1000
          insertID = binarySearch(tmpData, rec, (val1, val2) => {
            return val2.date - val1.date
          })
          if(insertID < tmpData.length){
            insertID++
          }
          tmpData.splice(insertID, 0, rec)
        })
        setRecords(tmpData)
      })
      .catch(err => {
        console.log("Failed to fetch records", err)
      })
    }else{
      setBookInfo({})
      setRecords([])
    }
  }, [props.BookId])

  return (
    <BookContext.Provider value={{bookInfo, setBookInfo, records, setRecords}}>
      {props.children}
    </BookContext.Provider>
  )
}