import React, { useState } from 'react';

import { Box, Button, Paper, TextField } from '@material-ui/core';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import ClearOutlinedIcon from '@material-ui/icons/ClearOutlined';

import testImg from './no-image.png';
import { useDispatch, useSelector } from 'react-redux';
import { editBook } from '../../service/reducers/book';
import bookApi from '../../service/api/book.api';
import { selectReadBookId } from '../../service/reducers/sectionList/selector';

function genNewBook() {
  let now = new Date();
  return {
    _id : -now.getTime(),
    name: 'No name',
    author: 'anonymous',
    pages: 1,
    dateCreated: now.getTime(),
  }
}

export default function BookEditor(props) {

  const [Book, setBook] = useState(props.Book || genNewBook());
  const rbid = useSelector(selectReadBookId);
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);

  const handleCancel = event => {
    event.preventDefault();
    typeof props.onCancel === 'function' && props.onCancel(0);

  }

  const handleChange = (ev, target) => {
    let value = ev.target.value;
    
    if(target === 'pages') {
      value = Number(value);
    }
    setBook({
      ...Book,
      [target]: value,
    })
  }

  const handleSave = event => {
    event.preventDefault();
    setLoading(true);
    let changes, fetcher;
    if(props.Book) {
      changes = {};
      Book.name !== props.Book.name && (changes.name = Book.name);
      Book.author !== props.Book.author && (changes.author = Book.author);
      Book.pages !== props.Book.pages && (changes.pages = Book.pages);
      fetcher = bookApi.edit(rbid, changes);
    }else{
      changes = Book;
      fetcher = bookApi.create(changes);
    }
    fetcher.then(resp => {
      typeof props.onSave === 'function' && props.onSave(resp.data);
      setLoading(false);
    }).catch(err => {
      console.log(err);
    })
    setLoading(true);
  }

  return (
    <Box>
      <img alt='test-review-img' src={testImg} />
      <Paper elevation={3}>
        <Box display="flex" flexDirection="row-reverse">
          <Button color="secondary" onClick={handleCancel} disabled={isLoading}>
            <ClearOutlinedIcon />
          </Button>
          <Button color="primary" onClick={handleSave} disabled={isLoading}>
            <SaveOutlinedIcon />
          </Button>
        </Box>
        <Box>
          <TextField fullWidth margin="normal" id="standard-required" 
              label="Name" defaultValue={Book.name}
              onChange={(ev) => handleChange(ev, 'name')}
          />
          <TextField fullWidth margin="normal" id="standard-basic" 
            label="Author" defaultValue={Book.author}
            onChange={(ev) => handleChange(ev, 'author')}
          />
          <TextField fullWidth margin="normal" id="standard-required" 
            type="number" label="Pages" defaultValue={Book.pages}
            onChange={(ev) => handleChange(ev, 'pages')}
          />
        </Box>
      </Paper>
    </Box>
  )
}