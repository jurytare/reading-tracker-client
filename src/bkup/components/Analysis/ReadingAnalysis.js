import React from 'react';
import { VIEW_MODE } from '../../const/info.const';
import AnalysisChart from './AnalysisChart';
import AnalysisInfo from './AnalysisInfo';

function second2Time(second) {
  let hour = Math.floor(second / 3600)
  second = second % 3600
  let minute = Math.floor(second / 60)
  second = second % 60
  return [hour, minute, second]
}

function analysisData(bookInfo, records) {
  var startRec = false
  let process, duration, efficiency
  let idRec = 0
  let alyData = []

  for (let id = 0; id < records.length; id++) {
    const rec = records[id];

    process = ((rec.page / bookInfo.pages) * 100)
    if (startRec) {
      duration = Math.floor((rec.date - records[id - 1].date) / 1000)
      efficiency = ((rec.page - records[id - 1].page) / (duration / 60))

      if ((rec.type & VIEW_MODE.isBeginBit) === 0) {
        startRec = false
      }
    }else{
      if(rec.type & VIEW_MODE.isBeginBit){
        startRec = true
        if(id === 0){
          duration = 0
          efficiency = 0
          idRec = 0;
        }else{
          duration = 0
          idRec++
        }
      }else{
        console.log("Found double stop at ", id, rec)
      }
    }
    alyData.push({
      id: idRec,
      time: rec.date,
      process: process,
      remain: 100 - process,
      duration: duration,
      eff: efficiency,
    })
  }
  return alyData
}

function ReadingAnalysis(props) {

  var bookInfo = props.BookInfo || {}
  var records =  []

  for (let i = props.Records.length - 1; i >= 0; i--) {
    if((props.Records[i].type & ~(VIEW_MODE.isBeginBit)) === 0){
      records.push(props.Records[i])
    }
  }

  var data = analysisData(bookInfo, records)
  return (
    <div>
      {/* <AnalysisInfo BookInfo={bookInfo} Records={records}/> */}
      {/* <AnalysisChart Data={data}/> */}
    </div>
  );
}

export default ReadingAnalysis;