import React, { useEffect, useState } from 'react';

function RawInfo(props) {

  const [InfoValue, setInfo] = useState("")

  useEffect(()=>{
    if(typeof props.value === "string" 
        || typeof props.value === "number"){
      setInfo(props.value)
    }else{
      setInfo("")
    }
  }, [props.value])

  const handleValueChange = e => {
    if(props.onChange) props.onChange(e.target.value)
  }

  return (
    <div>
      {props.isEditing ?
      < input type={typeof InfoValue} value={InfoValue+""} onChange={handleValueChange}/>
      :
      < label > {InfoValue}</label >
      }
    </div>
  )
}

export default RawInfo;
