import React from 'react';
import { Chart } from "react-google-charts";
import { ReadAnalysis } from './ReadAnalysis';

export default function DayAnalysis(props) {

  const sections = props.Sections || [];
  let SecAlys = new ReadAnalysis(sections);
  let timeData = SecAlys.getDayAnalysis();

  let dataChart = [['Time reading', 'Time (s)']];
  for (let i = 0; i < 31; i++) {
    let tmp = timeData.find(dat => dat.hour === i);
    if (tmp) {
      dataChart.push([i, tmp.time]);
    }else{
      dataChart.push([i, 0])
    }
  }

  return (
  <Chart
    width={600}
    height={300}
    chartType="ColumnChart"
    loader={<div>Loading Chart</div>}
    data={dataChart}
    options={{
      title: 'The most hour in a day for reading',
      chartArea: { width: '70%' },
      hAxis: {
        title: 'Hour in day',
        minValue: 0,
        maxValue: 23,
        viewWindow: {
          max: 24,
        }
      },
      vAxis: {
        title: 'Time reading (seconds)',
      },
    }}
    legendToggle
  />)
}