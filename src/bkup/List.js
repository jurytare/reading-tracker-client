import React from "react"

const List = props => {

  const handleSelect = e => {
    e.preventDefault();
    let bookid = e.target.dataset.key
    if(props.onSelect){
      props.onSelect(bookid)
    }
  }

  return (
    <ol>
      {
        props.BookList.map(book => {
          return (
            <li key={book.bookId}><a 
              href={"#" + book.bookId} 
              onClick={handleSelect}
              data-key={book.bookId}
              >
                {book.name}
            </a>
            </li>
          )
        })
      }
    </ol>
  )
}

export default List;