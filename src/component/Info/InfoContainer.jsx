import React, { useState } from 'react';

import { navigate } from 'hookrouter';

import { Box, CircularProgress } from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux';
import { selectBook, selectBookStatus } from '../../service/reducers/book/selector';
import BookInfo from './BookInfo';
import BookEditor from './BookEditor';
import bookApi from '../../service/api/book.api';

import { editBook, unloadBook } from '../../service/reducers/book';
import { loadSectionList, unloadSectionList } from '../../service/reducers/sectionList';

export default function InfoContainer(props) {
  const [view, setView] = useState(0);

  const dispatch = useDispatch();
  const Book = useSelector(selectBook);
  const bStatus = useSelector(selectBookStatus);

  console.log(bStatus);

  if (!props.show) {
    return null;
  } else if (bStatus === 'loading') {
    return <CircularProgress />
  } else if (bStatus === 'failed') {
    return <div>Book is not available</div>
  }

  const handleChangeView = (value) => {
    setView(value);
  }

  const handleDeleteBook = rbid => {
    bookApi.del(props.RBID)
      .then(resp => {
        dispatch(unloadSectionList());
        // if(Book.usage === 1) {
          dispatch(unloadBook());
          navigate('/bookshelf', true);
        // }
      });
  }

  const handleAddBook = ev => {
    bookApi.add(Book._id)
      .then(resp => {
        dispatch(editBook(resp.data));
        dispatch(loadSectionList(resp.data.rbid));
      }).catch(err => {
        console.log('Can\'t add book');
      })
  }

  const handleBookChange = (book) => {
    // Update list if needed
    dispatch(editBook(book));
  }

  return (
    <div>

      <Box>
        {view > 0 ?
          <BookEditor
            onCancel={handleChangeView}
            onSave={handleBookChange}
            Book={Book}
          />
          :
          <BookInfo
            Book={Book}
            RBID={props.RBID}
            onAddReq={handleAddBook}
            onEditReq={handleChangeView}
            onRmvReq={handleDeleteBook}
          />
        }
      </Box>
    </div>
  )
}