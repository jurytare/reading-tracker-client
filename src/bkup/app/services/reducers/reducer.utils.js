function defaultPendingCallback (state, action) {state.status = 'loading';}

function defaultFulfilledCallback (state, action) {
    state.data = action.payload
    state.status = 'succeeded';
}

function defaultRejectCallback (state, action) {
    state.status = 'failed';
    state.error = action.error.message;
}

export function addTrackingFetch (thunkAction, reducer) {
  return {
    [thunkAction.pending]: reducer.pendingCallback || defaultPendingCallback,
    [thunkAction.fulfilled]: reducer.fulfilledCallback || defaultFulfilledCallback,
    [thunkAction.rejected]: reducer.rejectedCallback || defaultRejectCallback,
  }
}

export const reducersTable = {
  BOOK: 'book',
  SECTION: 'section',
  TITLE: 'titles',
  SECTIONLIST: 'sectionList'
}

export function normalizeSection(section, ratio) {
  if(typeof ratio !== 'number') {
    ratio = 1/1000;
  } 
  if (section.date > 0) {
    section.date = Math.floor(section.date * ratio);
  }
  section.recs.forEach((rec, id) => {
    if (rec.time > 0) {
      section.recs[id].time = Math.floor(rec.time * ratio);
    }
  });
  return section;
}
