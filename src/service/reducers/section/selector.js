import { reducersTable } from "../reducer.utils";

export const selectCurrentSection = state => {
  return state[reducersTable.SECTION].data;
}

export const selectSectionStatus = state => {
  let localState = state[reducersTable.SECTION];
  return localState.status;
}

// export const selectCurrentcurrentId = state => {
//   let localState = state[reducersTable.SECTION];
//   if(localState.rbid < 0 || localState.currentId < 0) {
//     return '';
//   }else{
//     return localState.data[localState.rbid].sections[localState.currentId]._id;
//   }
// }

// export const selectSectionList = state => {
//     const localState = state[reducersTable.SECTION];
//   return localState.data.map(section => {
//     const {_id, date, page} = section;
//     return {_id, date, page}
//   });
// }

// export const selectLastedReadingStatus = state => {
//   const localState = state[reducersTable.SECTION];
//   let secLen = localState.data.length;
//   if(secLen) {
//     let lastSection = localState.data[secLen-1];
//     let recLen = lastSection.recs.length;
//     if(recLen) {
//       return lastSection.recs[recLen - 1].state;
//     }else{
//       return ReadingState.READING;
//     }
//   }else{
//     return ReadingState.IDLE;
//   }
// }