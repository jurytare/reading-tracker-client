import { reducersTable } from "../reducer.utils";

export const selectBookTitles = state => {
  let bookList = state[reducersTable.BOOKTITLE]
  return bookList.data || [];
}

export const selectBookTitlesStatus = state => {
  return state[reducersTable.BOOKTITLE].status;
}

// export const selectCurrentTitle =  state => {
//   let bookList = state[reducersTable.BOOKTITLE];
//   if( bookList.currentId < 0 || bookList.currentId >= bookList.data.length) {
//     return undefined;
//    } else {
//     return bookList.data[bookList.currentId];
//    };
// }

// export const selectCurrentTitleId =  state => {
//   return state[reducersTable.BOOKTITLE].currentId;
// }

// export const selectCurrentBookId = state => {
//   let bookList = state[reducersTable.BOOKTITLE];
//   if (bookList.currentId < 0 || bookList.currentId >= bookList.data.length) {
//     return undefined;
//   } else {
//     return bookList.data[bookList.currentId]._id;
//   };
// }

// export const selectCurrentReadBookId =  state => {
//   let bookList = state[reducersTable.BOOKTITLE];
//   return bookList.currentId < 0 ? undefined : bookList.data[bookList.currentId].rbid;
// }