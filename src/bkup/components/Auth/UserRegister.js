import React, { useState, useRef } from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import authService from '../../services/auth.service';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const vusername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const vpassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

function UserRegister(props) {
  // const form = useRef();
  // const checkBtn = useRef();

  const username = useRef();
  const email = useRef();
  const password = useRef();
  const password2 = useRef();
  // const [successful, setSuccessful] = useState(false);
  // const [message, setMessage] = useState("");

  const handleRegister = (e) => {
    e.preventDefault();
    let ac = username.current.value
    let em = email.current.value
    let pass = password.current.value
    let pass2 = password2.current.value
    if (pass !== pass2) {
      console.log("Password failed")
      return;
    }

    authService.register(ac, em, pass).then(
      (response) => {
        username.current.value = ""
        email.current.value = ""
        password.current.value = ""
        password2.current.value = ""
      },
      (error) => {
        const resMessage =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
        console.log(resMessage)
      }
    );
  };

  return (
    <div className="col-md-12">
      <div className="card card-container border-0">
      <Form onSubmit={handleRegister}>
          <Form.Group controlId="formRegisterUsername">
            <Form.Control ref={username} type="text" placeholder="Enter user name" />
            {/* <Form.Text className="text-muted">
              Your user name must be unique.
            </Form.Text> */}
          </Form.Group>

          <Form.Group controlId="formRegisterEmail">
            <Form.Control ref={email} type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group controlId="formRegisterPassword">
            <Form.Control ref={password} type="password" placeholder="Enter Password" />
          </Form.Group>

          <Form.Group controlId="formRegisterRePassword">
            <Form.Control ref={password2} type="password" placeholder="Re-Enter Password" />
          </Form.Group>

          <Form.Group controlId="formRegisterCheckbox">
            <Form.Check type="checkbox" label="Accept EULA" />
          </Form.Group>
          <Button variant="primary" type="submit">Submit</Button>
        </Form>
      </div>
    </div>
  );
};

export default UserRegister;

