import { Box, Button, Tab, Tabs, TextField } from '@material-ui/core';
import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import SwipeableViews from 'react-swipeable-views';
import authService from '../../service/api/auth.service';
import { login } from '../../service/reducers/user';
import { useStyles } from './style';

export default function Sign(props) {

  const [value, setVal] = React.useState(0);
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setVal(newValue)
  }


  return (
    <div className={classes.signBox}>
      <Tabs
        value={value}
        indicatorColor="secondary"
        textColor="secondary"
        onChange={handleChange}
      >
        <Tab className={classes.tabLable} label="Sign In" />
        <Tab className={classes.tabLable} label="Sign Up" />
      </Tabs>

      <SwipeableViews
        axis={value ? 'x' : 'x-reverse'}
        index={value}
        // onChangeIndex={handleChangeIndex}
      >
        <SignIn show={value === 0}/>
        <SignUp show={value === 1}/>

      </SwipeableViews>
    </div>
  );
}

function SignIn(props) {
  // const [userErr, setUserErr] = React.useState('');
  // const [passErr, setPassErr] = React.useState('');
  const [LoginErr, setLoginErr] = React.useState('');
  const userName = useRef();
  const password = useRef();
  const dispatch = useDispatch();
  const classes = useStyles();


  const handleLogIn = event => {
    event.preventDefault();

    let user, pass;
    user = userName.current.value;
    pass = password.current.value;

    if(typeof user !== 'string' || user.length === 0) {
      setLoginErr("Missing user name");
      return;
    }

    if(typeof pass !== 'string' || pass.length === 0) {
      setLoginErr("Missing password");
      return;
    }
    setLoginErr('Login...');
    authService.login(user, pass)
      .then((response) => {
        let userInfo = response.data;
        if (userInfo.accessToken) {
          setLoginErr('');
          localStorage.setItem("user", JSON.stringify(userInfo));
          dispatch(login(userInfo));
        }
      }).catch(err => {
        setLoginErr(`Failed to login`)
      });
  }

  const onTyping = (ev) => {
    if(LoginErr) {
      if(LoginErr.length) {
        setLoginErr('');
      }
    }
    if(ev.charCode === 13) {
      handleLogIn(ev);
    }
  }

  const handleClear = (ev) => {
    ev.preventDefault();
    userName.current.value = '';
    password.current.value = '';
  }

  if(props.show) 
    return (
      <Box>
        <div>
          <label>{LoginErr}</label>
        </div>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={userName}
            // error={userErr.length > 0}
            onKeyPress={onTyping}
            label="User name"
            helperText=""
          />
        </div>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={password}
            type="password"
            // error={passErr.length > 0}
            onKeyPress={onTyping}
            label="Password"
            helperText=""
          />
        </div>
        <div className={classes.signFooter}>
          <Button variant="contained" color="primary" onClick={handleLogIn}>Sign In</Button>
          <Button variant="contained" color="default" onClick={handleClear}>Clear</Button>
        </div>
      </Box>
    )
  else
    return null
}

function SignUp(props) {
  // const [userErr, setUserErr] = React.useState('');
  // const [passErr, setPassErr] = React.useState('');

  const userName = useRef();
  const password = useRef();
  const rePass = useRef();
  const email = useRef();
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleSignUp = ev => {
    ev.preventDefault();
    let [u, p, rp, e] = [
      userName.current.value,
      password.current.value,
      rePass.current.value,
      email.current.value,
    ]

    if(rp.length > 0 || rp === p) {
      authService.register(u, e, p)
        .then(response => {
          let userInfo = response.data;
          if(userInfo.accessToken) {
            localStorage.setItem("user", JSON.stringify(userInfo));
            dispatch(login(userInfo))
          }
        }).catch(err => {
          console.log(err)
        })
    }
  }
  
  if(props.show) 
    return (
      <Box>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={userName}
            // error={userErr.length > 0}
            label="User name"
            helperText=""
          />
        </div>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={password}
            type="password"
            // error={passErr.length > 0}
            label="Password"
            helperText=""
          />
        </div>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={rePass}
            type="password"
            // error={passErr.length > 0}
            label="Re-type password"
            helperText=""
          />
        </div>
        <div>
          <TextField
            className={classes.inputText}
            inputRef={email}
            type="email"
            // error={passErr.length > 0}
            label="Email"
            helperText=""
          />
        </div>
        <div className={classes.signFooter}>
          <Button variant="contained" color="primary" onClick={handleSignUp}>Sign Up</Button>
          <Button variant="contained" color="default">Clear</Button>
        </div>
      </Box>
    )
    else
      return null
}