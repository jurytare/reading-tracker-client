import BookApi from "../../api/book.api";

const getBookTitlesReducer = async (numberOfTitles) => {
  let resp = await BookApi.getBookList(numberOfTitles);
  return resp.data || [];
}

getBookTitlesReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}

getBookTitlesReducer.rejectedCallback = (state, action) => {
  state.data = [];
  state.status = 'empty';
  state.error = action.error.message;
}

export { getBookTitlesReducer };