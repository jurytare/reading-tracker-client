import React from 'react';
import ListPanelEditor from '.';

function ListElement(props) {
    return (
        <li><a href='#abc'> {props.value} </a></li>
    )
}

function MyBoard(props) {
    return (
        <table>
            <tbody>
                {props.label ? props.label.map((name, id) => 
                    <tr>
                        <td>{name}</td>
                        <td>{props.val[id]}</td>
                    </tr>
                ) : []}
            </tbody>
        </table>
    )
}

export default function ListPanelEditor_Test(props) {
    var labels = [
        'lb1',
        'lb2',
        'lb3',
    ]
    var data = [
        'Hello',
        'World',
        '123'
    ];

    var panelData = {
        label: labels,
        data : data,
    }

    return (
        <ListPanelEditor>
            <ListPanelEditor.List 
                List={data}
                Element={ListElement}
            />
            <ListPanelEditor.Panel data={panelData} Board={MyBoard}/>
            <ListPanelEditor.Editor />
        </ListPanelEditor>
    )
}