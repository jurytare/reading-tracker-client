import { reducersTable } from "../reducer.utils";

export const selectBook = state => {
  return state[reducersTable.BOOK].data;
}

export const selectTotalPages = state => {
  return state[reducersTable.BOOK].pages;
}

export const selectBookStatus = state => state[reducersTable.BOOK].status;