import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ProgressAnalysis from '../components/ProgressAnalysis';
import { loadSectionList, unloadSectionList } from '../services/reducers/sectionList';
import { selectCurrentReadBookId } from '../services/reducers/title/selector';
import ReadPlayerContainer from './ReadPlayerContainer';
import SectionContainer from './SectionContainer';
import SectionListContainer from './SectionListContainer';


function ReadingContainer() {

  const dispatch = useDispatch();
  const currentRbid = useSelector(selectCurrentReadBookId);

  console.log('Reading ID', currentRbid);
  useEffect(() => {
    if(currentRbid) {
      dispatch(loadSectionList(currentRbid));
    }else{
      dispatch(unloadSectionList());
    }
  }, [currentRbid])

  return currentRbid ? 
    <div>
      <ReadPlayerContainer/>
      <ProgressAnalysis/>
      <SectionListContainer />
      <SectionContainer/>
    </div>
    :
    <div></div>
  ;
}

export default ReadingContainer;
