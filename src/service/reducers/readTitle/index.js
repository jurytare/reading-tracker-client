import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { getReadTitlesReducer } from './reducer.getReadTitles';

const sliceName = 'readTitles';

const initialState = {
  data:[],
  currentId: -1,
  status: 'empty',
  error: null,
}

const getReadTitles = createAsyncThunk(sliceName + '/getReadTitles', getReadTitlesReducer);

const titleSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    pickReadTitle: (state, action) => {
      const { id } = action.payload;
      state.currentId = id;
    },
    unPickReadTitle: (state, action) => {
      state.currentId = -1;
    }
  },
  extraReducers: {
    ...addTrackingFetch(getReadTitles, getReadTitlesReducer),
  },
})

export default titleSlice.reducer;
export const { pickReadTitle, unPickReadTitle } = titleSlice.actions;
export { getReadTitles };
