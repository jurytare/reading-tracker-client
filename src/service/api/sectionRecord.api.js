import link from "./AxiosService";
import { normalizeSection } from "../reducers/reducer.utils";

class SectionRecord {

  getList(rbid) {
    return link.get(`/read/${rbid}/section`).then(resp => {
      return {
        rbid: resp.data.rbid,
        pages: resp.data.pages,
        sections: resp.data.sections.map(sec => {
            return normalizeSection(sec, 1000);
          }
        )
      }
    });
  }

  getListFromBid(bid) {
    return link.get(`/book/${bid}/section`).then(resp => {
      return {
        rbid: resp.data.rbid,
        pages: resp.data.pages,
        sections: resp.data.sections.map(sec => {
            return normalizeSection(sec, 1000);
          }
        )
      }
    });
  }

  create(rbid, section) {
    let tmp = normalizeSection(section);
    return link.post(`/read/${rbid}/section`, tmp).then(resp => normalizeSection(resp.data, 1000));
  }

  edit(rbid, sid, section) {
    let tmp = normalizeSection(section);
    return link.put(`/read/${rbid}/section/${sid}`, tmp).then(resp => normalizeSection(resp.data, 1000));
  }

  delete(rbid, sid) {
    return link.delete(`/read/${rbid}/section/${sid}`)
  }
}

export default new SectionRecord()