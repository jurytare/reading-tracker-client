import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause, faPlus, faSync } from '@fortawesome/free-solid-svg-icons'
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';

import ActivityRecord from './ActivityRecord'
import { RecordCommand, RecordType } from './RecordRef';
import { binarySearch, encodeNum } from '../Utils';
import api from '../services/api';
import { BookContext } from '../BookContext';

function ActivitiesInfo(props) {

  const { bookInfo, records, setRecords } = useContext(BookContext);

  function handleNewRecord (e) {
    if(!bookInfo.bookId){
      console.log("Chose your book first")
      return
    }
    let newRecord = {bookId: bookInfo.bookId}
    if(records.length > 0){
      newRecord.page = records[0].page+1
    }else{
      newRecord.page = bookInfo.start_page
    }
    const dateTime = new Date()
    newRecord.date = dateTime.getTime()
    newRecord.recId = encodeNum(newRecord.date)
    newRecord.type = (RecordType.isStartBit) | 
      (RecordType.isRmBit) | (RecordType.isEditBit)
    setRecords([newRecord, ...records])
  }

  function syncData(data) {
    let savingRec = []
    let insertID

    if (bookInfo.bookId) {
      data.forEach((rec, index) => {
        // Only manipulate record which is not isEdit and isRm
        if ((rec.type & (~RecordType.isStartBit)) === 0) {
          insertID = binarySearch(savingRec, rec, (val1, val2) => {
            return val2.date - val1.date
          })
          if(insertID < savingRec.length){
            insertID++
          }
        }else{
          insertID = index
        }
        savingRec.splice(insertID, 0, rec)
      })
    }
    return savingRec
  }

  function handleRecordChange(cmd, recId, val) {
    console.log("Got command " + cmd)
    let ret = 0
    let ActNew = [], dupCounter = 0
    let savingRec = []
    switch(cmd){
      case RecordCommand.ADD:
      case RecordCommand.EDIT:
        if(!Number.isInteger(val.date) || val.date < 1 
          || !Number.isInteger(val.page) || val.page < 1){
          console.log("Data error")
          ret = -2
          break;
        }
        let chosenRec
        records.forEach(rec => {
          if(rec.recId === recId){
            rec = {...val, type:val.type & RecordType.isStartBit}
            chosenRec = {...rec}
            let insertID  = binarySearch(ActNew, rec, (val1,val2) => {
              return val2.date - val1.date
            })
            if(insertID < ActNew.length){
              insertID++
            }
            ActNew.splice(insertID, 0, rec)
          } else {
            dupCounter += (rec.date === val.date)
            ActNew.push(rec)
          }
        });
        if(dupCounter > 0){
          ret = -2
          break
        }
        ActNew = syncData(ActNew)
        setRecords(ActNew)

        chosenRec.date = encodeNum(chosenRec.date/1000)
        if(cmd === RecordCommand.ADD){
          api.createRecord(bookInfo.bookId, chosenRec)
            .then(response => {
              if(response.data.err){
                console.log(response.data.err)
              }
            })
            .catch(err => {
              console.log("Failed to add a record", err)
            })
        }else{
          api.updateRecord(bookInfo.bookId, recId, chosenRec)
          .then(response => {
            if(response.data.err){
              console.log(response.data.err)
            }
          })
          .catch(err => {
            console.log("Failed to edit a record", err)
          })
        }
        break
      case RecordCommand.RM:
        savingRec = records.filter(act => {
          if(act.recId !== recId) return true
          else {
            if(!(act.type & RecordType.isEditBit && act.type & RecordType.isRmBit)){
              api.deleteRecord(bookInfo.bookId, recId)
              .then(response => {
                if(response.data.err){
                  console.log(response.data.err)
                }
              })
              .catch(err => {
                console.log("Failed to delete a record", err)
              })
            }
            return false
          }
        });
        syncData(savingRec)
        setRecords(savingRec)
        break
      default:
        console.log("Unsupport command")
        ret = -1
        break
    }

    return ret
  }

  let ActivityHeader = []
  ActivityHeader.push(
    (<div>
      <Button variant="success" size="sm"
        onClick={handleNewRecord}>
        <FontAwesomeIcon icon={faPlus} size="xs" />
      </Button>
      {/* <Button variant="success" size="sm"
        onClick={handleAnalysisData}>
        <FontAwesomeIcon icon={faSync} size="xs" />
      </Button> */}
    </div>),
    (<label>Type</label>),
    (<label>Date</label>),
    (<label>Time</label>),
    (<label>Pages</label>),
  )

  return (
    <div>
      <table border="true">
        <thead>
          <tr>{
            ActivityHeader.map((elm, id) => {
              return (<td key={"head-act-" + id}>{elm}</td>)
            })}</tr>
        </thead>
        <tbody>
          {records.map((act, id) => {
            return (
            <ActivityRecord 
              key={"rec-act-" + id} 
              record={act} 
              onRecordChange={handleRecordChange}
            />
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default ActivitiesInfo