import React from 'react';
import Typography from '@material-ui/core/Typography';
import Overall from './Overall';
import HourAnalysis from './HourAnalysis';
import DayAnalysis from './DayAnalysis';
import WeekAnalysis from './WeekAnalysis';
import MonthAnalysis from './MonthAnalysis';

export default function Analysis(props) {
  const sections = props.Sections;
  const alysContents = {
    overall: <Overall Sections={sections} Pages={props.Pages}/>,
    hour: <HourAnalysis Sections={sections} Pages={props.Pages}/>,
    day: <DayAnalysis Sections={sections} Pages={props.Pages}/>,
    week: <WeekAnalysis Sections={sections} Pages={props.Pages}/>,
    month: <MonthAnalysis Sections={sections} Pages={props.Pages}/>,
  }

  if(Array.isArray(sections) && sections.length) {
    return alysContents[props.AlysType]
  }else{
    return (
      <Typography variant="body1" gutterBottom>
        Let's start reading
      </Typography>
    )
  }
}