import { faExclamationTriangle, faPause, faPlay } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { VIEW_MODE } from '../../../const/info.const';

function RecType(props) {

  const handleTypeChange = e => {
    if(props.onChange) props.onChange()
  }

  return (
    <div>
      {props.Type & VIEW_MODE.isEditBit ?
        <button variant="warning" size="sm"
        onClick={handleTypeChange}
        >
          <FontAwesomeIcon size="xs" icon={
            (props.Type & VIEW_MODE.isBeginBit) ?
              faPlay : faPause
          } />
        </button>
        :
        <FontAwesomeIcon size="xs" icon={
          props.Type & VIEW_MODE.isRmBit ? faExclamationTriangle :
            props.Type & VIEW_MODE.isBeginBit ? faPlay : faPause} />
    }
    </div>
  );
}

export default RecType;