import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectTotalPages } from '../../reducers/book/selector';
import { selectCurrentSection } from '../../reducers/section/selector';
import { milis2DayHourMinSecStr } from '../../utils/Utils';
import SectionInfo from './SectionInfo';

function SectionAnalysis(props) {

  const section = useSelector(selectCurrentSection);
  const totalPages = useSelector(selectTotalPages);
  var SecInfo = []
  const [now, setNow] = useState(new Date())

  useEffect(() => {
    const timer = setInterval(() => {    
      setNow(new Date());
    }, 1000);
    return () => {
      clearInterval(timer);
    }
  }, []);

  if(section){
    let secInfo = new SectionInfo(section);
    let etaProgress, etaReading;
    etaProgress = Math.floor(secInfo.getETA('', totalPages) * 1000);
    etaReading =  Math.floor(secInfo.getETA('reading', totalPages) * 1000);
    if(secInfo.getStatus() !== 'Stopped') {
      etaProgress += now.getTime();
      etaReading += now.getTime();
    }
    SecInfo = [
      {
        name: "Start date",
        val: new Date(secInfo.getStartDay() * 1000).toLocaleString(),
      },
      {
        name: "Start page",
        val: secInfo.getStartPage() + " (%)",
      },
      {
        name: "Status",
        val: secInfo.getStatus() + " at page " + secInfo.getCurrentPage(),
      },
      {
        name: 'Total progress time',
        val: milis2DayHourMinSecStr(secInfo.getProgressTime() * 1000),
      },
      {
        name: 'Total reading time',
        val: milis2DayHourMinSecStr(secInfo.getReadTime() * 1000),
      },
      {
        name: 'Progress speed',
        val: milis2DayHourMinSecStr(secInfo.getSpeed() * 1000) + ' / page',
      },
      {
        name: 'Reading speed',
        val: milis2DayHourMinSecStr(secInfo.getSpeed('read') * 1000) + ' / page',
      },
      {
        name: 'ETA of progress',
        val: new Date(etaProgress).toLocaleString(),
      },
      {
        name: 'ETA of reading',
        val: new Date(etaReading).toLocaleString(),
      }
    ];

  }

  return (
    <div>
      <table>
        <tbody>
          {SecInfo.map((info, id) => {
            return (
              <tr key={"secAly-"+id}>
                <td><b>{info.name}</b></td>
                <td> : </td>
                <td>{info.val}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SectionAnalysis;