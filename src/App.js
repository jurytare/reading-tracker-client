import './App.css';
import React from 'react';
import { Provider } from 'react-redux';
import store from './service/reducers/store';
import { useRoutes } from 'hookrouter';

import MenuAppBar from './component/TopMenu/MenuAppBar';
import MainRouter from './route/MainRouter';
import NotificationService from './service/common/NotificationService';

function App() {
  const routerResult = useRoutes(MainRouter);
  return (
    <Provider store={store}>
      <NotificationService>
        <MenuAppBar />
        {routerResult || <div>Page not found</div>}
      </NotificationService>
    </Provider>
  );
}

export default App;
