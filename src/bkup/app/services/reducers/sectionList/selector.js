import { reducersTable } from "../reducer.utils";

export const selectSectionList = state => {
  const localState = state[reducersTable.SECTIONLIST];
  return localState.data.map(section => {
    const { _id, date, page } = section;
    return { _id, date, page }
  });
}

export const selectCurrentSectionId = state => {
  const localState = state[reducersTable.SECTIONLIST];
  return localState.currentId;
}