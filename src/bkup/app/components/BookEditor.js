import { faBan, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';


function BookEditor(props) {

  const [Book, setBook] = useState(props.Book || {});

  function handleBookChange(event, field) {
    setBook({
      ...Book,
      [field]: event.target.value,
    })
  }

  const handleSaveEvent = event => {
    event.preventDefault();
    if(typeof props.onSave === 'function') props.onSave(Book);
  }

  const handleCancelEvent = event => {
    event.preventDefault();
    console.log('Cancel', Book);
    if(typeof props.onCancel === 'function') props.onCancel();
  }

  var infoSet = [
    {
      label:"Name",
      view: (< input 
        type="text" 
        value={Book.name} 
        onChange={event => handleBookChange(event, 'name')}/>)
    },
    {
      label:"Pages",
      view: (< input 
        type="number" 
        value={Book.pages} 
        onChange={event => handleBookChange(event, 'pages')}/>)
    },
  ]

  return (
    <div>
      <button
        variant="success" size="sm"
        onClick={handleSaveEvent}
      >
        <FontAwesomeIcon icon={faCheck} size="sm" />
      </button>
      <button
        variant="danger" size="sm"
        onClick={handleCancelEvent}
      >
        <FontAwesomeIcon icon={faBan} size="sm" />
      </button>
      <table>
        <tbody>
          {infoSet.map(info => {
            return (
              <tr key={"book-editor-" + info.label}>
                <td><b>{info.label}</b></td>
                <td>{info.view}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default BookEditor;
