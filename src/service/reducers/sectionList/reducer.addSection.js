import sectionRecordApi from "../../api/sectionRecord.api";
import { normalizeSection } from "../reducer.utils";
import { selectCurrentReadBookId } from "../readTitle/selector";

const addSectionReducer = async (section, thunkApi) => {
  let rbid = selectCurrentReadBookId(thunkApi.getState());
  if(typeof rbid !== 'string' || rbid.length === 0) {
    return thunkApi.rejectWithValue('readbook is not available');
  }else{
    section = normalizeSection(section);
    const response = await sectionRecordApi.add(rbid, section);
    return response.data;
  }
}

addSectionReducer.fulfilledCallback = (state, action) => {
  let section = {...action.payload, ...action.meta.arg};
  section.recs = [];
  state.currentId = state.data.length
  state.data.push(section);
  state.status = 'succeeded';
}
addSectionReducer.rejectedCallback = (state, action) => {
  // state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addSectionReducer };