import axios from "axios";
import { baseUrl } from "./service.const";


class SectionRecord {
  constructor() {
    const user = JSON.parse(localStorage.getItem('user'))
    let heading = {
      'Content-type': 'application/json',
    }
    if (user) {
      heading['x-access-token'] = user.accessToken
    }
    this.link = axios.create({
      baseURL: baseUrl,
      headers: heading,
    })
  }


  getList(rbid) {
    return this.link.get(`/mybook/${rbid}/section`)
  }

  add(rbid, section) {
    return this.link.post(`/mybook/${rbid}/section`, section)
  }

  edit(rbid, sid, section) {
    return this.link.put(`/mybook/${rbid}/section/${sid}`, section);
  }

  delete(rbid, secId) {
    return this.link.delete(`/mybook/${rbid}/section/${secId}`)
  }
  
//   read(bookId) {
//     return this.link.get(`/mybook/${bookId}`)
//   }

  // update(bookId, bookInfo) {
  //   return this.link.post(`/mybook/${bookId}`, bookInfo)
  // }

  addRec(bookId, secId, rec) {
    return this.link.post(`/mybook/${bookId}/section/${secId}`, rec)
  }

  editRec(bookId, secId, recId, rec) {
    return this.link.put(`/mybook/${bookId}/section/${secId}/rec/${recId}`, rec)
  }

  delRec(bookId, secId, recId) {
    return this.link.delete(`/mybook/${bookId}/section/${secId}/rec/${recId}`)
  }
}

export default new SectionRecord()