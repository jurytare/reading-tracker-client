import React, { useContext, useState } from 'react';
import InfoBar from './InfoBar';
import BasicInfo from './BasicInfo';

import { ACT_MODE, ACT_EVENT, bookStructure, REQ } from './ref_value'
import { convertInt2TimeStr, encodeNum } from '../Utils';
import { getData, removeData, storeData } from '../storage';
import api from '../services/api';
import { BookContext } from '../BookContext';

const Info = props => {
  let dataRef = "bookTracker-book-"+props.BookId
  let tempBook = getData(dataRef) || {}
  if (Number.isInteger(tempBook.start_date)){
    [tempBook.start_day, tempBook.start_time] = convertInt2TimeStr(tempBook.start_date)
  }

  const [status, setStatus] = useState(ACT_MODE.VIEW)

  const { bookInfo, setBookInfo } = useContext(BookContext);

  function createNewBook() {
    let bookNew = {}
    for (let key in bookStructure) {
      bookNew[key] = bookStructure[key][2]
    }
    let dateTime = new Date()
    bookNew.bookId = encodeNum(dateTime.getTime());
    bookNew.start_date = dateTime.getTime()
    let temp = convertInt2TimeStr(bookNew.start_date)
    bookNew.start_day = temp[0]
    bookNew.start_time = temp[1]
    setBookInfo(bookNew)
  }

  function saveNewBook() {
    if(handleChangeBook(REQ.BOOK.ADD, bookInfo, null)){
      console.log("Failed to save new book")
      return -1
    }else{
      api.createBook(bookInfo)
        .then(response => {
          console.log("Create OK")
          if(response.data.err){
            console.log(response.data.err)
          }
        })
        .catch(err => {
          console.log("Failed to create Book", err)
        });
      // storeData("bookTracker-book-"+bookInfo.bookId, bookInfo)
      console.log("New book added")
    }
  }

  function cancelEditBook() {
    setBookInfo(tempBook)
  }

  function saveEditedBook() {
    if(handleChangeBook(REQ.BOOK.EDIT, bookInfo, props.BookId)){
      console.log("Failed to edit book")
      return -1
    }else{
      if(bookInfo.bookId !== props.bookId){
        removeData(dataRef)
        dataRef = "bookTracker-book-"+bookInfo.bookId
      }
      api.updateBook(bookInfo.bookId, bookInfo)
        .then(response => {
          console.log("Update OK")
          if(response.data.err){
            console.log(response.data.err)
          }
        })
        .catch(err => {
          console.log("Failed to update Book", err)
        });
      storeData(dataRef, bookInfo)
      console.log("Editted book saved")
    }
  }

  function removeBook() {
    if(handleChangeBook(REQ.BOOK.RM, null, props.BookId)){
      console.log("Failed to remove a book")
      return -1
    }else{
      api.deleteBook(props.BookId)
      .then(response => {
        console.log("Delete OK")
        if(response.data.err){
          console.log(response.data.err)
        }
      })
      .catch(err => {
        console.log("Failed to delete Book", err)
      });
      removeData(dataRef)
      console.log("The book is removed")
    }
  }

  let eventDispatcher = {
    [ACT_MODE.UNSELECT]: {
      [ACT_EVENT.ADD]: {
        nextStage: ACT_MODE.ADDING,
        action: createNewBook,
      },
    },
    [ACT_MODE.ADDING]: {
      [ACT_EVENT.SAVE]: {
        nextStage: ACT_MODE.VIEW,
        action: saveNewBook
      },
      [ACT_EVENT.CANCEL]: {
        nextStage: tempBook.bookId? 
          ACT_MODE.VIEW:ACT_MODE.UNSELECT,
        action: cancelEditBook
      },
    },
    [ACT_MODE.EDITING]: {
      [ACT_EVENT.SAVE]: {
        nextStage: ACT_MODE.VIEW,
        action: saveEditedBook
      },
      [ACT_EVENT.CANCEL]: {
        nextStage: ACT_MODE.VIEW,
        action: cancelEditBook
      },
    },
    [ACT_MODE.VIEW]: {
      [ACT_EVENT.ADD]: {
        nextStage: ACT_MODE.ADDING,
        action: createNewBook
      },
      [ACT_EVENT.EDIT]: { nextStage: ACT_MODE.EDITING, },
      [ACT_EVENT.RMV]: {
        nextStage: ACT_MODE.UNSELECT,
        action: removeBook
      },
    },
  }

  const handleEvent = ev => {
    let currentState = eventDispatcher[status]
    if (currentState.hasOwnProperty(ev)) {
      if (currentState[ev].hasOwnProperty('action')) {
        currentState[ev].action()
      }
      setStatus(currentState[ev].nextStage);
    }
  }

  const handleChange = (elem, val) => {
    setBookInfo({ ...bookInfo, [elem]: val })
  }

  const handleChangeBook = (cmd, key, bookid) => {
    props.onChangeBookId && props.onChangeBookId(cmd, key, bookid)
  }

  return (
    <div>
      <h3>Main information</h3>
      <InfoBar
        isFixed={status === ACT_MODE.VIEW || status === ACT_MODE.UNSELECT}
        onEvent={handleEvent}
      />
      <BasicInfo
        bookStructure={bookStructure}
        Book={bookInfo}
        isFixed={status === ACT_MODE.VIEW || status === ACT_MODE.UNSELECT}
        onChange={handleChange}
      />
    </div>
  )
}

export default Info;