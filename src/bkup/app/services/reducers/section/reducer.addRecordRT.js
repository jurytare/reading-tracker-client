import sectionRecordApi from "../../api/sectionRecord.api";
import { selectCurrentReadBookId } from "../title/selector";
import { selectReadingSection } from "./selector";

const addRecordRTReducer = async (record, thunkApi) => {
  let rbid = selectCurrentReadBookId(thunkApi.getState());
  let section = selectReadingSection(thunkApi.getState());
  if(section && typeof section._id === 'string') {
    const response = await sectionRecordApi.addRec(rbid, section._id, record);
    return response.data;
  }else{
    return null;
  }
}

addRecordRTReducer.fulfilledCallback = (state, action) => {
//   state.data._id = action.payload;
  state.status = 'succeeded';
}
addRecordRTReducer.rejectedCallback = (state, action) => {
  // state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { addRecordRTReducer };