import axios from "axios";
import { baseUrl } from "./service.const";

const register = (userID, email, password) => {
  return axios.post(baseUrl + "/auth/signup", {
    userID,
    email,
    password,
  });
};

const login = (userID, password) => {
  return axios
    .post(baseUrl + "/auth/signin", {
      userID,
      password,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};
