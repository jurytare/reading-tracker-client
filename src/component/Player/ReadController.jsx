import { faPause, faPlay, faStop } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Box, TextField } from '@material-ui/core';
import React, { useRef } from 'react';
import { recordGenerator } from '../../utils/Utils';
import { ReadingState } from '../common/utils';

function ReadController(props) {

  const inputPage = useRef();

  // console.log(section);
  const section = props.Section;
  let readState;
  if(section && Array.isArray(section.recs)){
    if(section.recs.length){
      readState = section.recs[section.recs.length - 1].state;
    }else{
      readState = ReadingState.READING;
    }
  }else{
    readState = ReadingState.IDLE;
  }

  const handleRead = ev => {
    ev.preventDefault();
    let pageRead = Number(inputPage.current.value) || 0;
    let now = new Date().getTime();
    inputPage.current.value = "";

    if(pageRead > 0){
      if(readState === ReadingState.IDLE) {
        console.log('Start section at page', pageRead)
        if(typeof props.onStart === 'function') {
          props.onStart({
            _id: -now,
            date: now,
            page: pageRead,
            recs: [],
          });
        }
      }else if(readState === ReadingState.PAUSED) {
        console.log('Resume reading at page', pageRead)
        if(typeof props.onEvent === 'function') {
          let rec = recordGenerator(ReadingState.READING, pageRead, now - section.date);
          // props.onResume();
          props.onEvent(rec);
        }
      }
    }else{
      if(readState === ReadingState.PAUSED) {
        console.log('Resume reading');
        if(typeof props.onEvent === 'function') {
          let rec = recordGenerator(ReadingState.RESUME, pageRead, now - section.date);
          props.onEvent(rec);
        }
      }
    }
  }

  const handleUpdatePage = ev => {
    if(readState !== ReadingState.IDLE) {
      let now = new Date().getTime();
      if(ev.key === 'Enter') {
        let pageRead = Number(ev.target.value) || 0;
        ev.target.value = "";

        if(pageRead) {
          let rec = recordGenerator(ReadingState.READING, pageRead, now - section.date);
          typeof props.onEvent === 'function' && props.onEvent(rec);
        }
      }
    }
  } 

  const handlePause = ev => {
    let now = new Date().getTime();
    if(typeof props.onEvent === 'function') {
      let rec = recordGenerator(ReadingState.PAUSED, 0, now - section.date);
      props.onEvent(rec);
    }
  }

  const handleStop = ev => {
    ev.preventDefault();
    let now = new Date().getTime();
    let pageRead = Number(inputPage.current.value) || 0;
    inputPage.current.value = "";

    if(pageRead > 0 && typeof props.onEvent === 'function') {
      let rec = recordGenerator(ReadingState.IDLE, pageRead, now - section.date);
      props.onEvent(rec);
    }

  }

  const readButton =
    <Button style={{
        margin: '20px',
      }}
      variant="contained" color="primary" size="medium"
      onClick={handleRead}
    >
      <FontAwesomeIcon size="xs" icon={faPlay} />
    </Button>

  const pauseButton =
    <Button style={{
        margin: '20px',
      }}
      variant="contained" color="default" size="medium"
      onClick={handlePause}
    >
      <FontAwesomeIcon size="xs" icon={faPause} />
    </Button>

  const stopButton =
    <Button style={{
        margin: '20px',
      }}
      variant="contained" color="secondary" size="medium"
      onClick={handleStop}
    >
      <FontAwesomeIcon size="xs" icon={faStop} />
    </Button>

  const inputPageRead =
    < TextField inputRef={inputPage} type="number" step="1" 
      label="Page"
      placeholder="Enter page read"
      onKeyDown={handleUpdatePage}
    />


  if (ReadingState.IDLE === readState) {
    return (
      <div>
        {readButton}
        {inputPageRead}
      </div>
    )
  } else if (ReadingState.PAUSED === readState) {
    return (
      <Box>
        {readButton}
        {inputPageRead}
        {stopButton}
      </Box>
    )
  } else {
    return (
      <Box>
        {pauseButton}
        {inputPageRead}
        {stopButton}
      </Box>
    )
  }
}

export default ReadController;
