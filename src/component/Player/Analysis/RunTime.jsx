import { Paper, Table } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { convertInt2TimeStr, milis2DayHourMinSecStr } from '../../../utils/Utils';
import Info from '../../common/Info';
import { ReadingState, ReadingStateStr } from '../../common/utils';
import { SectionAnalysis } from './SectionAnalysis';

export default function Runtime(props) {
  const section = props.Section;
  const pages = props.Pages || 0;
  const timer = useRef(null);
  const [now, setNow] = useState(new Date());

  useEffect(() => {

    return () => {
      if(timer.current){
        clearInterval(timer.current);
        timer.current = null;
      }
    }
  }, []);

  useEffect(() => {
    if(section.recs.length &&
      (section.recs[section.recs.length - 1].state === ReadingState.IDLE ||
        section.recs[section.recs.length - 1].state === ReadingState.PAUSED )) {
        if(timer.current) {
          clearInterval(timer.current); 
          timer.current = null;
        }
    }else{
      if(timer.current === null) {
        timer.current = setInterval(() => {
          setNow(new Date());
        }, 1000);
      }
    }
    setNow(new Date());
  }, [section])

  const SecAlys = new SectionAnalysis(section);
  let analysInfo, readTime;

  readTime = SecAlys.getReadingRealTime();
  analysInfo = {
    date: convertInt2TimeStr(SecAlys.baseTime).join(' ') + 
      ' + ' + 
      milis2DayHourMinSecStr(readTime),

    status : ReadingStateStr[ReadingState.READING],
    page : `${section.page}`,
    speed : '0',
    eta : '...',
  }
  if(section.recs.length) {
    let speed = readTime / (SecAlys.getLatestReadPage() - section.page);
    analysInfo.status = ReadingStateStr[SecAlys.getLatestState()];
    analysInfo.page = `${section.page} -> ${SecAlys.getLatestReadPage()}`;
    analysInfo.speed = `${milis2DayHourMinSecStr(speed)} / page`;
    if(SecAlys.getLatestState() === ReadingState.IDLE) {
      analysInfo.eta = `Stopped at ${convertInt2TimeStr(SecAlys.getLatestTime()).join(' ')}`;
    }else{
      analysInfo.eta = convertInt2TimeStr(
        SecAlys.getLatestTime() + speed * (pages - SecAlys.getLatestReadPage())
      ).join(' ');
    }
  }

  return (
    <Paper elevation={3}>
      <table>
        <tbody>
          <Info label="Status" value={analysInfo.status} />
          <Info label="Time reading" value={analysInfo.date} />
          <Info label="Page reading" value={analysInfo.page} />
          <Info label="Reading speed" value={analysInfo.speed} />
          <Info label="ETA" value={analysInfo.eta} />
        </tbody>
      </table>
  </Paper>
  )
}

const rInfo = {
  status: 'Reading',
  progress: 100,
  start_date: 0,
  start_page: 10,
  total_time: 100,
}
