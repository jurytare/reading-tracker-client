import React from 'react';
import BookFilter from './Filter/BookFilter';
import BasicInfo from './Info/BasicInfo';
import ReadingRecord from './ReadRecord';
import { useSelector } from 'react-redux';
import { selectCurrentBookId } from '../reducers/title/selector';


function BookTracker() {


  const chosenBookID = useSelector(selectCurrentBookId);

  return (
    <div>
      <BookFilter />
      {chosenBookID ? 
        <div>
          <BasicInfo />
          <ReadingRecord />
        </div> : null
      }
    </div>
  );
}

export default BookTracker;
