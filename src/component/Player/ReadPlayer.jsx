import { Box, Button, Container, Paper, Tab, Tabs, Divider } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

import React from 'react';

import ReadController from './ReadController';
import { useDispatch, useSelector } from 'react-redux';
import { selectReadInfo } from '../../service/reducers/sectionList/selector';
import { addSection, editSection } from '../../service/reducers/sectionList';
import sectionRecordApi from '../../service/api/sectionRecord.api';
import { ReadingState } from '../common/utils';
import Runtime from './Analysis/RunTime';
import Analysis from './Analysis/Analysis';

function sectionLoadGenerator(sid, section) {
  let payload = {};
  payload.sid = sid;
  payload.section = {...section};
  payload.section.recs = [...section.recs];
  return payload;
}

export default function ReadPlayer(props) {

  const rbid = props.RBID;
  const [alysMode, setAlysMode] = React.useState('overall');
  const {pages, sections} = useSelector(selectReadInfo);
  const dispatch = useDispatch();

  if(!props.show) {
    return null;
  }

  const lastSection = sections.length ? sections[sections.length - 1] : undefined;

  const handleAlysChange = (event, newVal) => {
    setAlysMode(newVal);
  }

  const handleStartReading = section => {
    dispatch(addSection(section));
    sectionRecordApi.create(rbid, section)
      .then(resp => {
        let payload = sectionLoadGenerator(section._id, resp);
        dispatch(editSection(payload));
      }).catch(err => {
        console.log('Unable to create db');
      })
  }

  const handleReadingEvent = (rec) => {
    if(rec.page === pages) {
      rec.state = ReadingState.IDLE;
    }
    let payload = sectionLoadGenerator(lastSection._id, lastSection);
    payload.section.recs.push(rec);
    dispatch(editSection(payload));
    sectionRecordApi.edit(rbid, payload.sid, payload.section)
      .then(resp => {
        payload.section = resp;
        dispatch(editSection(payload))
      }).catch(err => {
        console.log('Unable to update db');
      })
  }

  return (
    <Container>
      <Box style={{marginTop: '10px'}}>
        <Typography variant="h5" gutterBottom>
          Section status
        </Typography>
        {lastSection ? 
          <Runtime Section={lastSection} Pages={pages}/>
          :
          <Typography variant="body1" gutterBottom>
            Let's start reading
          </Typography>
        }
      </Box>
      <Divider />
      <Box style={{marginTop: '5px'}}>
        <ReadController 
          Section={lastSection}
          onStart={handleStartReading}
          onEvent={handleReadingEvent}
          />
      </Box>
      <Divider />
      <Box style={{marginTop: '10px'}}>
      <Typography variant="h5" gutterBottom>
        Analysis
        </Typography>
        <Tabs
          value={alysMode}
          onChange={handleAlysChange}
          indicatorColor="secondary"
          textColor="secondary"
        >
          <Tab label="Overall" value='overall'  />
          <Tab label="Hour" value='hour'  />
          <Tab label="Day" value='day'  />
          <Tab label="Week" value='week'  />
          <Tab label="Month" value='month'  />
        </Tabs>
        <Paper>
          <Analysis 
            AlysType={alysMode} 
            Sections={sections}
            Pages={pages}
          />
        </Paper>
      </Box>
    </Container>
  );
}