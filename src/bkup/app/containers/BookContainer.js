import React, { useEffect, useState } from 'react';
import BookInfo from '../components/BookInfo';
import BookEditor from '../components/BookEditor';
import { useDispatch, useSelector } from 'react-redux';
import { selectCurrentBookId } from '../services/reducers/title/selector';
import { addBook, editBook, loadBook, unloadBook } from '../services/reducers/book';
import { selectBook, selectBookStatus } from '../services/reducers/book/selector';

function BookContainer() {

  const [EditView, setEditView] = useState(false);
  const dispatch = useDispatch();
  const currentBid = useSelector(selectCurrentBookId);
  const Book = useSelector(selectBook);
  const BookStatus = useSelector(selectBookStatus);

  useEffect(() => {
    if(currentBid) {
      dispatch(loadBook(currentBid));
    }else{
      dispatch(unloadBook());
    }
  }, [dispatch, currentBid]);

  const handleChangeView = () => {
    setEditView(true);
  }

  const handleCancelBook = () => {
    if (BookStatus === 'new') {
      if(currentBid) {
        dispatch(loadBook(currentBid));
      }else{
        dispatch(unloadBook());
      }
    } else {
      setEditView(false);
    }
  }

  const handleSaveBook = bookEditted => {
    if(BookStatus === 'ready') {
      dispatch(editBook({
        bid: currentBid,
        book: bookEditted,
      }))
    } else if (BookStatus === 'new') {
      dispatch(addBook(bookEditted))
    }
    setEditView(false);
  }

  if(BookStatus === 'ready') {
    if(EditView) {
      return <BookEditor Book={Book} onSave={handleSaveBook} onCancel={handleCancelBook} />
    } else {
      return <BookInfo Book={Book} onChangeView={handleChangeView} />;
    }
  }else if (BookStatus === 'new') {
    return <BookEditor Book={Book} onSave={handleSaveBook} onCancel={handleCancelBook} />
  } else if (BookStatus === 'pending') {
    return (<div>Loading Book...</div>);
  } else if (BookStatus === 'rejected') {
    return (<div>Can't load the book</div>);
  } else {
    return (<div>Please select book</div>);
  }
}

export default BookContainer;
