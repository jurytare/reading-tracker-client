import React from 'react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { ACT_EVENT } from '../../const/info.const';
import { editBook } from '../../reducers/book';
import InfoController from './InfoController';


function InfoEditor(props) {

  const [Book, setBook] = useState(props.Book)
  const dispatch = useDispatch();

  const handleBookChange = (data, key) => {
    let dataConverted = ""

    if(data.target.type === "datetime-local") {
      let [day, time] = data.target.value.split('T')
      dataConverted = new Date(day + " " + time).getTime()
    }else if(data.target.type === "number") {
      dataConverted = parseInt(data.target.value)
    }else {
      dataConverted = data.target.value
    }
    setBook({...Book, [key]:dataConverted})
  }

  var infoSet = [
    {
      label:"Name",
      view: (< input 
        type="text" 
        value={Book.name} 
        onChange={data => handleBookChange(data, 'name')}/>)
    },
    {
      label:"Pages",
      view: (< input 
        type="number" 
        value={Book.pages} 
        onChange={data => handleBookChange(data, 'pages')}/>)
    },
  ]

  const handleEvent = (cmd, nextView) => {
    if (cmd === ACT_EVENT.UPDATE) {
      for (let key in props.Book) {
        if (props.Book[key] !== Book[key]) {
          dispatch(editBook({bookId: Book._id, bookInfo: Book}))
            .then(originalPromiseResult  => {
              if(originalPromiseResult.error) {
                console.log("Unable to edit book");
              }else{
                if(props.onEvent) props.onEvent(cmd, nextView);
              }
            })
          return;
        }
      }
    }

    if(props.onEvent) props.onEvent(cmd, nextView);
  }

  return (
    <div>
      <InfoController onEvent={handleEvent} ViewMode={props.ViewMode} />
      <table>
        <tbody>
          {infoSet.map(info => {
            return (
              <tr key={"info-editor-" + info.label}>
                <td><b>{info.label}</b></td>
                <td>{info.view}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default InfoEditor;