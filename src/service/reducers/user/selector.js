import { reducersTable } from "../reducer.utils";

export const selectUserName = state => {
  return state[reducersTable.USER].data.uid;
}