import { SectionAnalysis } from "./SectionAnalysis";

export class ReadAnalysis {
  constructor(sections) {
    this.data = sections.map(sec => new SectionAnalysis(sec));
  }

  getPageRead() {
    return this.data.reduce((total, sec) => total += sec.getReadPage(), 0)
  }

  getStartTime() {
    return this.data.length ? this.data[0].baseTime : 0;
  }

  getReadTime() {
    return this.data.reduce((total, sec) => total += sec.getReadingRealTime(), 0)
  }

  getLatestState() {
    return this.data[0].getLatestState();
  }

  getLatestPage() {
    return this.data.length ? this.data[0].getReadPage() : 0;
  }

  getLatestReadTime() {
    return this.data.length ? this.data[0].getLatestTime() : 0;
  }

  getHourAnalysis() {
    let result = [];
    this.data.forEach(sec => {
      sec.getHourAnalysis(result);
    });
    return result;
  }

  getDayAnalysis() {
    let result = [];
    this.data.forEach(sec => {
      sec.getDayAnalysis(result);
    });
    return result;
  }

  getWeekAnalysis() {
    let result = [];
    this.data.forEach(sec => {
      sec.getWeekAnalysis(result);
    });
    return result;
  }

  getMonthAnalysis() {
    let result = [];
    this.data.forEach(sec => {
      sec.getMonthAnalysis(result);
    });
    return result;
  }
}