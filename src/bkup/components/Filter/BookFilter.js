import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTitles, pickTitle, unPickTitle } from '../../reducers/title';
import { selectTitles, selectCurrentBookId } from '../../reducers/title/selector';
import ListSelector from './ListSelector';
import SearchBar from './SearchBar';

function BookFilter(props) {

  // const [bookList, setBookList] = useState([])
  const dispatch = useDispatch();
  const bookList = useSelector(selectTitles);
  const currentBookId = useSelector(selectCurrentBookId);

  useEffect(() => {
    dispatch(getTitles(10));
  }, [dispatch])

  const handleAddBook = e => {
    if (props.onSelectBook) props.onSelectBook(undefined)
  }

  const handleSelectBook = (title, id) => {
    if(currentBookId === title._id) {
      dispatch(unPickTitle());
    }else{
      dispatch(pickTitle({title, id}));
    }
  }

  return (
    <div>
      <SearchBar
      // onNewBook={handleAddBook}
      />
      <ListSelector List={bookList || []} onSelect={handleSelectBook} />
    </div>
  );
}

export default BookFilter;