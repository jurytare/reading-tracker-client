import React from 'react';
// import Button from 'react-bootstrap/Button';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEdit, faTrash, faSave, faBan } from '@fortawesome/free-solid-svg-icons';
import { ACT_EVENT } from './ref_value';

const InfoBar = props => {

  return (
    <div>
      {(props.isFixed) ?
        <div>

          {/* ADDING BUTTON */}
          <button
            variant="success" size="sm"
            onClick={e => props.onEvent(ACT_EVENT.ADD)}
          >
            <FontAwesomeIcon icon={faPlus} size="sm" />
          </button>

          {/* EDIT BUTTON */}
          <button
            variant="warning" size="sm"
            onClick={e => props.onEvent(ACT_EVENT.EDIT)}
          >
            <FontAwesomeIcon icon={faEdit} size="sm" />
          </button>

          {/* REMOVE BUTTON */}
          <button
            variant="danger" size="sm"
            onClick={e => props.onEvent(ACT_EVENT.RMV)}
          >
            <FontAwesomeIcon icon={faTrash} size="sm" />
          </button>
        </div>
        :
        <div>

          {/* SAVING BUTTON */}
          <button
            variant="primary" size="sm"
            onClick={e => props.onEvent(ACT_EVENT.SAVE)}
          >
            <FontAwesomeIcon icon={faSave} size="sm" />
          </button>

          {/* CANCEL BUTTON */}
          <button
            variant="danger" size="sm"
            onClick={e => props.onEvent(ACT_EVENT.CANCEL)}
          >
            <FontAwesomeIcon icon={faBan} size="sm" />
          </button>
        </div>
      }
    </div>
  )
}

export default InfoBar;