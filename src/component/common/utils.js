export const ReadingState = {
  IDLE: 0,
  READING: 1,
  PAUSED: 2,
  RESUME: 3,
  ROTATE: 4,
}

export const ReadingStateStr = {
  [ReadingState.IDLE] : 'Idle',
  [ReadingState.READING] : 'Reading',
  [ReadingState.PAUSED] : 'Paused',
  [ReadingState.RESUME] : 'Resumed',
}
