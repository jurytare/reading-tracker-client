import { faPause, faPlay, faStop } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef } from 'react';
import { ReadingState } from '../containers/ReadPlayerContainer';

function ReadingController(props) {

  const readState = props.State || ReadingState.IDLE;
  const inputPage = useRef();

  const handleRead = ev => {
    ev.preventDefault();
    let pageRead = Number(inputPage.current.value) || 0;
    inputPage.current.value = "";

    if(pageRead > 0){
      if(readState === ReadingState.IDLE) {
        console.log('Start section at page', pageRead)
        if(typeof props.onStart === 'function') {
          props.onStart(pageRead);
        }
      }else if(readState === ReadingState.PAUSED) {
        console.log('Resume reading at page', pageRead)
        if(typeof props.onResume === 'function') {
          props.onResume();
          props.onUpdate(pageRead);
        }
      }
    }else{
      if(readState === ReadingState.PAUSED) {
        console.log('Resume reading');
        if(typeof props.onResume === 'function') {
          props.onResume();
        }
      }
    }
  }

  const handleUpdatePage = ev => {
    if(readState !== ReadingState.IDLE) {
      if(ev.key === 'Enter') {
        let pageRead = Number(ev.target.value) || 0;
        ev.target.value = "";

        if(pageRead) {
          props.onUpdate(pageRead);
        }
      }
    }
  } 

  const handlePause = ev => {
    if(typeof props.onPause === 'function') {
      props.onPause();
    }
  }

  const handleStop = ev => {
    ev.preventDefault();
    let pageRead = Number(inputPage.current.value) || 0;
    inputPage.current.value = "";

    if(pageRead > 0 && typeof props.onStop === 'function') {
      props.onStop(pageRead);
    }
  }

  const readButton =
    <button variant="warning" size="sm"
      onClick={handleRead}
    >
      <FontAwesomeIcon size="xs" icon={faPlay} />
    </button>

  const pauseButton =
    <button variant="warning" size="sm"
      onClick={handlePause}
    >
      <FontAwesomeIcon size="xs" icon={faPause} />
    </button>

  const stopButton =
    <button variant="warning" size="sm"
      onClick={handleStop}
    >
      <FontAwesomeIcon size="xs" icon={faStop} />
    </button>

  const inputPageRead =
    < input ref={inputPage} type="number" step="1" 
      placeholder="Enter page read"
      onKeyDown={handleUpdatePage}
    />


  if (ReadingState.IDLE === readState) {
    return (
      <div>
        {readButton}
        {inputPageRead}
      </div>
    )
  } else if (ReadingState.PAUSED === readState) {
    return (
      <div>
        {readButton}
        {inputPageRead}
        {stopButton}
      </div>
    )
  } else {
    return (
      <div>
        {pauseButton}
        {inputPageRead}
        {stopButton}
      </div>
    )
  }
}

export default ReadingController;
