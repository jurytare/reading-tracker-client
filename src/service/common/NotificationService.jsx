import React, { createContext, useState } from 'react';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';


export const NotificationContext = createContext({});

export default function NotificationService(props) {

  const [ NotifProps, setNotifProps ] = useState({
    open: false,
    msg: "",
    severity: "warning",
  });

  const setNotification = props => {
    let isUpdate = false;
    for (const key in props) {
      if (! NotifProps.hasOwnProperty(key) || NotifProps[key] !== props[key]) {
        isUpdate = true;
        break;
      }
    }
    if(isUpdate){
      setNotifProps({...NotifProps, ...props})
    }
  }

  const handleClose = event => {
    setNotifProps({...NotifProps, open: false});
  }

  return (
    <NotificationContext.Provider value={{ setNotification }}>
      {props.children}

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        open={NotifProps.open}
        // autoHideDuration={3000}
        // onClose={handleClose}
        // message={NotifProps.msg}
        // action={
        //   <React.Fragment>
        //     <IconButton
        //       aria-label="close"
        //       color="inherit"
        //       // className={classes.close}
        //       onClick={handleClose}
        //     >
        //       <CloseIcon />
        //     </IconButton>
        //   </React.Fragment>
        // }
        >
          <Alert 
            onClose={handleClose} 
            severity={NotifProps.severity}
            elevation={6} variant="filled"
          >
            {NotifProps.msg}
          </Alert>
        </Snackbar>
    </NotificationContext.Provider>
  )
}
