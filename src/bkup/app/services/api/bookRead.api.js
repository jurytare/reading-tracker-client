import axios from "axios";
import { baseUrl } from "./service.const";


class BookRead {
  constructor() {
    const user = JSON.parse(localStorage.getItem('user'))
    let heading = {
      'Content-type': 'application/json',
    }
    if (user) {
      heading['x-access-token'] = user.accessToken
    }
    this.link = axios.create({
      baseURL: baseUrl,
      headers: heading,
    })
  }


  getList() {
    return this.link.get("/mybook")
  }

  add(bookInfo) {
    return this.link.post(`/mybook`, bookInfo)
  }

  read(bookId) {
    return this.link.get(`/mybook/${bookId}`)
  }

  // update(bookId, bookInfo) {
  //   return this.link.post(`/mybook/${bookId}`, bookInfo)
  // }

  delete(bookId) {
    return this.link.delete(`/mybook/${bookId}`)
  }
}

export default new BookRead()