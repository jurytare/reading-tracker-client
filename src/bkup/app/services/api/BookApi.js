import axios from "axios";
import { baseUrl } from "./service.const";


class BookApi {
  constructor() {
    const user = JSON.parse(localStorage.getItem('user'))
    let heading = {
      'Content-type': 'application/json',
    }
    if (user) {
      heading['x-access-token'] = user.accessToken
    }
    this.link = axios.create({
      baseURL: baseUrl,
      headers: heading,
    })
  }


  getTitles(numberOfTitles) {
    return this.link.get("/book", {params: {n:numberOfTitles}})
  }

  createBook(bookInfo) {
    return this.link.post(`/book`, bookInfo)
  }

  readBook(bookId) {
    return this.link.get(`/book/${bookId}`)
  }

  updateBook(bookId, bookInfo) {
    return this.link.post(`/book/${bookId}`, bookInfo)
  }

  deleteBook(bookId) {
    return this.link.delete(`/book/${bookId}`)
  }

  getAllRecord(bookId) {
    return this.link.get(`/book/${bookId}/record`)
  }

  createRecord(bookId, record) {
    return this.link.post(`/book/${bookId}/record`, record)
  }

  updateRecord(bookId, recId, record) {
    return this.link.post(`/book/${bookId}/record/${recId}`, record)
  }

  deleteRecord(bookId, recId) {
    return this.link.delete(`/book/${bookId}/record/${recId}`)
  }
}

export default new BookApi()