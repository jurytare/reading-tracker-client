import { configureStore } from "@reduxjs/toolkit";
import { reducersTable } from "./reducer.utils";
import bookTitleReducer from './bookTitle';
import readTitleReducer from './readTitle';
import bookReducer from './book';
import sectionReducer from './section';
import sectionListReducer from "./sectionList";
import userReducer from './user';

export default configureStore({
    reducer: {
        [reducersTable.BOOKTITLE]: bookTitleReducer,
        [reducersTable.READTITLE]: readTitleReducer,
        [reducersTable.BOOK]: bookReducer,
        [reducersTable.SECTIONLIST]: sectionListReducer,
        [reducersTable.SECTION]: sectionReducer,
        [reducersTable.USER]: userReducer,
    }
})