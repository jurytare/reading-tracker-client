import { createSlice } from '@reduxjs/toolkit'


const sliceName = 'userInfo';

const initialState = {
  data:{
    uid: '',
    email: '',
  },
  status: 'empty',
  error: null,
}


const userSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    login: (state, action) => {
      state.data = action.payload;
      state.status = 'ready';
      state.error = null;
    },
    logout: (state, action) => {
      state.data = initialState.data;
      state.status = 'empty';
      state.error = null;
    },
  },
})

export default userSlice.reducer;
export const { login, logout } = userSlice.actions;

