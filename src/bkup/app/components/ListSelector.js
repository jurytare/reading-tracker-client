import React from 'react';

function genElement(element, id, handleSelect) {
  return (
    <li key={element._id}>
      <a href={"#" + element._id}
        onClick={handleSelect}
        data-key={id}
      >
        {element.elementName}
      </a>
    </li>
  )
}

function ListSelector(props) {
  
  const handleSelect = e => {
    e.preventDefault();
    let listId = Number(e.target.dataset.key);
    if (props.onSelect) {
      props.onSelect(props.List[listId], listId)
    }
  }

  const filterLowerCase = typeof props.Filter === 'string' ? 
    props.Filter.toLowerCase() : "";
  let list = []

  if(Array.isArray(props.List)) {
    props.List.forEach((element, id) => {
      if(filterLowerCase.length) {
        if (element.elementName.toLowerCase().includes(filterLowerCase)) {
          list.push(genElement(element, id, handleSelect));
        }
      }else{
        list.push(genElement(element, id, handleSelect));
      }
    });
  }

  return (
    <ol>
      {list}
    </ol>
  );
}

export default ListSelector;