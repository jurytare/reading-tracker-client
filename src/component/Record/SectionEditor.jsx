import { faCheck, faBan, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef, useState } from 'react';
import { convertInt2TimeStr } from '../../utils/Utils';
import RecordEditor from './RecordEditor';
import { ReadingState } from '../common/utils';

function SectionEditor(props) {

  const [Section, setSection] = useState(props.Section || {recs: []});
  const changeLog = useRef({
    _id: Section._id,
    recs:[],
  });
    
  useEffect(() => {
    if(props.Section) {
      setSection(props.Section);
      changeLog.current = {
        _id:props.Section._id,
        recs:[],
      };
    }
  }, [props.Section]);

  function handleSectionChange(event, field) {
    event.preventDefault();
    let value = null;
    if(field === 'page') {
      value = event.target.value.length ?
        Number(event.target.value) : '';
    }else if(field === 'date') {
      let date = new Date(event.target.value.replace("T", " "))
      value = date.getTime();
    }

    if(value != null) {
      setSection({
        ...Section,
        [field]: value,
      })
      changeLog.current[field] = value;
    }
  }

  const handleSaveEvent = event => {
    event.preventDefault();
    if(typeof props.onSave === 'function') props.onSave(Section);
  }

  const handleDeleteEvent = event => {
    event.preventDefault();
    console.log('Cancel', Section);
    if(typeof props.onDelete === 'function') props.onDelete(Section);
  }

  const handleRecordAdd = () => {
    let newRecord, refPage, now;
    now = new Date();
    if(Section.recs.length) {
      refPage = Section.recs[Section.recs.length - 1].page;
    }else{
      refPage = Section.page;
    }
    newRecord = {
      _id: - now.getTime(),
      time: now.getTime() - Section.date,
      page: refPage + 1,
      state: ReadingState.READING,
      changeId: changeLog.current.recs.length,
    }
    changeLog.current.recs.push(newRecord);
    setSection({
      ...Section,
      recs: [...Section.recs, newRecord]
    })
  }

  const handleRecordChange = (event, field, rid) => {
    event.preventDefault();
    let records = Section.recs.map((record, id) => {
      if (rid === record._id) {
        let value = null;
        let recEditted = {...record};
        if (field === 'state') {
          value = record.state + 1;
          if(value >= ReadingState.ROTATE) {
            value = 0;
          }
        } else if(field === 'time') {
          let date = new Date(event.target.value.replace("T", " "))
          value = date.getTime() - Section.date;
        } else if(field === 'page'){
          value = event.target.value.length ?
            Number(event.target.value) : '';
        }
        if(value !== null) {
          recEditted[field] = value;
          if(recEditted.hasOwnProperty('changeId')) {
            changeLog.current.recs[recEditted.changeId] = recEditted;
          } else {
            recEditted.changeId = changeLog.current.recs.length;
            changeLog.current.recs.push(recEditted);
          }
        }
        return recEditted;
      } else {
        return record;
      }
    })
    setSection({
      ...Section,
      recs: records,
    })
  }

  const handleRecordDelete = recId => {
    let newRecords;
    if(typeof recId === 'string') {
      newRecords = Section.recs.filter((rec) => {
        if(rec._id === recId) {
          changeLog.current.recs.push({_id: rec._id});
        }
        return rec._id !== recId;
      })
    }else{
      newRecords = Section.recs.filter(rec => rec._id !== recId);
    }
    setSection({
      ...Section,
      recs: newRecords,
    })
  }

  let [dayStr, timeStr] = convertInt2TimeStr(Section.date);
  var infoSet = [
    {
      label:"Part name",
      view: (< input 
        type="text" 
        value={Section.name} 
        onChange={event => handleSectionChange(event, 'name')}/>)
    },
    {
      label:"Start page",
      view: (< input 
        type="number" 
        value={Section.page} 
        onChange={event => handleSectionChange(event, 'page')}/>)
    },
    {
      label:"Start time",
      view: (< input 
        type="datetime-local"
        value={dayStr+"T"+timeStr} 
        onChange={event => handleSectionChange(event, 'date')}/>)
    },
  ]

  return (
    <div>
      <button
        variant="success" size="sm"
        onClick={handleSaveEvent}
      >
        <FontAwesomeIcon icon={faCheck} size="sm" />
      </button>
      <button
        variant="danger" size="sm"
        onClick={handleDeleteEvent}
      >
        <FontAwesomeIcon icon={faTrash} size="sm" />
      </button>
      <table>
        <tbody>
          {infoSet.map(info => {
            return (
              <tr key={"section-editor-" + info.label}>
                <td><b>{info.label}</b></td>
                <td>{info.view}</td>
              </tr>
            )
          })}
        </tbody>
        </table>
        <RecordEditor 
          BaseTime={Section.date}
          Records={Section.recs} 
          onCreate={handleRecordAdd}
          onChange={handleRecordChange}
          onDelete={handleRecordDelete}
        />
    </div>
  );
}

export default SectionEditor;
