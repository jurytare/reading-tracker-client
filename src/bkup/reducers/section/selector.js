import { reducersTable } from "../reducer.utils";

export const selectCurrentSection = state => {
  let localState = state[reducersTable.SECTION];
  if(localState.currentId < 0){
    return undefined;
  } else {
    return localState.data[localState.sid];
  }
}

// export const selectCurrentSID = state => {
//   let localState = state[reducersTable.SECTION];
//   if(localState.rbid < 0 || localState.sid < 0) {
//     return '';
//   }else{
//     return localState.data[localState.rbid].sections[localState.sid]._id;
//   }
// }

export const selectSectionList = state => {
    const localState = state[reducersTable.SECTION];
  return localState.data.map(section => {
    const {_id, date, page} = section;
    return {_id, date, page}
  });
}