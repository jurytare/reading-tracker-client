function loadRecords(state, action) {
    const { id } = action.payload;
    state.sid = id;
}

function unloadRecords(state, action) {
    state.sid = -1;
}

function addRecord(state, action) {

}

function editRecord(state, action) {

}

function delRecord(state, action) {

}

export const ChangeLocalSection = { loadRecords, unloadRecords, addRecord, editRecord, delRecord };