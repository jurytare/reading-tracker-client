export const RecordElement = {
  day: ["Date", "date", 0],
  time: ["Time", "time", 0],
  page: ["Page", "number", 0],
}

export const RecordType = {
  isStartBit: 1<<0,
  isEditBit: 1<<1,
  isRmBit: 1<<2,
}

export const RecordCommand = {
  ADD:'A',
  EDIT:'E',
  RM:'R',
}