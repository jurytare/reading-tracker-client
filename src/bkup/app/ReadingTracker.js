import React from 'react';
import BookListContainer from './containers/BookListContainer';
import BookContainer from './containers/BookContainer';
import ReadingContainer from './containers/ReadingContainer';

import { Provider } from 'react-redux';
import store from './services/reducers/store';


function ReadingTracker() {

  return (
    <Provider store={store}>
      <BookListContainer/>
      <BookContainer />
      <ReadingContainer />
    </Provider>
  );
}

export default ReadingTracker;
