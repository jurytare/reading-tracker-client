import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef, useState } from 'react';
import SectionAlys from '../models/SectionAlys';
import Info from './common/Info';

function SectionInfo(props) {

  let timeTotal, timeReading, pagesRead, speedAvr, speedHighest;

  const [now, setNow] = useState(new Date());
  let sectionInfo = useRef(new SectionAlys(props.Section))
  
  useEffect(() => {
    const timer = setInterval(() => {    
      setNow(new Date());
    }, 1000);
    return () => {
      clearInterval(timer);
    }
  }, []);

  useEffect(() => {
    sectionInfo.current = new SectionAlys(props.Section);
  }, [props.Section])

  const handleEditEvent = event => {
    event.preventDefault();
    if(typeof props.onChangeView === 'function') props.onChangeView();
  }

  return (
    <div>
      <button
        variant="success" size="sm"
        onClick={handleEditEvent}
      >
        <FontAwesomeIcon icon={faEdit} size="sm" />
      </button>
      <table>
        <tbody>
          <Info label="Total time" value={sectionInfo.current.getProgressTime()} />
          <Info label="Reading time" value={sectionInfo.current.getReadTime()} />
          <Info label="Pages read" value={sectionInfo.current.getCurrentPage() - sectionInfo.current.getStartPage()} />
          <Info label="Average speed" value={sectionInfo.current.getSpeed('read')} />
          <Info label="Highest speed" value={speedHighest} />
        </tbody>
      </table>
    </div>

  )
}

export default SectionInfo;