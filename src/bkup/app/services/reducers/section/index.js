import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { editSectionReducer } from './reducer.editSection';
import { loadSectionReducer } from './reducer.loadSection';

const sliceName = 'section';

const initialState = {
  data:{
    page: 0,
    date: 0,
    recs: [],
  },
  status: 'empty',
  error: null,
}

const loadSection = createAsyncThunk(sliceName + '/loadSection', loadSectionReducer);
const editSection = createAsyncThunk(sliceName + '/editSection', editSectionReducer);

const sectionSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    unloadSection: (state) => {
      state.data = initialState.data;
      state.status = initialState.status;
      state.error = null;
    },
    createSection: (state, action) => {
      state.data = initialState.data;
      state.status = 'new';
      state.error = null;
    },
    addRecord: (state, action) => {
      let rec = {...action.payload};
      let now = new Date();
      rec._id = - now.getTime();
      rec.time = Math.floor(now.getTime()/1000) - state.data.date;
      state.data.recs.push(rec);
    }
  },
  extraReducers: {
    ...addTrackingFetch(loadSection, loadSectionReducer),
    ...addTrackingFetch(editSection, editSectionReducer),
  },
})

export default sectionSlice.reducer;
export { loadSection, editSection };
export const { addRecord, createSection, unloadSection } = sectionSlice.actions;
