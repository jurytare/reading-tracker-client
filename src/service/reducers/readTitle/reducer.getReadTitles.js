import bookApi from "../../api/book.api";

const getReadTitlesReducer = async () => {
  let resp = await bookApi.getReadList();
  return resp.data || [];
}

getReadTitlesReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}

getReadTitlesReducer.rejectedCallback = (state, action) => {
  state.data = [];
  state.status = 'failed';
  state.error = action.error.message;
}

export { getReadTitlesReducer };