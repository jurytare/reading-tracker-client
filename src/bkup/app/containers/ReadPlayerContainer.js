import React, { useDebugValue, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ReadingAnalysis from '../components/ReadingAnalysis';
import ReadingController from '../components/ReadingController';
import { editSection } from '../services/reducers/section';
import { selectCurrentSection, selectSectionStatus } from '../services/reducers/section/selector';
import { addSection } from '../services/reducers/sectionList';
import { recordGenerator } from '../utils/Utils';

export const ReadingState = {
  IDLE : 0,
  READING : 1,
  PAUSED : 2,
  RESUME: 3,
  ROTATE : 4,
}

export default function ReadPlayerContainer(props) {

  const dispatch = useDispatch();
  const readingSection = useSelector(selectCurrentSection);
  const sectionStatus = useSelector(selectSectionStatus);
  let readingState;
  if(sectionStatus === 'succeeded') {
    if(readingSection.recs.length) {
      readingState = readingSection.recs[readingSection.recs.length - 1].state;
    }else{
      readingState = ReadingState.READING;
    }
  }else{
    readingState = ReadingState.IDLE;
  }

  console.log('Current Section', readingSection);
  const handleStart = pageRead => {
    console.log('Start section at page', pageRead)
    dispatch(addSection({page:pageRead}));
  }

  const handleUpdate = pageRead => {
    console.log('Update section at page', pageRead)
    let rec = recordGenerator(ReadingState.READING, pageRead, null);

    dispatch(editSection({
      _id: readingSection._id,
      recs: [rec],
    }));
  }

  const handlePause = () => {
    console.log('Pause reading section');
    let rec = recordGenerator(ReadingState.PAUSED, 0, null);
    console.log('handlePause', rec)
    dispatch(editSection({
      _id: readingSection._id,
      recs: [rec],
    }));  
  }

  const handleResume = pageRead => {
    console.log('Resume section', pageRead);
    let rec = recordGenerator(ReadingState.RESUME, pageRead, null);
    dispatch(editSection({
      _id: readingSection._id,
      recs: [rec],
    }));
  }

  const handleStop = pageRead => {
    console.log('Stop section at', pageRead);
    let rec = recordGenerator(ReadingState.IDLE, pageRead, null);
    dispatch(editSection({
      _id: readingSection._id,
      recs: [rec],
    }));
  }

  return (
    <div>
      {readingState === ReadingState.IDLE ? 
      null : <ReadingAnalysis/>}
      <ReadingController State={readingState}
        onStart={ handleStart }
        onUpdate={ handleUpdate }
        onPause={ handlePause }
        onResume={ handleResume }
        onStop={ handleStop }
      />
    </div>
  )
}