import axios from "axios";

const baseURL = process.env.PUBLIC_URL

class Api {
    constructor() {
    this.link = axios.create({
        baseURL: baseURL,
        headers: {
          "Content-type": "application/json"
        }
      })
    }

    getAllBook() {
        return this.link.get("/book")
    }

    createBook(bookInfo) {
        return this.link.post(`/book`, bookInfo)
    }

    readBook(bookId) {
        return this.link.get(`/book/${bookId}`)
    }

    updateBook(bookId, bookInfo) {
        return this.link.put(`/book/${bookId}`, bookInfo)
    }

    deleteBook(bookId) {
        return this.link.delete(`/book/${bookId}`)
    }

    getAllRecord(bookId) {
        return this.link.get(`/record/${bookId}`)
    }

    createRecord(bookId, record) {
        return this.link.post(`/record/${bookId}`, record)
    }

    updateRecord(bookId, recId, record) {
        return this.link.put(`/record/${bookId}/${recId}`, record)
    }

    deleteRecord(bookId, recId) {
        return this.link.delete(`/record/${bookId}/${recId}`)
    }
}

export default new Api()