import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addTrackingFetch } from '../reducer.utils';
import { loadBookReducer } from './reducer.loadBook';

// import { addBookReducer } from './reducer.addBook';
// import { delBookReducer } from './reducer.delBook';
// import { editBookReducer } from './reducer.editBook';
// import { createBookReducer } from './reducer.createBook';

const sliceName = 'bookInfo';

const initialState = {
  data:{
    _id: '',
    name: '',
    pages: 0,
  },
  status: 'empty',
  error: null,
}

const loadBook = createAsyncThunk(sliceName + '/loadBook', loadBookReducer);
// const createBook = createAsyncThunk(sliceName + '/createBook', createBookReducer);
// const editBook = createAsyncThunk(sliceName + '/editBook', editBookReducer);
// const delBook = createAsyncThunk(sliceName + '/delBook', delBookReducer);
// const addBook = createAsyncThunk(sliceName + '/addBook', addBookReducer);

const bookSlice = createSlice({
  name: sliceName,
  initialState,
  reducers: {
    editBook: (state, action) => {
      state.data = action.payload;
      state.status = 'ready';
      state.error = null;
    },
    unloadBook: (state, action) => {
      state.data = initialState.data;
      state.status = 'empty';
      state.error = null;
    },
  },
  extraReducers: {
    ...addTrackingFetch(loadBook, loadBookReducer),
    // ...addTrackingFetch(addBook, addBookReducer),
    // ...addTrackingFetch(delBook, delBookReducer),
    // ...addTrackingFetch(editBook, editBookReducer),
  },
})

export default bookSlice.reducer;
export const { editBook, unloadBook } = bookSlice.actions;
export { loadBook };
// export { loadBook, createBook, delBook, editBook, addBook };
