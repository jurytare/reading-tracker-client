import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';


function SearchBar(props) {
  const handleAddBook = e => {
    if(props.onNewBook) props.onNewBook(e)
  }

  return (
    <div>
      <table>
        <thead>
          <tr>
            <td>
              <button variant="warning" size="sm"
              onClick={handleAddBook}
              >
                <FontAwesomeIcon size="xs" icon={faPlus} />
              </button>
            </td>
            <td>
              <input type="text" />
            </td>
            <td>
              <button variant="warning" size="sm"
              // onClick={handleTypeChange}
              >
                <FontAwesomeIcon size="xs" icon={faTimes} />
              </button>
            </td>
            <td>
              <button variant="warning" size="sm"
              // onClick={handleTypeChange}
              >
                <FontAwesomeIcon size="xs" icon={faSearch} />
              </button>
            </td>
          </tr>
        </thead>
      </table>
    </div>
  );
}

export default SearchBar;