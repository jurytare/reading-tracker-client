import React, { useContext, useEffect, useRef, useState } from 'react';
import { ACT_EVENT, VIEW_MODE } from '../../const/info.const';
import { REC_EVENT, REC_STATE } from '../../const/rec.const';
import { BookContext } from '../../context/BookContextProvider';
import { encodeNum } from '../../utils/Utils';
import RecordDialog from './RecordDialog';
import RecordRuntime from './RecordRuntime';
import RecTableHead from './RecTableHead';
import RecTableRow from './Row/RecTableRow';

function ReadingRecTable(props) {

  const {bookInfo, records, crudRecord} = useContext(BookContext);
  // const [isRecording, setReading] = useState(false);
  const [RecState, setRecState] = useState(null);

  const RecDataModel = useRef([])

  useEffect(() => {
    RecDataModel.current = []
    let currentRecSection = null;
    records.forEach((rec, id, arr) => {
      let index = arr.length - 1 -id;
      if(!currentRecSection){
        currentRecSection = {
          start_date: arr[index].date,
          rec: []
        }
        RecDataModel.current.splice(0, 0, currentRecSection)
      }
      let tmpRec = {
        type: arr[index].type,
        time: arr[index].date - currentRecSection.start_date,
      };
      
      currentRecSection.rec.splice(0, 0, tmpRec);

      if(arr[index].page) {
        tmpRec.page = arr[index].page;
        if(!(tmpRec.type & VIEW_MODE.isBeginBit)) {
          currentRecSection = null;
        }
      }
    });
    if(records.length) {
      if(records[0].type & VIEW_MODE.isBeginBit){
        setRecState(REC_STATE.Reading);
      }else if (!records[0].page){
        setRecState(REC_STATE.Paused);
      }
    }
  }, [records])

  const handleNewRecord = () => {
    let newRecord = {bookId: 0}
    if(records.length > 0){
      newRecord.page = records[0].page+1
    }else{
      newRecord.page = 0
    }
    const dateTime = new Date()
    newRecord.date = dateTime.getTime()
    newRecord._id = encodeNum(newRecord.date)
    newRecord.date = Math.floor(newRecord.date/1000)*1000
    newRecord.type = (VIEW_MODE.isBeginBit) | 
      (VIEW_MODE.isRmBit) | (VIEW_MODE.isEditBit)
    crudRecord(ACT_EVENT.ADD, newRecord)
    return newRecord;
  }

  const handleStartRec = e => {
    // setReading(true);
    if(records.length && records[0].page === bookInfo.pages) {
      setRecState(REC_STATE.Finish);
    }else{
      setRecState(REC_STATE.Idle);
    }
  }

  const handleStopRecord = e => {
    // setReading(false);
    setRecState(null);
  }

  const handleRecordChange = (cmd, record) => {
    return crudRecord(cmd, record)
  }

  const handleRecordEvent = (event, data) => {
    console.log("Got Event", event, "data: ", data)
    let tmp;
    switch(event) {
      // TODO: inform RecordRuntime about resuming
      case REC_EVENT.RESUME:
      case REC_EVENT.UPDATE:
      case REC_EVENT.READ:
        if(data) {
          tmp = handleNewRecord();
          tmp.date = data.time;
          tmp.page = data.page;
          if(data.page === bookInfo.pages){
            tmp.type = 0;
          }
          crudRecord(ACT_EVENT.CREAT, tmp);
        }
        if(data.page === bookInfo.pages){
          setRecState(REC_STATE.Stopped);
        }else{
          setRecState(REC_STATE.Reading);
        }
        break;
      case REC_EVENT.PAUSE:
        if(data) {
          tmp = handleNewRecord();
          tmp.type = 0;
          tmp.date = data.time;
          tmp.page = 0;
          crudRecord(ACT_EVENT.CREAT, tmp);
        }
        setRecState(REC_STATE.Paused)
        break;
      case REC_EVENT.STOP:
        if(data) {
          tmp = handleNewRecord();
          tmp.type = 0;
          tmp.date = data.time;
          tmp.page = data.page;
          crudRecord(ACT_EVENT.CREAT, tmp);
        }
        setRecState(REC_STATE.Stopped)
        break;
      case REC_EVENT.END:
        if(data) {
          // TODO: save all records
        }else{
          // TODO: Remove all records
        }
        if(records[0].page === bookInfo.pages){
          setRecState(REC_STATE.Finish)  
        }else{
          setRecState(REC_STATE.Idle)
        }
        break;
      default:
        console.log("Unknown command", event)
        break;
    }
  }
  
  return (
    <div>
      <RecordDialog onClose={handleStopRecord}
        onEvent={handleRecordEvent} RecState={RecState}
      >
        <RecordRuntime RecState={RecState} RecData={RecDataModel.current}/>
      </RecordDialog>
      <table>
        <RecTableHead onAddRec={handleNewRecord} onStartRec={handleStartRec}/>
        <tbody>
          {
            records.map(rec => {
              return (
                <RecTableRow key={rec._id} Record={rec} onRecordChange={handleRecordChange} />
              )
            })
          }
        </tbody>
      </table>
    </div>
  );
}

export default ReadingRecTable;
