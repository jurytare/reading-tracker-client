import React, { useEffect, useState } from 'react';
import SectionInfo from '../components/SectionInfo';
import SectionEditor from '../components/SectionEditor';
import { useDispatch, useSelector } from 'react-redux';
import { editSection, loadSection, unloadSection } from '../services/reducers/section';
import { selectCurrentSection, selectSectionStatus } from '../services/reducers/section/selector';
import { selectCurrentSectionId } from '../services/reducers/sectionList/selector';
import { addSection } from '../services/reducers/sectionList';

function SectionContainer() {

  const [EditView, setEditView] = useState(false);
  const dispatch = useDispatch();
  const currentSectionId = useSelector(selectCurrentSectionId);
  const Section = useSelector(selectCurrentSection);
  const SectionStatus = useSelector(selectSectionStatus);

  useEffect(() => {
    if(currentSectionId < 0) {
      dispatch(unloadSection());
    }else{
      dispatch(loadSection({sid:currentSectionId}));
    }
  }, [dispatch, currentSectionId]);

  useEffect(() => {
    if(SectionStatus === 'empty') {
      setEditView(false);
    }
  }, [SectionStatus]);

  const handleChangeView = () => {
    setEditView(true);
  }

  const handleCancelSection = () => {
    if (SectionStatus === 'new') {
        dispatch(unloadSection());
    }
    setEditView(false);
  }

  const handleSaveSection = (section, changeLog) => {
    if(SectionStatus === 'succeeded') {
      dispatch(editSection(changeLog));
    } else if (SectionStatus === 'new') {
      dispatch(addSection(section));
    }
    console.log('Edit command', changeLog);
    setEditView(false);
  }

  if(SectionStatus === 'succeeded') {
    if(EditView) {
      return <SectionEditor Section={Section} onSave={handleSaveSection} onCancel={handleCancelSection} />
    } else {
      return (
      <SectionInfo Section={Section} onChangeView={handleChangeView} />);
    }
  }else if (SectionStatus === 'new') {
    return <SectionEditor onSave={handleSaveSection} onCancel={handleCancelSection} 
      Section={{
        date: new Date().getTime(),
        page: 0,
        type: 1,
        recs: [],
      }}
    />
  } else {
    return (<div></div>);
  }
}

export default SectionContainer;
