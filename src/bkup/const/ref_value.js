export const bookList = [{
    _id: "5fd782f7ea044362c51561b5",
    bookId: "VCo0yHW",
    name: "Mar Heaven",
    pages: 123
},
{
    _id: "5fd7830bea044362c51561b7",
    bookId: "VCo13B9",
    name: "Mastering Javascript",
    pages: 150
}];
export const readRecord = [{
    _id: "5fd78318ea044362c51561b9",
    bookId: "VCo0yHW",
    recId: "VCo19sr",
    type: 0,
    date: "1t86wm",
    page: 35
},
{
    _id: "5fd78319ea044362c51561ba",
    bookId: "VCo0yHW",
    recId: "VCo18it",
    type: 1,
    date: "1t86wi",
    page: 34
},
{
    _id: "5fd78319ea044362c51561bb",
    bookId: "VCo0yHW",
    recId: "VCo18at",
    type: 0,
    date: "1t86wh",
    page: 33
},
{
    _id: "5fd7831aea044362c51561bc",
    bookId: "VCo0yHW",
    recId: "VCo18L2",
    type: 1,
    date: "1t86wg",
    page: 32
},
{
    _id: "5fd78368ea044362c51561bd",
    bookId: "VCo13B9",
    recId: "VCo1Ust",
    type: 1,
    date: "1t86y4",
    page: 92
},
{
    _id: "5fd78369ea044362c51561be",
    bookId: "VCo13B9",
    recId: "VCo1UUt",
    type: 0,
    date: "1t86y3",
    page: 91
},
{
    _id: "5fd7836aea044362c51561bf",
    bookId: "VCo13B9",
    recId: "VCo1TFA",
    type: 1,
    date: "1t86xx",
    page: 90
},
{
    _id: "5fd7836bea044362c51561c0",
    bookId: "VCo13B9",
    recId: "VCo1VT3",
    type: 1,
    date: "1t86y6",
    page: 93
}];
export const bookDetail = [{
    _id: "5fd782f7ea044362c51561b6",
    bookId: "VCo0yHW",
    start_date: 1607959277182,
    start_page: 32,
    recId: null
},
{
    _id: "5fd7830bea044362c51561b8",
    bookId: "VCo13B9",
    start_date: 1607959291677,
    start_page: 90,
    recId: null
}];