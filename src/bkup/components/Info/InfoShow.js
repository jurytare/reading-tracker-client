import React from 'react';
import { convertInt2TimeStr } from '../../utils/Utils';


// function Info(props) {
//   return (
//     <tr>
//       <td><label><b>{props.title}</b></label></td>
//       <td><RawInfo 
//           isEditing={props.isEditing}
//           value={props.value}
//           onChange={props.onChange}
//         />
//       </td>
//     </tr>
//   )
// }

function Info(props) {
  return (
    <tr>
      <td><b>{props.label}</b></td>
      <td>{": " + props.value}</td>
    </tr>
  )
}

function InfoShow(props) {

  // const handleBookChange = (data, key) => {
  //   let tempBook = {...props.Book, [key]:data}
  //   // console.log(data, key, tempBook)
  //   if(props.onChange) props.onChange(tempBook)
  // }
  // const [dayStr, timeStr] = convertInt2TimeStr(props.Book.dateCreated)
  const tmp = new Date(props.Book.dateCreated);
  const dayStr = tmp.toDateString();
  const timeStr = tmp.toLocaleTimeString();


  return (
    <table>
      <tbody>
        <Info label="Name" value={props.Book.name}/>
        <Info label="Author" value={props.Book.author}/>
        <Info label="Pages" value={props.Book.pages}/>
        <Info label="Usage reference" value={props.Book.usage}/>
        <Info label="Shared by" value={props.Book.user}/>
        <Info label="Date shared" value={dayStr + " " + timeStr}/>
      </tbody>
    </table>
  )
  // return (
  //   <table>
  //     <tbody>
  //       <Info
  //         title={PROPS_NAME.name}
  //         value={props.Book.name}
  //         isEditing={props.ViewMode & VIEW_MODE.isEditBit}
  //         onChange={data => handleBookChange(data, 'name')}
  //       />
  //       <Info
  //         title={PROPS_NAME.pages}
  //         value={props.Book.pages}
  //         isEditing={props.ViewMode & VIEW_MODE.isEditBit}
  //         onChange={data => handleBookChange(data, 'pages')}
  //       />
  //       <Info
  //         title={PROPS_NAME.start_page}
  //         value={props.Book.start_page}
  //         isEditing={props.ViewMode & VIEW_MODE.isEditBit}
  //         onChange={data => handleBookChange(data, 'start_page')}
  //       />
  //       <tr>
  //         <td><label><b>{PROPS_NAME.start_time}</b></label></td>
  //         <td>
  //           <TimeInfo
  //             Time={props.Book.start_date}
  //             isEditing={props.ViewMode & VIEW_MODE.isEditBit}
  //             onChange={data => handleBookChange(data, 'start_date')}
  //           />
  //         </td>
  //       </tr>
  //     </tbody>
  //   </table>
  // )
}

export default InfoShow;