import { Paper, Table } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { convertInt2TimeStr, milis2DayHourMinSecStr } from '../../../utils/Utils';
import Info from '../../common/Info';
import { ReadingStateStr } from '../../common/utils';
import { ReadAnalysis } from './ReadAnalysis';


export default function Overall(props) {
  const sections = props.Sections || [];
  const pages = props.Pages || 0;
  const timer = useRef(null);
  const readAlys = useRef(new ReadAnalysis(sections));
  const [now, setNow] = useState(new Date());

  useEffect(() => {
    
    if(timer.current === null) {
      timer.current = setInterval(() => {
        setNow(new Date());
      }, 1000);
    }

    return () => {
      if (timer.current) {
        clearInterval(timer.current);
        timer.current = null;
      }
    }
  }, []);

  useEffect(() => {
    readAlys.current = new ReadAnalysis(sections);
  }, [sections])

  let readAlysTmp = readAlys.current;
  let analysInfo, pageRemain, speedProgress, speedRead;
  let readTime, totalTime;

  pageRemain = pages - readAlysTmp.getLatestPage();
  readTime = readAlysTmp.getReadTime();
  totalTime = now.getTime() - readAlysTmp.getStartTime();

  analysInfo = {
    status: ReadingStateStr[readAlysTmp.getLatestState()],
    date: convertInt2TimeStr(readAlysTmp.getStartTime()).join(' '),
  }

  analysInfo.readTime = milis2DayHourMinSecStr(readTime)
  let readEfficient = readTime/totalTime;
  if(readEfficient > 0.01) {
    analysInfo.readTime += ` (${(readEfficient * 100).toFixed(2)} %)`;
  }else if(readEfficient > 0.0001) {
    analysInfo.readTime += ` (${(readEfficient * 1000).toFixed(2)} ‰)`;
  }else{
    analysInfo.readTime += ` (${(readEfficient * 10000).toFixed(2)} ‱)`;
  }

  if(pageRemain) {
    speedRead =  readTime / readAlysTmp.getPageRead();
    speedProgress = (totalTime) / readAlysTmp.getPageRead();

    analysInfo.etaRead = milis2DayHourMinSecStr(speedRead * pageRemain)
      + ` (${milis2DayHourMinSecStr(speedRead)} / page)`

    analysInfo.etaProgress = convertInt2TimeStr(
      now.getTime() + speedProgress * pageRemain
    ).join(' ') + ` (${milis2DayHourMinSecStr(speedProgress)} / page)`
  }

  return (
    <Paper elevation={3}>
      <table>
        <tbody>
          <Info label="Status" value={analysInfo.status} />
          <Info label="Start at" value={analysInfo.date} />
          <Info label="Total read time" value={analysInfo.readTime} />
          {/* <Info label="Reading speed" value={analysInfo.speed} /> */}
          {/* <Info label="Progress speed" value={analysInfo.speed} /> */}
          <Info label="Estimate remain" value={analysInfo.etaRead} />
          <Info label="Progress ETA" value={analysInfo.etaProgress} />
          {/* <Info label="Delay" value={analysInfo.eta} /> */}
          {/* <Info label="Advance" value={analysInfo.eta} /> */}
        </tbody>
      </table>
    </Paper>
  )
}