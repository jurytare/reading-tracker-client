import link from "./AxiosService";
import { baseUrl } from "./service.const";


class BookApi {
  getBookList(numberOfTitles) {
    return link.get("/book", {params: {n:numberOfTitles}});
  }

  getReadList() {
    return link.get('/read');
  }

  get(bid) {
    return link.get(`/book/${bid}`);
  }

  create(bInfo) {
    return link.post(`/book`, bInfo)
  }

  add(bid) {
    return link.post(`/book/${bid}`);
  }

  edit(rbid, bInfo) {
    return link.put(`/read/${rbid}`, bInfo);
  }

  del(rbid) {
    return link.delete(`/read/${rbid}`);
  }
}

export default new BookApi()