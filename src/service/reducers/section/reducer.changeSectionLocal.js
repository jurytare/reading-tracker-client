function loadSection(state, action) {
    const { id } = action.payload;
    state.currentId = id;
}

function unloadSection(state, action) {
    state.currentId = -1;
}

function createSection(state, action) {
    state.currentId = -2;
}

export const ChangeLocalSection = { loadSection, unloadSection, createSection };