import { reducersTable } from "../reducer.utils";

export const selectTitles = state => {
  let bookList = state[reducersTable.TITLE]
  return bookList.data || [];
}

export const selectCurrentTitle =  state => {
  let bookList = state[reducersTable.TITLE];
  if( bookList.currentId < 0 || bookList.currentId >= bookList.data.length) {
    return undefined;
   } else {
    return bookList.data[bookList.currentId];
   };
}

export const selectCurrentTitleId =  state => {
  return state[reducersTable.TITLE].currentId;
}

export const selectCurrentBookId = state => {
  let bookList = state[reducersTable.TITLE];
  if (bookList.currentId < 0 || bookList.currentId >= bookList.data.length) {
    return undefined;
  } else {
    return bookList.data[bookList.currentId]._id;
  };
}

export const selectCurrentReadBookId =  state => {
  let bookList = state[reducersTable.TITLE];
  return bookList.currentId < 0 ? undefined : bookList.data[bookList.currentId].rbid;
}