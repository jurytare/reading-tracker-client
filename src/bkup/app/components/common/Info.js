import React from 'react';

export default function Info(props) {
    return (
      <tr>
        <td><b>{props.label}</b></td>
        <td>{": " + props.value}</td>
      </tr>
    )
  }