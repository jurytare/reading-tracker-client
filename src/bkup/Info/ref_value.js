
export const bookStructure = {
  // bookId: ["ID", "text", 0],
  name: ["Name", "text", ""],
  pages: ["NO. pages", "number", 0],
  start_page: ["Starting page", "number", 0],
  start_day: ["Starting day", "date", 0],
  start_time: ["Starting time", "time", 0]
}

export const REQ = {
  BOOK: {
    ADD: 'AB',
    RM: 'RB',
    EDIT: 'EB',
  }
}

export const ACT_MODE = {
  UNSELECT: "UNSELECT",
  VIEW: "SELECTED",
  EDITING: "EDITING",
  ADDING: "ADDING",
}

export const ACT_EVENT = {
  ADD: "ADD",
  RMV: "RMV",
  EDIT: "EDIT",
  SAVE: "SAVE",
  CANCEL: "CANCEL",
}