import sectionRecordApi from "../../api/sectionRecord.api";

const loadSectionsReducer = async (rbid) => {
  const response = await sectionRecordApi.getList(rbid);
  return response.data;
}

loadSectionsReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.sid = -1;
  state.status = 'succeeded';
}
loadSectionsReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadSectionsReducer };