import { configureStore } from "@reduxjs/toolkit";
import { reducersTable } from "./reducer.utils";
import titleReducer from './title';
import bookReducer from './book';
import sectionReducer from './section';
import sectionListReducer from "./sectionList";

export default configureStore({
    reducer: {
        [reducersTable.TITLE]: titleReducer,
        [reducersTable.BOOK]: bookReducer,
        [reducersTable.SECTIONLIST]: sectionListReducer,
        [reducersTable.SECTION]: sectionReducer,
    }
})