import BookApi from "../../api/BookApi";

const loadBookReducer = async (bookId) => {
  // If data is available, return here
  const response = await BookApi.readBook(bookId);
  return response.data;
}

loadBookReducer.fulfilledCallback = (state, action) => {
  state.data = action.payload;
  state.status = 'ready';
}

loadBookReducer.rejectedCallback = (state, action) => {
  state.data = {};
  state.status = 'failed';
  state.error = action.error.message;
}

export { loadBookReducer };