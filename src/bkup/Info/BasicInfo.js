import React from 'react';
import Table from 'react-bootstrap/Table';

function BasicInfo(props) {
  const handleChange = e => {
    let elem = e.target.dataset.key
    let value = e.target.value
    if(props.bookStructure[elem][1] === "number"){
      value = parseInt(value, 10)
    }
    props.onChange && props.onChange(elem, value)
  }

  return (
    <table>
      <tbody>
        {
          Object.keys(props.bookStructure).map(id => {
            return (
              <tr key={id}>
                <td><label><b>{props.bookStructure[id][0]}</b></label></td>
                <td>
                  {props.isFixed ?
                    <label>{": " + (props.Book.hasOwnProperty(id) ? props.Book[id] : "")}</label>
                    : <input
                      data-key={id}
                      type={props.bookStructure[id][1]}
                      value={props.Book[id]}
                      onChange={handleChange}
                    />}
                </td>
              </tr>
            )
          })
        }
      </tbody>
    </table>
  )
}

export default BasicInfo